/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.estudiante;

import java.awt.HeadlessException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import logica.clsConexion;
import logica.logger.UseLogger;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.RichTextString;

/**
 *
 * @author Gloriana
 */
public class clsGestorEstudiante extends clsConexion {

    public boolean insertarEst(clsEstudiante est) {
        boolean rpta = false;
        try {
            //Nombre del procedimiento almacenado y como espera tres parametros
            //le ponemos 3 interrogantes
            String call = "{CALL proc_guardarEstudiante(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";

            //Preparamos la sentecia
            conectarBD();
            obj_Procedimiento = getConexion().prepareCall(call);
            //tipo de identificacion
            obj_Procedimiento.setInt(1, est.getTipoIdentificacion());
            //Identificacion
            obj_Procedimiento.setString(2, est.getIdentificacion());
            //primer nombre
            obj_Procedimiento.setString(3, est.getPrimerNombre());
            //segundo nombre
            obj_Procedimiento.setString(4, est.getSegundoNombre());
            //primer apellido
            obj_Procedimiento.setString(5, est.getPrimerApellido());
            //segundo apellido
            obj_Procedimiento.setString(6, est.getSegundoApellido());
            //genero
            obj_Procedimiento.setString(7, String.valueOf(est.getGenero()));
            //fecha nacimiento
            obj_Procedimiento.setString(8, est.getFechaNac());
            //año de ingreso
            obj_Procedimiento.setInt(9, est.getAnnioIngreso());
            //fecha ingreso
            obj_Procedimiento.setString(10, est.getFechaIngreso());
            //periodo de ingreso
            obj_Procedimiento.setString(11, est.getPeriodoIngreso());
            //ultimo anio de colegio
            obj_Procedimiento.setString(12, est.getUltimoAnnioColegio());
            //ultimo grado academico
            obj_Procedimiento.setString(13, est.getUltimoGradoAcademico());
            //institucion procedencia
            obj_Procedimiento.setString(14, est.getInstitucionProcedencia());
            //correo
            obj_Procedimiento.setString(15, est.getCorreo());
            //telefono casa
            obj_Procedimiento.setString(16, est.getTelefonoCasa());
            // telefono movil
            obj_Procedimiento.setString(17, est.getTelefonoMovil());
            //telefono trabajo
            obj_Procedimiento.setString(18, est.getTelefonoTrabajo());
            //provincia
            obj_Procedimiento.setInt(19, est.getProvincia());
            //canton
            obj_Procedimiento.setInt(20, est.getCanton());
            //distrito
            obj_Procedimiento.setInt(21, est.getDistrito());
            //otras senas
            obj_Procedimiento.setString(22, est.getOtrasSenas());
            //nombr encargad
            obj_Procedimiento.setString(23, est.getNombreEncargado());
            //parentesco
            obj_Procedimiento.setString(24, est.getParentesco());
            //vive con
            obj_Procedimiento.setString(25, est.getViveCon());
            //estado civil
            obj_Procedimiento.setString(26, est.getEstadoCivil());
            //nombre madre
            obj_Procedimiento.setString(27, est.getNombreMadre());
            //profesion madre
            obj_Procedimiento.setString(28, est.getProfesionMadre());
            //nombre padre
            obj_Procedimiento.setString(29, est.getNombrePadre());
            //profesion padre
            obj_Procedimiento.setString(30, est.getProfesionPadre());
            //padecimiento medico
            obj_Procedimiento.setString(31, est.getPadecimiento());
            //medicamentos
            obj_Procedimiento.setString(32, est.getMedicamentos());
            //persona emergencia
            obj_Procedimiento.setString(33, est.getPersonaEmergencia());
            //telefono emergencia
            obj_Procedimiento.setString(34, est.getTelefonoEmergencia());
            rpta = obj_Procedimiento.executeUpdate() == 1;
            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rpta;
    }

    /**
     * Copia un solo archivo
     *
     * @param s
     * @param t
     * @return
     */
    public boolean copyFile(File s, File t) {
        try {
            FileChannel out;
            try (FileChannel in = (new FileInputStream(s)).getChannel()) {
                out = (new FileOutputStream(t)).getChannel();
                in.transferTo(0, s.length(), out);
            }
            out.close();
            return true;
        } catch (IOException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            return false;
        }
    }

    public ResultSet buscarEstudiante(String cedula) {
        try {
            //Nombre del procedimiento almacenado y como espera tres parametros
            //le ponemos 3 interrogantes
            String call = "{CALL proc_buscarEstudiante(?)}";
            //Preparamos la sentecia
            obj_Procedimiento = getConexion().prepareCall(call);
            //Identificacion
            obj_Procedimiento.setString(1, cedula);
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;

    }

    public boolean editarEst(clsEstudiante est) {
        boolean rpta = false;
        try {
            //Nombre del procedimiento almacenado y como espera tres parametros
            //le ponemos 3 interrogantes
            String call = "{CALL proc_editarEstudiante(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";

            //Preparamos la sentecia
            conectarBD();
            obj_Procedimiento = getConexion().prepareCall(call);
            //tipo de identificacion
            obj_Procedimiento.setInt(1, est.getTipoIdentificacion());
            //Identificacion
            obj_Procedimiento.setString(2, est.getIdentificacion());
            //primer nombre
            obj_Procedimiento.setString(3, est.getPrimerNombre());
            //segundo nombre
            obj_Procedimiento.setString(4, est.getSegundoNombre());
            //primer apellido
            obj_Procedimiento.setString(5, est.getPrimerApellido());
            //segundo apellido
            obj_Procedimiento.setString(6, est.getSegundoApellido());
            //genero
            obj_Procedimiento.setString(7, String.valueOf(est.getGenero()));
            //fecha nacimiento
            obj_Procedimiento.setString(8, est.getFechaNac());
            //año de ingreso
            obj_Procedimiento.setInt(9, est.getAnnioIngreso());
            //fecha ingreso
            obj_Procedimiento.setString(10, est.getFechaIngreso());
            //periodo de ingreso
            obj_Procedimiento.setString(11, est.getPeriodoIngreso());
            //ultimo anio de colegio
            obj_Procedimiento.setString(12, est.getUltimoAnnioColegio());
            //ultimo grado academico
            obj_Procedimiento.setString(13, est.getUltimoGradoAcademico());
            //institucion procedencia
            obj_Procedimiento.setString(14, est.getInstitucionProcedencia());
            //correo
            obj_Procedimiento.setString(15, est.getCorreo());
            //telefono casa
            obj_Procedimiento.setString(16, est.getTelefonoCasa());
            // telefono movil
            obj_Procedimiento.setString(17, est.getTelefonoMovil());
            //telefono trabajo
            obj_Procedimiento.setString(18, est.getTelefonoTrabajo());
            //provincia
            obj_Procedimiento.setInt(19, est.getProvincia());
            //canton
            obj_Procedimiento.setInt(20, est.getCanton());
            //distrito
            obj_Procedimiento.setInt(21, est.getDistrito());
            //otras senas
            obj_Procedimiento.setString(22, est.getOtrasSenas());
            //nombr encargad
            obj_Procedimiento.setString(23, est.getNombreEncargado());
            //parentesco
            obj_Procedimiento.setString(24, est.getParentesco());
            //vive con
            obj_Procedimiento.setString(25, est.getViveCon());
            //estado civil
            obj_Procedimiento.setString(26, est.getEstadoCivil());
            //nombre madre
            obj_Procedimiento.setString(27, est.getNombreMadre());
            //profesion madre
            obj_Procedimiento.setString(28, est.getProfesionMadre());
            //nombre padre
            obj_Procedimiento.setString(29, est.getNombrePadre());
            //profesion padre
            obj_Procedimiento.setString(30, est.getProfesionPadre());
            //padecimiento medico
            obj_Procedimiento.setString(31, est.getPadecimiento());
            //medicamentos
            obj_Procedimiento.setString(32, est.getMedicamentos());
            //persona emergencia
            obj_Procedimiento.setString(33, est.getPersonaEmergencia());
            //telefono emergencia
            obj_Procedimiento.setString(34, est.getTelefonoEmergencia());
            rpta = obj_Procedimiento.executeUpdate() == 1;
            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rpta;

    }

    public boolean empadronarEstudiante(String identificacion, String codigoPlan, String nombreCarrera) {
        boolean rpta1 = false, rpta2 = false, rpta3 = false, rpta4 = false;
        String codigoCarrera = "";
        try {
            //Obtenemos la conexion
            conectarBD();
            //Decimos que vamos a crear una transaccion
            getConexion().setAutoCommit(false);

            rs = seleccionar("select codigocarrera from tblcarreras where nombrecarrera like '" + nombreCarrera + "'");
            if (rs != null) {
                rs.next();
                codigoCarrera = rs.getString(1);
                rpta1 = true;
            }

            //Preparamos la sentecia 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_empadronarCarrera(?,?,?)}");
            obj_Procedimiento.setString(1, identificacion);
            obj_Procedimiento.setString(2, codigoPlan);
            obj_Procedimiento.setString(3, codigoCarrera);

            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta2 = obj_Procedimiento.executeUpdate() == 1;

            //Preparamos la sentecia 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_empadronarCursos(?,?,?)}");
            obj_Procedimiento.setString(1, identificacion);
            obj_Procedimiento.setString(2, codigoCarrera);
            obj_Procedimiento.setString(3, codigoPlan);

            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta3 = obj_Procedimiento.executeUpdate() == 1;

            if (rpta1 && rpta2) {
                //Confirmamos la transaccion
                getConexion().commit();
            } else {
                //Negamos la transaccion
                getConexion().rollback();
            }
            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex) {
                new UseLogger().writeLog(this.getClass().getName(), e.toString());
            }
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex) {
                new UseLogger().writeLog(this.getClass().getName(), e.toString());
            }
            desconectarBD();
        }
        return rpta1 && rpta2;
    }

    public boolean verificarPadronCarreras(String identificacion, String codigoPlan, String nombreCarrera) {
        boolean respuesta = false;
        try {
            //Preparamos la sentecia
            conectarBD();
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_verificarPadronCarreras(?,?,?)}");
            //Identificacion
            obj_Procedimiento.setString(1, identificacion);
            obj_Procedimiento.setString(2, codigoPlan);
            obj_Procedimiento.setString(3, nombreCarrera);
            rs = obj_Procedimiento.executeQuery();
            if (rs != null) {
                if (rs.next()) {
                    respuesta = true;
                }
            }
            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return respuesta;
    }

    public String buscarCodigoCarrera(String nombreCarrera) {
        String codigoCarrera = "";
        try {
            conectarBD();
            rs = seleccionar("select codigocarrera from tblcarreras where nombrecarrera like '" + nombreCarrera + "'");
            if (rs != null) {
                rs.next();
                codigoCarrera = rs.getString(1);
            }
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return codigoCarrera;
    }

    public ResultSet buscarEstudiantesCarreras(String codigoCarrera) {
        try {
            //Nombre del procedimiento almacenado y como espera tres parametros
            //le ponemos 3 interrogantes
            String call = "{CALL proc_cargarEstudiantesCarreras(?)}";
            //Preparamos la sentecia
            obj_Procedimiento = getConexion().prepareCall(call);
            //Identificacion
            obj_Procedimiento.setString(1, codigoCarrera);
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    public ResultSet cargarEstudiantesCarreras() {
        try {
            //Nombre del procedimiento almacenado y como espera tres parametros
            //le ponemos 3 interrogantes
            String call = "{CALL proc_cargarEstudiantesCarrerasTodos}";
            //Preparamos la sentecia
            obj_Procedimiento = getConexion().prepareCall(call);
            //Identificacion
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    public ResultSet cargarEstudiantesEmpadronados() {
        try {
            //Nombre del procedimiento almacenado y como espera tres parametros
            //le ponemos 3 interrogantes
            String call = "{CALL proc_cargarTodosEstudiantesEmpadronados}";
            //Preparamos la sentecia
            obj_Procedimiento = getConexion().prepareCall(call);
            //Identificacion
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    public void cargarReporteEstudiante(String identificacion) {
        String logotipo = "/reportes/logoinvenio.png";

        Icon foto = cargarFoto(identificacion);
        File entrada = null;
        if (foto != null) {
            entrada = new File(foto.toString());
        }
        conectarBD();
        JasperReport reporte;
        JasperPrint reporte_view;
        try {
            //direccion del archivo JASPER
            URL in = this.getClass().getResource("/reportes/estudiantes/rptEstudiante1.jasper");
            reporte = (JasperReport) JRLoader.loadObject(in);

            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();
            parametros.put("identificacion", identificacion);
            parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));
            if (entrada != null) {
                try {
                    parametros.put("foto", new FileInputStream(entrada));
                } catch (FileNotFoundException ex) {
                    new UseLogger().writeLog(this.getClass().getName(), ex.toString());
                }
            } else {
                parametros.put("foto", this.getClass().getResourceAsStream("/reportes/no-foto.png"));
            }
            //-----------------------------------
            reporte_view = JasperFillManager.fillReport(reporte, parametros, getConexion());
            JasperViewer.viewReport(reporte_view, false);

        } catch (JRException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
        }
        desconectarBD();
    }
    //=========================================================================

    public Icon cargarFoto(String identificacion) {
        Icon foto = null;
        File dir = new File(System.getProperty("user.dir") + "\\fotos\\");
        String[] ficheros = dir.list();
        if (ficheros == null) {
            System.out.println("No hay ficheros en el directorio especificado");
        } else {
            for (int x = 0; x < ficheros.length; x++) {
                if (ficheros[x].startsWith(identificacion)) {
                    foto = new ImageIcon(System.getProperty("user.dir") + "\\fotos\\" + ficheros[x]);
                    break;
                }
            }
        }
        return foto;
    }
    //Buscar si foto existe

    public void cargarReporteCarreras(String sql) {
        String logotipo = "/reportes/logoinvenio.png";
        conectarBD();
        rs = seleccionar(sql);
        if (rs != null) {
            JasperReport reporte;
            JasperPrint reporte_view;
            try {
                //direccion del archivo JASPER
                URL in = this.getClass().getResource("/reportes/estudiantes/rptEstudiantesCarreras.jasper");
                reporte = (JasperReport) JRLoader.loadObject(in);
                //Se crea un objeto HashMap
                Map parametros = new HashMap();
                parametros.clear();
                parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));
                JRResultSetDataSource resultSetDataSource = new JRResultSetDataSource(rs);
                reporte_view = JasperFillManager.fillReport(reporte, parametros, resultSetDataSource);
                JasperViewer.viewReport(reporte_view, false);

            } catch (JRException ex) {
                new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            }
        } else {
            System.out.println("El ResultSet del reporte malo");
        }
        desconectarBD();
    }

    public void cargarGraficoEstCarr(String sql, String titulo) {
        String logotipo = "/reportes/logoinvenio.png";
        conectarBD();
        rs = seleccionar(sql);
        if (rs != null) {
            JasperReport reporte;
            JasperPrint reporte_view;
            try {
                //direccion del archivo JASPER
                URL in = this.getClass().getResource("/reportes/estudiantes/rptGraficosEstudiantesCarreras.jasper");
                reporte = (JasperReport) JRLoader.loadObject(in);
                //Se crea un objeto HashMap
                Map parametros = new HashMap();
                parametros.clear();
                parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));
                parametros.put("titulo", titulo);
                JRResultSetDataSource resultSetDataSource = new JRResultSetDataSource(rs);
                reporte_view = JasperFillManager.fillReport(reporte, parametros, resultSetDataSource);
                JasperViewer.viewReport(reporte_view, false);

            } catch (JRException ex) {
                new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            }
        } else {
            System.out.println("El ResultSet del reporte malo");
        }
        desconectarBD();

    }

    public void cargarGraficosUltimos() {
        conectarBD();
        JasperReport reporte;
        JasperPrint reporte_view;
        try {
            //direccion del archivo JASPER
            URL in = this.getClass().getResource("/reportes/estudiantes/rptUltimosAnnios.jasper");
            reporte = (JasperReport) JRLoader.loadObject(in);
            Calendar fechaAct = Calendar.getInstance();
            int annio = fechaAct.get(Calendar.YEAR);
            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();
            parametros.put("logoinvenio", this.getClass().getResourceAsStream("/reportes/logoinvenio.png"));
            parametros.put("titulo", "AÑOS: " + (annio - 2) + " - " + (annio - 1) + " - " + annio);

            //-----------------------------------
            reporte_view = JasperFillManager.fillReport(reporte, parametros, getConexion());
            JasperViewer.viewReport(reporte_view, false);

        } catch (JRException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
        }
        desconectarBD();
    }

    public ResultSet cargarCarrerasEmpadronadas(String identificacion) {
        try {
            //Nombre del procedimiento almacenado y como espera tres parametros
            //le ponemos 3 interrogantes
            String call = "{CALL proc_buscarCarrerasEmpadronadas(?)}";
            //Preparamos la sentecia
            obj_Procedimiento = getConexion().prepareCall(call);
            //Identificacion
            obj_Procedimiento.setString(1, identificacion);

            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    public boolean eliminarEstudiante(String identificacion) {
        boolean rpta = false;
        try {
            //Obtenemos la conexion
            conectarBD();
            //Decimos que vamos a crear una transaccion
            getConexion().setAutoCommit(false);

            //Preparamos la inserccion 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_eliminarEstudiante(?)}");
            obj_Procedimiento.setString(1, identificacion);
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta = obj_Procedimiento.executeUpdate() == 1 ? true : false;

            if (rpta) {
                //Confirmamos la transaccion
                getConexion().commit();
            } else {
                //Negamos la transaccion
                getConexion().rollback();
            }
            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex) {
                new UseLogger().writeLog(this.getClass().getName(), e.toString());
            }
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex) {
                new UseLogger().writeLog(this.getClass().getName(), e.toString());
            }
            desconectarBD();
        }
        return rpta;
    }

    public void imprimirResidenciaAprobados(String identificacion, String nombre, String plan, String carrera) {
        String logotipo = "/reportes/logoinvenio.png";
        conectarBD();
        JasperReport reporte;
        JasperPrint reporte_view;
        try {
            //direccion del archivo JASPER
            URL in = this.getClass().getResource("/reportes/estudiantes/rptResidenciaEstudiantil.jasper");
            reporte = (JasperReport) JRLoader.loadObject(in);

            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();
            parametros.put("identificacion", identificacion);
            parametros.put("estudiante", nombre);
            parametros.put("carrera", carrera);
            parametros.put("plan", plan);
            parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));
            reporte_view = JasperFillManager.fillReport(reporte, parametros, getConexion());
            JasperViewer.viewReport(reporte_view, false);

        } catch (JRException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
        }
        desconectarBD();
    }

    public boolean eliminarCarreraEmpadronada(String identificacion, String carrera, String plan) {
        boolean rpta = false;
        try {
            //Obtenemos la conexion
            conectarBD();
            //Preparamos la inserccion 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_eliminarCarreraEmpadronada(?,?,?)}");
            obj_Procedimiento.setString(1, identificacion);
            obj_Procedimiento.setString(2, carrera);
            obj_Procedimiento.setString(3, plan);
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta = obj_Procedimiento.executeUpdate() == 1 ? true : false;
            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rpta;
    }

    public ResultSet cargarEstudiantesDesertores() {
        try {
            //Nombre del procedimiento almacenado y como espera tres parametros
            //le ponemos 3 interrogantes
            String call = "{CALL proc_cargarEstudiantesDesertores}";
            //Preparamos la sentecia
            obj_Procedimiento = getConexion().prepareCall(call);
            //Identificacion
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    public boolean guardarEstudianteDesertor(String identificacion, String codigoPlan, String codigoCarrera,
            String annio, String periodo, String observaciones) {
        boolean rpta = false;
        try {
            //Nombre del procedimiento almacenado y como espera tres parametros
            //le ponemos 3 interrogantes
            String call = "{CALL proc_guardarEstudianteDesertor(?,?,?,?,?,?)}";

            //Preparamos la sentecia
            conectarBD();
            obj_Procedimiento = getConexion().prepareCall(call);
            //tipo de identificacion
            obj_Procedimiento.setString(1, identificacion);
            //Identificacion
            obj_Procedimiento.setString(2, codigoPlan);
            //primer nombre
            obj_Procedimiento.setString(3, codigoCarrera);
            //segundo nombre
            obj_Procedimiento.setString(4, annio);
            //primer apellido
            obj_Procedimiento.setString(5, periodo);
            //segundo apellido
            obj_Procedimiento.setString(6, observaciones);

            rpta = obj_Procedimiento.executeUpdate() == 1 ? true : false;
            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rpta;
    }

    public boolean guardarEstudianteInactivo(String identificacion, String codigoPlan, String codigoCarrera,
            String annio, String periodo, String observaciones) {
        boolean rpta = false;
        try {
            //Nombre del procedimiento almacenado y como espera tres parametros
            //le ponemos 3 interrogantes
            String call = "{CALL proc_guardarEstudianteInactivo(?,?,?,?,?,?)}";

            //Preparamos la sentecia
            conectarBD();
            obj_Procedimiento = getConexion().prepareCall(call);
            //tipo de identificacion
            obj_Procedimiento.setString(1, identificacion);
            //Identificacion
            obj_Procedimiento.setString(2, codigoPlan);
            //primer nombre
            obj_Procedimiento.setString(3, codigoCarrera);
            //segundo nombre
            obj_Procedimiento.setString(4, annio);
            //primer apellido
            obj_Procedimiento.setString(5, periodo);
            //segundo apellido
            obj_Procedimiento.setString(6, observaciones);

            rpta = obj_Procedimiento.executeUpdate() == 1 ? true : false;
            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rpta;
    }

    public void cargarReporteEgresados(String sql) {
        String logotipo = "/reportes/logoinvenio.png";
        conectarBD();
        rs = seleccionar(sql);
        if (rs != null) {
            JasperReport reporte;
            JasperPrint reporte_view;
            try {
                //direccion del archivo JASPER
                URL in = this.getClass().getResource("/reportes/estudiantes/rptEstudiantesEgresados.jasper");
                reporte = (JasperReport) JRLoader.loadObject(in);
                //Se crea un objeto HashMap
                Map parametros = new HashMap();
                parametros.clear();
                parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));
                JRResultSetDataSource resultSetDataSource = new JRResultSetDataSource(rs);
                reporte_view = JasperFillManager.fillReport(reporte, parametros, resultSetDataSource);
                JasperViewer.viewReport(reporte_view, false);

            } catch (JRException ex) {
                new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            }
        } else {
            System.out.println("El ResultSet del reporte malo");
        }
        desconectarBD();
    }

    public void cargarReporteDesertores(String sql) {
        String logotipo = "/reportes/logoinvenio.png";
        conectarBD();
        rs = seleccionar(sql);
        if (rs != null) {
            JasperReport reporte;
            JasperPrint reporte_view;
            try {
                //direccion del archivo JASPER
                URL in = this.getClass().getResource("/reportes/estudiantes/rptEstudiantesDesertores.jasper");
                reporte = (JasperReport) JRLoader.loadObject(in);
                //Se crea un objeto HashMap
                Map parametros = new HashMap();
                parametros.clear();
                parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));
                JRResultSetDataSource resultSetDataSource = new JRResultSetDataSource(rs);
                reporte_view = JasperFillManager.fillReport(reporte, parametros, resultSetDataSource);
                JasperViewer.viewReport(reporte_view, false);

            } catch (JRException ex) {
                new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            }
        } else {
            System.out.println("El ResultSet del reporte malo");
        }
        desconectarBD();
    }

    public void cargarReporteInactivos(String sql) {
        String logotipo = "/reportes/logoinvenio.png";
        conectarBD();
        rs = seleccionar(sql);
        if (rs != null) {
            JasperReport reporte;
            JasperPrint reporte_view;
            try {
                //direccion del archivo JASPER
                URL in = this.getClass().getResource("/reportes/estudiantes/rptEstudiantesInactivos.jasper");
                reporte = (JasperReport) JRLoader.loadObject(in);
                //Se crea un objeto HashMap
                Map parametros = new HashMap();
                parametros.clear();
                parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));
                JRResultSetDataSource resultSetDataSource = new JRResultSetDataSource(rs);
                reporte_view = JasperFillManager.fillReport(reporte, parametros, resultSetDataSource);
                JasperViewer.viewReport(reporte_view, false);

            } catch (JRException ex) {
                new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            }
        } else {
            System.out.println("El ResultSet del reporte malo");
        }
        desconectarBD();
    }

    public boolean guardarEgresado(String identificacion, String codigoPlan, String codigoCarrera,
            String annio, String periodo) {
        boolean rpta = false;
        try {
            //Nombre del procedimiento almacenado y como espera tres parametros
            //le ponemos 3 interrogantes
            String call = "{CALL proc_guardarEstudianteEgresado(?,?,?,?,?)}";

            //Preparamos la sentecia
            conectarBD();
            obj_Procedimiento = getConexion().prepareCall(call);
            //tipo de identificacion
            obj_Procedimiento.setString(1, identificacion);
            //Identificacion
            obj_Procedimiento.setString(2, codigoPlan);
            //primer nombre
            obj_Procedimiento.setString(3, codigoCarrera);
            //segundo nombre
            obj_Procedimiento.setString(4, annio);
            //primer apellido
            obj_Procedimiento.setString(5, periodo);

            rpta = obj_Procedimiento.executeUpdate() == 1 ? true : false;
            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rpta;
    }

    public ResultSet cargarEstudiantesEgresados() {
        try {
            //Nombre del procedimiento almacenado y como espera tres parametros
            //le ponemos 3 interrogantes
            String call = "{CALL proc_cargarEstudiantesEgresados}";
            //Preparamos la sentecia
            obj_Procedimiento = getConexion().prepareCall(call);
            //Identificacion
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    public boolean cambiarEstado(String identificacion, String codigoPlan, String codigoCarrera) {
        boolean rpta = false;
        try {
            //Nombre del procedimiento almacenado y como espera tres parametros
            //le ponemos 3 interrogantes
            String call = "{CALL proc_editarEstadoEstudiante(?,?,?)}";

            //Preparamos la sentecia
            conectarBD();
            obj_Procedimiento = getConexion().prepareCall(call);
            //tipo de identificacion
            obj_Procedimiento.setString(1, identificacion);
            //Identificacion
            obj_Procedimiento.setString(2, codigoPlan);
            //primer nombre
            obj_Procedimiento.setString(3, codigoCarrera);

            rpta = obj_Procedimiento.executeUpdate() == 1 ? true : false;
            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rpta;
    }

    public boolean cambiarEstadoInactivo(String identificacion, String codigoPlan, String codigoCarrera) {
        boolean rpta = false;
        try {
            //Nombre del procedimiento almacenado y como espera tres parametros
            //le ponemos 3 interrogantes
            String call = "{CALL proc_eliminarEstudianteInactivo(?,?,?)}";

            //Preparamos la sentecia
            conectarBD();
            obj_Procedimiento = getConexion().prepareCall(call);
            //tipo de identificacion
            obj_Procedimiento.setString(1, identificacion);
            //Identificacion
            obj_Procedimiento.setString(2, codigoPlan);
            //primer nombre
            obj_Procedimiento.setString(3, codigoCarrera);

            rpta = obj_Procedimiento.executeUpdate() == 1 ? true : false;
            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rpta;
    }

    public void imprimirResidenciaAprobadosTodos(String identificacion, String nombre, String plan, String carrera) {
        String logotipo = "/reportes/logoinvenio.png";
        conectarBD();
        JasperReport reporte;
        JasperPrint reporte_view;
        try {
            //direccion del archivo JASPER
            URL in = this.getClass().getResource("/reportes/estudiantes/rptResidenciaEstudiantilCalif.jasper");
            reporte = (JasperReport) JRLoader.loadObject(in);

            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();
            parametros.put("identificacion", identificacion);
            parametros.put("carrera", carrera);
            parametros.put("codigoplan", plan);
            parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));
            reporte_view = JasperFillManager.fillReport(reporte, parametros, getConexion());
            JasperViewer.viewReport(reporte_view, false);

        } catch (JRException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
        }
        desconectarBD();
    }

    public void imprimirResidenciaAprobadosTodosConNota(String identificacion, String nombre, String plan, String carrera) {
        String logotipo = "/reportes/logoinvenio.png";
        conectarBD();
        JasperReport reporte;
        JasperPrint reporte_view;
        try {
            //direccion del archivo JASPER
            URL in = this.getClass().getResource("/reportes/estudiantes/rptResidenciaEstudiantilConNota.jasper");
            reporte = (JasperReport) JRLoader.loadObject(in);

            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();
            parametros.put("identificacion", identificacion);
            parametros.put("estudiante", nombre);
            parametros.put("carrera", carrera);
            parametros.put("plan", plan);
            parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));
            reporte_view = JasperFillManager.fillReport(reporte, parametros, getConexion());
            JasperViewer.viewReport(reporte_view, false);

        } catch (JRException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
        }
        desconectarBD();
    }

    public void imprimirResidenciaAprobadosTodosConNotaPorPeriodos(String sql, String plan, String carrera) {
        String logotipo = "/reportes/logoinvenio.png";
        conectarBD();
        rs = seleccionar(sql);
        if (rs != null) {
            JasperReport reporte;
            JasperPrint reporte_view;
            try {
                //direccion del archivo JASPER
                URL in = this.getClass().getResource("/reportes/estudiantes/rptResidenciaEstudiantilCalifPorPeriodos.jasper");
                reporte = (JasperReport) JRLoader.loadObject(in);
                //Se crea un objeto HashMap
                Map parametros = new HashMap();
                parametros.clear();
                parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));
                parametros.put("carrera", carrera);
                parametros.put("codigoplan", plan);
                JRResultSetDataSource resultSetDataSource = new JRResultSetDataSource(rs);
                reporte_view = JasperFillManager.fillReport(reporte, parametros, resultSetDataSource);
                JasperViewer.viewReport(reporte_view, false);

            } catch (JRException ex) {
                new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            }
        } else {
            System.out.println("El ResultSet del reporte malo");
        }
        desconectarBD();
    }

    public int buscarTipoIdentificacion(String identificacion) {
        int tipo = -1;
        conectarBD();
        rs = seleccionar("select IDTIPOIDENTIFICACION from tblestudiante where IDENTIFICACION like '" + identificacion + "'");
        if (rs != null) {
            try {
                if (rs.next()) {
                    tipo = rs.getInt(1);

                }
            } catch (SQLException ex) {
                Logger.getLogger(clsGestorEstudiante.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }
        desconectarBD();
        return tipo;
    }
    //nueva funcionalidad

    public void generarArchivoExcel(String sql) {

        try {
            HSSFWorkbook wb = new HSSFWorkbook();
            HSSFSheet sheet = wb.createSheet("hoja1");
            HSSFRow row = sheet.createRow(0);

            conectarBD();
            rs = seleccionar(sql);
            ResultSetMetaData Datos = rs.getMetaData();
            row = sheet.createRow(0);
            for (int i = 0; i < Datos.getColumnCount(); i++) {
                row.createCell(i).setCellValue(Datos.getColumnName(i + 1));
            }

            int fil = 1;
            while (rs.next()) {
                row = sheet.createRow(fil++);
                for (int col = 0; col < Datos.getColumnCount(); col++) {
                    try {
                        row.createCell(col).setCellValue(rs.getObject(col + 1).toString());
                    } catch (Exception e) {
                        row.createCell(col).setCellValue("");
                    }
                }
            }
            JFileChooser fc = new JFileChooser();

            fc.setSelectedFile(new File("Lista.xls"));
            int returnVal = fc.showSaveDialog(null);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                try (FileOutputStream fileOut = new FileOutputStream(file.getPath())) {
                    wb.write(fileOut);
                }

                //Ejecutar archivo de excel
//                String dir=file.getPath();
//                Runtime.getRuntime().exec("cmd /c start " +dir);
                JOptionPane.showMessageDialog(null, "Archivo " + fc.getSelectedFile().getName() + " guarado exitosamente");

            }
        } catch (SQLException | HeadlessException | IOException e) {
            javax.swing.JOptionPane.showMessageDialog(null, "Error al exportar la consulta: " + e);
            desconectarBD();
        }

        desconectarBD();
    }

    public void generarArchivoExcelPromedio(String sql, String estudiante) {

        try {
            HSSFWorkbook wb = new HSSFWorkbook();
            conectarBD();
            HSSFSheet sheet = wb.createSheet("PROMEDIOS");
            HSSFRow row = sheet.createRow(0);
            row.createCell(0).setCellValue("UNIVERSIDAD DUAL INVENIO");
            boolean inicio = true;

            rs = seleccionar(sql);

            int fil = 4; // a partir de la fila 8 llena la lista de estudiantes
            Object dato;
            CellStyle style1 = wb.createCellStyle();
            while (rs.next()) {
                if (inicio) {
                    Object cedula = rs.getObject(1);
                    Object nombre = rs.getObject(2);
                    row = sheet.createRow(2);
                    row.createCell(0).setCellValue((String) cedula);
                    row.createCell(1).setCellValue((String) nombre);
                    nombre = rs.getObject(4);
                    row = sheet.createRow(1);
                    row.createCell(0).setCellValue((String) nombre);
                    inicio = false;
                }
                row = sheet.createRow(fil++);
                try {
                    dato = rs.getObject(6);
                    row.createCell(0).setCellValue(("" + dato).substring(0, 4));
                    dato = rs.getObject(7);
                    row.createCell(1).setCellValue("" + dato);
                    dato = rs.getObject(8);
                    String curso = (String) dato;
                    String separados[] = curso.split("\n");

                    for (String separado : separados) {
                        row.createCell(2).setCellValue(separado.substring(0, separado.indexOf(':') + 1));
                        row.createCell(3).setCellValue(Integer.parseInt(separado.substring(separado.indexOf(':') + 1, separado.length()).trim()));
                        row = sheet.createRow(fil++);
                    }

//                    style1.setFillForegroundColor(HSSFColor.LIME.index);
//                    style1.setFillPattern((short)(1));
                    HSSFFont font = wb.createFont();
                    font.setColor(HSSFColor.RED.index);
                    style1.setFont(font);
                    dato = rs.getObject(9);
                    row.createCell(4).setCellValue(Double.parseDouble(("" + dato)));
                    row.getCell(4).setCellType(Cell.CELL_TYPE_NUMERIC);
                    row.getCell(4).setCellStyle(style1);

                } catch (SQLException e) {
                    row.createCell(4).setCellValue("");
                }

            }
            row = sheet.createRow(fil++);
            row.createCell(4).setCellValue("PROMEDIO GENERAL");
            row.createCell(5).setCellFormula("AVERAGE(E5:E" + (fil - 1) + ")");
            row.getCell(5).setCellStyle(style1);
            desconectarBD();
            JFileChooser fc = new JFileChooser();

            fc.setSelectedFile(new File(estudiante + ".xls"));
            int returnVal = fc.showSaveDialog(null);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                try (FileOutputStream fileOut = new FileOutputStream(file.getPath())) {
                    wb.write(fileOut);
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(clsGestorEstudiante.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(clsGestorEstudiante.class.getName()).log(Level.SEVERE, null, ex);
                }

                //Ejecutar archivo de excel
                //String dir = file.getPath();
                //Runtime.getRuntime().exec("cmd /c start " + dir);
                JOptionPane.showMessageDialog(null, "Archivo " + fc.getSelectedFile().getName() + " guarado exitosamente");

            }
        } catch (SQLException | HeadlessException e) {
            javax.swing.JOptionPane.showMessageDialog(null, "Error al exportar la consulta: " + e);
            desconectarBD();
//        }
        }
    }

    public void imprimirCursosEstudiantesConFoto(String condiciones) {
        String logotipo = "/reportes/logoinvenio.png";

        JasperReport reporte;
        JasperPrint reporte_view;
        try {
            //direccion del archivo JASPER
            URL in = this.getClass().getResource("/reportes/grupos/rptListaDeEstudiantesConFotoCarne.jasper");
            reporte = (JasperReport) JRLoader.loadObject(in);

            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();
            parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));

            //==================================================================
            clsGestorEstudianteDataSourceFoto datasource = new clsGestorEstudianteDataSourceFoto();
            clsEstudianteFotoLista estudianteFoto;
            conectarBD();
            String nombre1, nombre2, apellido1, apellido2;
            rs = cargarEstudianteConFoto(condiciones);
            if (rs != null) {
                while (rs.next()) {
                    estudianteFoto = new clsEstudianteFotoLista();
                    estudianteFoto.setIdentificacion(rs.getString(1));
                    nombre1 = rs.getString(2);
                    nombre2 = rs.getString(3);
                    apellido1 = rs.getString(4);
                    apellido2 = rs.getString(5);
                    estudianteFoto.setNombreCompleto(apellido1 + " " + apellido2
                            + " " + nombre1 + " " + nombre2);
                    estudianteFoto.setFechaNacimiento(rs.getString(6));
                    estudianteFoto.setCarrera(rs.getString(7));
                    Icon foto = cargarFoto(estudianteFoto.getIdentificacion());
                    File entrada = null;
                    if (foto != null) {
                        entrada = new File(foto.toString());
                    }

                    estudianteFoto.setFoto(entrada);

                    datasource.addclsEstudianteFoto(estudianteFoto);
                }

            }
            desconectarBD();
            //==================================================================
            parametros.put("total", datasource.getTotal());
            reporte_view = JasperFillManager.fillReport(reporte, parametros, datasource);
            JasperViewer.viewReport(reporte_view, false);

        } catch (JRException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        } catch (NullPointerException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        } catch (Exception ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
    }

    public ResultSet cargarEstudianteConFoto(String condiciones) {
        String sql = "SELECT "
                + " tblestudiante.`IDENTIFICACION`,"
                + " tblestudiante.`PRIMERNOMBRE` ,"
                + " tblestudiante.`SEGUNDONOMBRE` ,"
                + " tblestudiante.`PRIMERAPELLIDO` ,"
                + " tblestudiante.`SEGUNDOAPELLIDO`,"
                + " tblestudiante.`FECHANACIMIENTO`,"
                + " tblcarreras.`NOMBRECARRERA`"
                + " FROM "
                + " `tblestudiante` tblestudiante INNER JOIN `tblempadronamientocarrera` tblempadronamientocarrera ON tblestudiante.`IDENTIFICACION` = tblempadronamientocarrera.`IDENTIFICACION`"
                + " INNER JOIN `tblplanesxcarrera` tblplanesxcarrera ON tblempadronamientocarrera.`CODIGOPLAN` = tblplanesxcarrera.`CODIGOPLAN` "
                + " AND tblplanesxcarrera.`CODIGOCARRERA` = tblempadronamientocarrera.`CODIGOCARRERA` "
                + " INNER JOIN `tblcarreras` tblcarreras ON tblplanesxcarrera.`CODIGOCARRERA` = tblcarreras.`CODIGOCARRERA` "
                + " WHERE "
                + " tblempadronamientocarrera.ACTIVO = 1 "
                + condiciones
                + " ORDER BY tblempadronamientocarrera.CODIGOCARRERA,tblestudiante.PERIODOINGRESO,tblestudiante.ANNIOINGRESO";
        rs = seleccionar(sql);
        return rs;

    }

    public void generarArchivoExcel(String titulo, JTable tblDatos) {

        try {
            HSSFWorkbook wb = new HSSFWorkbook();
            HSSFSheet sheet = wb.createSheet(titulo);
            HSSFRow row = sheet.createRow(0);
            row.createCell(0).setCellValue(titulo);
            row = sheet.createRow(1);
            row = sheet.createRow(2);
            for (int i = 0; i < tblDatos.getColumnCount(); i++) {
                row.createCell(i).setCellValue(tblDatos.getColumnName(i));
            }

            int fil = 3;
            for (int i = 0; i < tblDatos.getRowCount(); i++) {
                row = sheet.createRow(fil++);
                for (int col = 0; col < tblDatos.getColumnCount(); col++) {
                    try {
                        row.createCell(col).setCellValue(tblDatos.getValueAt(i, col).toString());
                    } catch (Exception e) {
                        row.createCell(col).setCellValue("");
                    }
                }
            }
            JFileChooser fc = new JFileChooser();

            fc.setSelectedFile(new File(titulo + ".xls"));
            int returnVal = fc.showSaveDialog(null);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                try (FileOutputStream fileOut = new FileOutputStream(file.getPath())) {
                    wb.write(fileOut);
                }

                //Ejecutar archivo de excel
//                String dir=file.getPath();
//                Runtime.getRuntime().exec("cmd /c start " +dir);
                JOptionPane.showMessageDialog(null, "Archivo " + fc.getSelectedFile().getName() + " guarado exitosamente");

            }
        } catch (IOException | HeadlessException e) {
            javax.swing.JOptionPane.showMessageDialog(null, "Error al exportar la consulta: " + e);
            desconectarBD();
        }
        desconectarBD();
    }

    public void imprimirCertificacionResidencia(int tipo, String identificacion, String plan, String carrera) {
        String logotipo = "/reportes/logoinvenio.png";
        conectarBD();
        JasperReport reporte;
        JasperPrint reporte_view;
        String rector = "";
        try {
            //direccion del archivo JASPER
            URL in = null;
            switch (tipo) {
                case 1:
                    in = this.getClass().getResource("/reportes/estudiantes/rptEstudianteCertificacionCalifAprob.jasper");
                    rs = seleccionar("select nombrecompleto from tblautoridades where cargo like 'Rector(a)'");
                    if (rs.next()) {
                        rector = rs.getString(1);
                    }
                    break;
                case 2:
                    rs = seleccionar("select nombrecompleto from tblautoridades where cargo like 'Rector(a)'");
                    if (rs.next()) {
                        rector = rs.getString(1);
                    }
                    in = this.getClass().getResource("/reportes/estudiantes/rptEstudianteCertificacionCalifAprobRep.jasper");
                    break;
                case 3:
                    in = this.getClass().getResource("/reportes/estudiantes/rptEstudianteCertificacionCalifAprobCierre.jasper");
                    rector = "";
                    rs = seleccionar("select nombrecompleto from tblautoridades where cargo like 'Registro'");
                    if (rs.next()) {
                        rector = rs.getString(1);
                    }
                    break;
            }
            reporte = (JasperReport) JRLoader.loadObject(in);
            Date fechaDate = new Date();

            String fecha = (new java.text.SimpleDateFormat("EEEEE dd MMMMM yyyy", new Locale("es", "ES"))).format(fechaDate);
            String hora = "" + (new java.text.SimpleDateFormat("hh:mm aaa", new Locale("es", "ES"))).format(fechaDate);
            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();
            parametros.put("identificacion", identificacion);
            parametros.put("carrera", carrera);
            parametros.put("codigoplan", plan);
            parametros.put("fecha", fecha);
            parametros.put("hora", hora);
            parametros.put("rector", rector);
            parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));
            reporte_view = JasperFillManager.fillReport(reporte, parametros, getConexion());
            reporte_view.setTimeZoneId(hora);
            JasperViewer.viewReport(reporte_view, false);
        } catch (JRException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());

        } catch (SQLException ex) {
            Logger.getLogger(clsGestorEstudiante.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
//        catch (SQLException ex) {
//            System.out.println(ex.toString());
//        }
        desconectarBD();
    }

    public void imprimirConstanciaResidencia(int tipo, String identificacion, String plan, String carrera) {
        String logotipo = "/reportes/logoinvenio.png";
        conectarBD();
        JasperReport reporte;
        JasperPrint reporte_view;
        String rector = "";
        try {
            //direccion del archivo JASPER
            URL in = null;
            switch (tipo) {
                case 1:
                    in = this.getClass().getResource("/reportes/estudiantes/rptEstudianteConstanciaCalifAprob.jasper");
                    rs = seleccionar("select nombrecompleto from tblautoridades where cargo like 'Registro'");
                    if (rs.next()) {
                        rector = rs.getString(1);
                    }
                    break;
                case 2:
                    rs = seleccionar("select nombrecompleto from tblautoridades where cargo like 'Registro'");
                    if (rs.next()) {
                        rector = rs.getString(1);
                    }
                    in = this.getClass().getResource("/reportes/estudiantes/rptEstudianteConstanciaCalifAprobRep.jasper");
                    break;
                case 3:
                    in = this.getClass().getResource("/reportes/estudiantes/rptEstudianteConstanciaCalifAprobCierre.jasper");
                    rector = "";
                    rs = seleccionar("select nombrecompleto from tblautoridades where cargo like 'Registro'");
                    if (rs.next()) {
                        rector = rs.getString(1);
                    }
                    break;
            }
            reporte = (JasperReport) JRLoader.loadObject(in);
            Date fechaDate = new Date();

            String fecha = (new java.text.SimpleDateFormat("EEEEE dd MMMMM yyyy", new Locale("es", "ES"))).format(fechaDate);
            String hora = "" + (new java.text.SimpleDateFormat("hh:mm aaa", new Locale("es", "ES"))).format(fechaDate);
            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();
            parametros.put("identificacion", identificacion);
            parametros.put("carrera", carrera);
            parametros.put("codigoplan", plan);
            parametros.put("fecha", fecha);
            parametros.put("hora", hora);
            parametros.put("rector", rector);
            parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));
            reporte_view = JasperFillManager.fillReport(reporte, parametros, getConexion());
            reporte_view.setTimeZoneId(hora);
            JasperViewer.viewReport(reporte_view, false);
        } catch (JRException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());

        } catch (SQLException ex) {
            Logger.getLogger(clsGestorEstudiante.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
//        catch (SQLException ex) {
//            System.out.println(ex.toString());
//        }
        desconectarBD();
    }

    public boolean empadronarCambioCarreraEstudiante(String identificacion, String codigoPlanDestino, String nombreCarreraDestino,
            String codigoPlanOrigen, String codigoCarreraOrigen) {
        boolean rpta1 = false, rpta2 = false, rpta3 = false, rpta4 = false, rpta5 = false;
        String codigoCarreraDestino = "";
        try {
            //Obtenemos la conexion
            conectarBD();
            //Decimos que vamos a crear una transaccion
            getConexion().setAutoCommit(false);

            rs = seleccionar("select codigocarrera from tblcarreras where nombrecarrera like '" + nombreCarreraDestino + "'");
            if (rs != null) {
                rs.next();
                codigoCarreraDestino = rs.getString(1);
                rpta1 = true;
            }

            //Preparamos la sentecia 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_empadronarCarrera(?,?,?)}");
            obj_Procedimiento.setString(1, identificacion);
            obj_Procedimiento.setString(2, codigoPlanDestino);
            obj_Procedimiento.setString(3, codigoCarreraDestino);

            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta2 = obj_Procedimiento.executeUpdate() == 1;

            //Preparamos la sentecia 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_empadronarCursos(?,?,?)}");
            obj_Procedimiento.setString(1, identificacion);
            obj_Procedimiento.setString(2, codigoCarreraDestino);
            obj_Procedimiento.setString(3, codigoPlanDestino);

            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta3 = obj_Procedimiento.executeUpdate() == 1;

            //Preparamos la sentecia 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_realizarCambioCarrera(?,?,?)}");
            obj_Procedimiento.setString(1, identificacion);
            obj_Procedimiento.setString(2, codigoPlanOrigen);
            obj_Procedimiento.setString(3, codigoCarreraOrigen);

            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta4 = obj_Procedimiento.executeUpdate() == 1;

            //Preparamos la sentecia 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_guardarCambioCarrera(?,?,?,?,?)}");
            obj_Procedimiento.setString(1, identificacion);
            obj_Procedimiento.setString(2, codigoPlanOrigen);
            obj_Procedimiento.setString(3, codigoCarreraOrigen);
            obj_Procedimiento.setString(4, codigoPlanDestino);
            obj_Procedimiento.setString(5, codigoCarreraDestino);

            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta5 = obj_Procedimiento.executeUpdate() == 1;

            if (rpta1 && rpta2 && rpta4 && rpta5) {
                //Confirmamos la transaccion
                getConexion().commit();
            } else {
                //Negamos la transaccion
                getConexion().rollback();
            }
            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex) {
                new UseLogger().writeLog(this.getClass().getName(), e.toString());
            }
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex) {
                new UseLogger().writeLog(this.getClass().getName(), e.toString());
            }
            desconectarBD();
        }
        return rpta1 && rpta2 && rpta4 && rpta5;
    }

    public void imprimirResidenciaAprobadosTodos(int tipo, String identificacion, String nombre, String plan, String carrera) {
        String logotipo = "/reportes/logoinvenio.png";
        conectarBD();
        JasperReport reporte;
        JasperPrint reporte_view;
        try {
            //direccion del archivo JASPER
            URL in = null;
            switch (tipo) {
                case 1:
                    in = this.getClass().getResource("/reportes/estudiantes/rptResidenciaEstudiantilCalif.jasper");
                    break;
                case 2:
                    in = this.getClass().getResource("/reportes/estudiantes/rptResidenciaEstudiantilCalifAprob.jasper");
                    break;
                case 3:
                    in = this.getClass().getResource("/reportes/estudiantes/rptResidenciaEstudiantilCalifRep.jasper");
                    break;
            }
            reporte = (JasperReport) JRLoader.loadObject(in);

            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();
            parametros.put("identificacion", identificacion);
            parametros.put("carrera", carrera);
            parametros.put("codigoplan", plan);
            parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));
            reporte_view = JasperFillManager.fillReport(reporte, parametros, getConexion());
            JasperViewer.viewReport(reporte_view, false);

        } catch (JRException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
        }
        desconectarBD();
    }

    public ResultSet cargarEstudiantesConCambioCarrera() {
        try {
            //Nombre del procedimiento almacenado y como espera tres parametros
            //le ponemos 3 interrogantes
            String call = "{CALL proc_cargarEstudiantesConCambioCarrera}";
            //Preparamos la sentecia
            obj_Procedimiento = getConexion().prepareCall(call);
            //Identificacion
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    public ResultSet cargarEstudiantesInactivos() {
        try {
            //Nombre del procedimiento almacenado y como espera tres parametros
            //le ponemos 3 interrogantes
            String call = "{CALL proc_cargarEstudiantesInactivos}";
            //Preparamos la sentecia
            obj_Procedimiento = getConexion().prepareCall(call);
            //Identificacion
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    public synchronized DefaultTableModel cargarDeasignar(ResultSet rs) {
        DefaultTableModel modelo = null;
        ResultSetMetaData metaDatos;
        Object[] etiquetas;

        final int numeroColumnas;
        try {
            metaDatos = rs.getMetaData();
            numeroColumnas = metaDatos.getColumnCount();
            etiquetas = new Object[numeroColumnas + 1];
            // Se obtiene cada una de las etiquetas para cada columna
            for (int i = 0; i < numeroColumnas; i++) {
                // Nuevamente, para ResultSetMetaData la primera columna es la 1.
                etiquetas[i] = metaDatos.getColumnLabel(i + 1).toUpperCase();
            }
            etiquetas[numeroColumnas] = "HABILITAR";
            modelo = new javax.swing.table.DefaultTableModel(
                    new Object[][]{},
                    etiquetas);

            while (rs.next()) {
                // Se crea un array que será una de las filas de la tabla.
                Object[] fila = new Object[numeroColumnas + 1]; // Hay tres columnas en la tabla

                // Se rellena cada posición del array con una de las columnas de la tabla en base de datos.
                int i;
                for (i = 0; i < numeroColumnas; i++) {
                    fila[i] = rs.getObject(i + 1); // El primer indice en rs es el 1, no el cero, por eso se suma 1.
                }
                fila[i] = Boolean.FALSE;
                // Se añade al modelo la fila completa.
                modelo.addRow(fila);
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return modelo;
    }//=========================================================================

    public String buscarRealizadoPor(String realizado) {

        conectarBD();
        rs = seleccionar("SELECT concat(CONCAT(apellidos,' '),nombre) from tblusuarios where USUARIO LIKE '" + realizado + "'");
        if (rs != null) {
            try {
                if (rs.next()) {
                    realizado = rs.getString(1);

                }
            } catch (SQLException ex) {
                Logger.getLogger(clsGestorEstudiante.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }
        desconectarBD();
        return realizado;
    }

    public void cargarReporteEstudianteConstancia(String identificacion, String fechaInicio, String fechaFin, String periodo, String annio, String realizado,
            int constancia) {
        String logotipo = "/reportes/logoinvenio.png";
        conectarBD();
        JasperReport reporte;
        JasperPrint reporte_view;
        try {
            //direccion del archivo JASPER
            URL in = null;
            switch (constancia) {
                case 0:
                    in = this.getClass().getResource("/reportes/estudiantes/rptEstudianteConstancia.jasper");
                    break;
                case 1:
                    in = this.getClass().getResource("/reportes/estudiantes/rptEstudianteConstanciaBeca.jasper");
                    break;
            }
            reporte = (JasperReport) JRLoader.loadObject(in);

            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();
            parametros.put("identificacion", identificacion);
            parametros.put("fechaInicio", fechaInicio);
            parametros.put("fechaFin", fechaFin);
            parametros.put("cuatrimestre", periodo);
            parametros.put("annio", annio);
            parametros.put("realizado", realizado);
            Locale locale = new Locale("en", "EC"); //idioma de  Ecuador
            parametros.put(JRParameter.REPORT_LOCALE, locale);
            parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));
            reporte_view = JasperFillManager.fillReport(reporte, parametros, getConexion());
            JasperViewer.viewReport(reporte_view, false);

        } catch (JRException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
        }
        desconectarBD();
    }

    public void cargarReportePromedios(String sql, String periodo) {
        String logotipo = "/reportes/logoinvenio.png";
        conectarBD();
        rs = seleccionar(sql);
        if (rs != null) {
            JasperReport reporte;
            JasperPrint reporte_view;
            try {
                //direccion del archivo JASPER
                URL in = this.getClass().getResource("/reportes/estudiantes/rptEstudiantesCarrerasPromedios.jasper");
                reporte = (JasperReport) JRLoader.loadObject(in);
                //Se crea un objeto HashMap
                Map parametros = new HashMap();
                parametros.clear();
                parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));
                parametros.put("periodo", periodo);
                JRResultSetDataSource resultSetDataSource = new JRResultSetDataSource(rs);
                reporte_view = JasperFillManager.fillReport(reporte, parametros, resultSetDataSource);
                JasperViewer.viewReport(reporte_view, false);

            } catch (JRException ex) {
                new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            }
        } else {
            System.out.println("El ResultSet del reporte malo");
        }
        desconectarBD();
    }

    public String buscarCodigoPlan(String nombreCarrera) {
        String codigoPlan = "";
        try {
            conectarBD();
            rs = seleccionar("SELECT "
                    + "tblplanesxcarrera.CODIGOPLAN "
                    + "FROM tblplanesxcarrera "
                    + "INNER JOIN tblcarreras "
                    + "ON tblplanesxcarrera.CODIGOCARRERA = tblcarreras.CODIGOCARRERA "
                    + "WHERE tblcarreras.NOMBRECARRERA LIKE '" + nombreCarrera + "'");
            if (rs != null) {
                rs.next();
                codigoPlan = rs.getString(1);
            }
            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return codigoPlan;
    }

    public void cargarReportePromedios(String annio, String periodo, String codigoPlan, String identificacion, String estudiante) {
        String logotipo = "/reportes/logoinvenio.png";
        conectarBD();
        if (rs != null) {
            JasperReport reporte;
            JasperPrint reporte_view;
            try {
                //direccion del archivo JASPER
                URL in = this.getClass().getResource("/reportes/estudiantes/rptResidenciaEstudiantilUnPeriodo.jasper");
                reporte = (JasperReport) JRLoader.loadObject(in);
                //Se crea un objeto HashMap
                Map parametros = new HashMap();
                parametros.clear();
                parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));
                parametros.put("annio", annio);
                parametros.put("periodo", periodo);
                parametros.put("plan", codigoPlan);
                parametros.put("identificacion", identificacion);
                parametros.put("estudiante", estudiante);
                reporte_view = JasperFillManager.fillReport(reporte, parametros, getConexion());
                JasperViewer.viewReport(reporte_view, false);

            } catch (JRException ex) {
                new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            }
        } else {
            System.out.println("El ResultSet del reporte malo");
        }
        desconectarBD();
    }

    public void cargarReportePromediosPorCuatrimestre(String codigoPlan, String identificacion, String estudiante) {
        String logotipo = "/reportes/logoinvenio.png";
        conectarBD();
        if (rs != null) {
            JasperReport reporte;
            JasperPrint reporte_view;
            try {
                //direccion del archivo JASPER
                URL in = this.getClass().getResource("/reportes/estudiantes/rptEstudiantesPromedios.jasper");
                reporte = (JasperReport) JRLoader.loadObject(in);
                //Se crea un objeto HashMap
                Map parametros = new HashMap();
                parametros.clear();
                parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));
                parametros.put("plan", codigoPlan);
                parametros.put("identificacion", identificacion);
                parametros.put("estudiante", estudiante);
                reporte_view = JasperFillManager.fillReport(reporte, parametros, getConexion());
                JasperViewer.viewReport(reporte_view, false);

            } catch (JRException ex) {
                new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            }
        } else {
            System.out.println("El ResultSet del reporte malo");
        }
        desconectarBD();
    }

    public void cargarReportePromediosPorPeriodos(String codigoPlan, String identificacion, String estudiante) {
        String logotipo = "/reportes/logoinvenio.png";
        conectarBD();
        if (rs != null) {
            JasperReport reporte;
            JasperPrint reporte_view;
            try {
                //direccion del archivo JASPER
                URL in = this.getClass().getResource("/reportes/estudiantes/rptEstudiantesPromediosPorPeriodos.jasper");
                reporte = (JasperReport) JRLoader.loadObject(in);
                //Se crea un objeto HashMap
                Map parametros = new HashMap();
                parametros.clear();
                parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));
                parametros.put("plan", codigoPlan);
                parametros.put("identificacion", identificacion);
                parametros.put("estudiante", estudiante);
                reporte_view = JasperFillManager.fillReport(reporte, parametros, getConexion());
                JasperViewer.viewReport(reporte_view, false);

            } catch (JRException ex) {
                new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            }
        } else {
            System.out.println("El ResultSet del reporte malo");
        }
        desconectarBD();
    }

    public void cargarReporteEstudiantePendientes(String identificacion, String carrera) {
        String logotipo = "/reportes/logoinvenio.png";

        Icon foto = cargarFoto(identificacion);
        File entrada = null;
        if (foto != null) {
            entrada = new File(foto.toString());
        }
        conectarBD();
        JasperReport reporte;
        JasperPrint reporte_view;
        try {
            //direccion del archivo JASPER
            URL in = this.getClass().getResource("/reportes/estudiantes/rptResidenciaEstudiantilPendientes.jasper");
            reporte = (JasperReport) JRLoader.loadObject(in);

            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();
            parametros.put("identificacion", identificacion);
            parametros.put("codigoCarrera", carrera);
            parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));
            if (entrada != null) {
                try {
                    parametros.put("foto", new FileInputStream(entrada));
                } catch (FileNotFoundException ex) {
                    new UseLogger().writeLog(this.getClass().getName(), ex.toString());
                }
            } else {
                parametros.put("foto", this.getClass().getResourceAsStream("/reportes/no-foto.png"));
            }
            //-----------------------------------
            reporte_view = JasperFillManager.fillReport(reporte, parametros, getConexion());
            JasperViewer.viewReport(reporte_view, false);

        } catch (JRException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
        }
        desconectarBD();
    }

}
