/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.estudiante;

/**
 *
 * @author Gloriana
 */
public class clsEstudiante {

    private int tipoIdentificacion;
    private String identificacion;
    private String primerNombre;
    private String segundoNombre;
    private String primerApellido;
    private String segundoApellido;
    private char genero;
    private String fechaNac;
    private int annioIngreso;
    private String fechaIngreso;
    private String periodoIngreso;
    private String ultimoAnnioColegio;
    private String ultimoGradoAcademico;
    private String institucionProcedencia;
    private String correo;
    private String telefonoCasa;
    private String telefonoMovil;
    private String telefonoTrabajo;
    private int provincia;
    private int canton;
    private int distrito;
    private String otrasSenas;
    private String nombreEncargado;
    private String parentesco;
    private String viveCon;
    private String estadoCivil;
    private String nombreMadre;
    private String profesionMadre;
    private String nombrePadre;
    private String profesionPadre;
    private String padecimiento;
    private String medicamentos;
    private String personaEmergencia;
    private String telefonoEmergencia;
    private String carnet;

    public clsEstudiante(int tipoIdentificacion, String identificacion, String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, char genero, String fechaNac, int annioIngreso, String fechaIngreso, String periodoIngreso, String ultimoAnnioColegio, String ultimoGradoAcademico, String institucionProcedencia, String correo, String telefonoCasa, String telefonoMovil, String telefonoTrabajo, int provincia, int canton, int distrito, String otrasSenas, String nombreEncargado, String parentesco, String viveCon, String estadoCivil, String nombreMadre, String profesionMadre, String nombrePadre, String profesionPadre, String padecimiento, String medicamentos, String personaEmergencia, String telefenoEmergencia) {
        this.tipoIdentificacion = tipoIdentificacion;
        this.identificacion = identificacion;
        this.primerNombre = primerNombre;
        this.segundoNombre = segundoNombre;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.genero = genero;
        this.fechaNac = fechaNac;
        this.annioIngreso = annioIngreso;
        this.fechaIngreso = fechaIngreso;
        this.periodoIngreso = periodoIngreso;
        this.ultimoAnnioColegio = ultimoAnnioColegio;
        this.ultimoGradoAcademico = ultimoGradoAcademico;
        this.institucionProcedencia = institucionProcedencia;
        this.correo = correo;
        this.telefonoCasa = telefonoCasa;
        this.telefonoMovil = telefonoMovil;
        this.telefonoTrabajo = telefonoTrabajo;
        this.provincia = provincia;
        this.canton = canton;
        this.distrito = distrito;
        this.otrasSenas = otrasSenas;
        this.nombreEncargado = nombreEncargado;
        this.parentesco = parentesco;
        this.viveCon = viveCon;
        this.estadoCivil = estadoCivil;
        this.nombreMadre = nombreMadre;
        this.profesionMadre = profesionMadre;
        this.nombrePadre = nombrePadre;
        this.profesionPadre = profesionPadre;
        this.padecimiento = padecimiento;
        this.medicamentos = medicamentos;
        this.personaEmergencia = personaEmergencia;
        this.telefonoEmergencia = telefenoEmergencia;
    }

    public clsEstudiante() {
    }

    public int getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(int tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public char getGenero() {
        return genero;
    }

    public void setGenero(char genero) {
        this.genero = genero;
    }

    public String getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(String fechaNac) {
        this.fechaNac = fechaNac;
    }

    public int getAnnioIngreso() {
        return annioIngreso;
    }

    public void setAnnioIngreso(int annioIngreso) {
        this.annioIngreso = annioIngreso;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getPeriodoIngreso() {
        return periodoIngreso;
    }

    public void setPeriodoIngreso(String periodoIngreso) {
        this.periodoIngreso = periodoIngreso;
    }

    public String getUltimoAnnioColegio() {
        return ultimoAnnioColegio;
    }

    public void setUltimoAnnioColegio(String ultimoAnnioColegio) {
        this.ultimoAnnioColegio = ultimoAnnioColegio;
    }

    public String getUltimoGradoAcademico() {
        return ultimoGradoAcademico;
    }

    public void setUltimoGradoAcademico(String ultimoGradoAcademico) {
        this.ultimoGradoAcademico = ultimoGradoAcademico;
    }

    public String getInstitucionProcedencia() {
        return institucionProcedencia;
    }

    public void setInstitucionProcedencia(String institucionProcedencia) {
        this.institucionProcedencia = institucionProcedencia;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefonoCasa() {
        return telefonoCasa;
    }

    public void setTelefonoCasa(String telefonoCasa) {
        this.telefonoCasa = telefonoCasa;
    }

    public String getTelefonoMovil() {
        return telefonoMovil;
    }

    public void setTelefonoMovil(String telefonoMovil) {
        this.telefonoMovil = telefonoMovil;
    }

    public String getTelefonoTrabajo() {
        return telefonoTrabajo;
    }

    public void setTelefonoTrabajo(String telefonoTrabajo) {
        this.telefonoTrabajo = telefonoTrabajo;
    }

    public int getProvincia() {
        return provincia;
    }

    public void setProvincia(int provincia) {
        this.provincia = provincia;
    }

    public int getCanton() {
        return canton;
    }

    public void setCanton(int canton) {
        this.canton = canton;
    }

    public int getDistrito() {
        return distrito;
    }

    public void setDistrito(int distrito) {
        this.distrito = distrito;
    }

    public String getOtrasSenas() {
        return otrasSenas;
    }

    public void setOtrasSenas(String otrasSenas) {
        this.otrasSenas = otrasSenas;
    }

    public String getNombreEncargado() {
        return nombreEncargado;
    }

    public void setNombreEncargado(String nombreEncargado) {
        this.nombreEncargado = nombreEncargado;
    }

    public String getParentesco() {
        return parentesco;
    }

    public void setParentesco(String parentesco) {
        this.parentesco = parentesco;
    }

    public String getViveCon() {
        return viveCon;
    }

    public void setViveCon(String viveCon) {
        this.viveCon = viveCon;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public String getNombreMadre() {
        return nombreMadre;
    }

    public void setNombreMadre(String nombreMadre) {
        this.nombreMadre = nombreMadre;
    }

    public String getProfesionMadre() {
        return profesionMadre;
    }

    public void setProfesionMadre(String profesionMadre) {
        this.profesionMadre = profesionMadre;
    }

    public String getNombrePadre() {
        return nombrePadre;
    }

    public void setNombrePadre(String nombrePadre) {
        this.nombrePadre = nombrePadre;
    }

    public String getProfesionPadre() {
        return profesionPadre;
    }

    public void setProfesionPadre(String profesionPadre) {
        this.profesionPadre = profesionPadre;
    }

    public String getPadecimiento() {
        return padecimiento;
    }

    public void setPadecimiento(String padecimiento) {
        this.padecimiento = padecimiento;
    }

    public String getMedicamentos() {
        return medicamentos;
    }

    public void setMedicamentos(String medicamentos) {
        this.medicamentos = medicamentos;
    }

    public String getPersonaEmergencia() {
        return personaEmergencia;
    }

    public void setPersonaEmergencia(String personaEmergencia) {
        this.personaEmergencia = personaEmergencia;
    }

    public String getTelefonoEmergencia() {
        return telefonoEmergencia;
    }

    public void setTelefonoEmergencia(String telefenoEmergencia) {
        this.telefonoEmergencia = telefenoEmergencia;
    }

    @Override
    public String toString() {
        return "clsEstudiante{" + "tipoIdentificacion=" + tipoIdentificacion + ", identificacion=" + identificacion + ", primerNombre=" + primerNombre + ", segundoNombre=" + segundoNombre + ", primerApellido=" + primerApellido + ", segundoApellido=" + segundoApellido + ", genero=" + genero + ", fechaNac=" + fechaNac + ", annioIngreso=" + annioIngreso + ", fechaIngreso=" + fechaIngreso + ", periodoIngreso=" + periodoIngreso + ", ultimoAnnioColegio=" + ultimoAnnioColegio + ", ultimoGradoAcademico=" + ultimoGradoAcademico + ", institucionProcedencia=" + institucionProcedencia + ", correo=" + correo + ", telefonoCasa=" + telefonoCasa + ", telefonoMovil=" + telefonoMovil + ", telefonoTrabajo=" + telefonoTrabajo + ", provincia=" + provincia + ", canton=" + canton + ", distrito=" + distrito + ", otrasSenas=" + otrasSenas + ", nombreEncargado=" + nombreEncargado + ", parentesco=" + parentesco + ", viveCon=" + viveCon + ", estadoCivil=" + estadoCivil + ", nombreMadre=" + nombreMadre + ", profesionMadre=" + profesionMadre + ", nombrePadre=" + nombrePadre + ", profesionPadre=" + profesionPadre + ", padecimiento=" + padecimiento + ", medicamentos=" + medicamentos + ", personaEmergencia=" + personaEmergencia + ", telefenoEmergencia=" + telefonoEmergencia + '}';
    }

    public String getCarnet() {
        return carnet;
    }

    public void setCarnet(String carnet) {
        this.carnet = carnet;
    }
    
    
}
