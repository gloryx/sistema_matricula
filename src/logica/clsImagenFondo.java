/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.border.Border;
import logica.logger.UseLogger;

/**
 *
 * @author Gloriana
 */
public class clsImagenFondo implements Border {

    BufferedImage fondo;

    public clsImagenFondo(String rutaImgen) {
        try {
            //se obtiene la imagen            
            URL url = new URL(getClass().getResource(rutaImgen).toString());
            fondo = ImageIO.read(url);

        } catch (IOException ex) {
            new UseLogger().writeLog("clsImagenFondo",ex.toString());
        }
    }
    // se sobreescriben metodos propios de Border

    @Override
    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
        //se dibuja la imagen de fondo en el centro del contenedor
        //cada que se redimensione el contenedor, la imagen automaticamente se posiciona en el centro
        g.drawImage(fondo, (x + (width - fondo.getWidth()) / 2), (y + (height - fondo.getHeight()) / 2), null);
    }

    @Override
    public Insets getBorderInsets(Component c) {
        return new Insets(0, 0, 0, 0);
    }

    @Override
    public boolean isBorderOpaque() {
        return true;
    }
}
