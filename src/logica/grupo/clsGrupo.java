/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.grupo;

import java.util.ArrayList;

/**
 *
 * @author Gloriana
 */
public class clsGrupo {

    private String codigo;
    private int numero;
    private String fechaApertura;
    private String identificacion;
    private String profesor;
    private int estado;
    private ArrayList<clsHorario> listaHorarios;

    public clsGrupo() {
        this.listaHorarios = new ArrayList();
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getFechaApertura() {
        return fechaApertura;
    }

    public void setFechaApertura(String fechaApertura) {
        this.fechaApertura = fechaApertura;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public ArrayList<clsHorario> getListaHorarios() {
        return listaHorarios;
    }

    public void setListaHorarios(ArrayList<clsHorario> listaHorarios) {
        this.listaHorarios = listaHorarios;
    }

    public String getProfesor() {
        if (profesor==null) {
            return "POR NOMBRAR";
        } else {
            return profesor;
        }
    }

    public void setProfesor(String profesor) {
        this.profesor = profesor;
    }
}
