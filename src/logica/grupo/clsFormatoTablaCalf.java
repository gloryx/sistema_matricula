/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.grupo;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Gloriana
 */
public class clsFormatoTablaCalf extends DefaultTableCellRenderer {

    private final int columna_patron;

    public clsFormatoTablaCalf(int Colpatron) {
        this.columna_patron = Colpatron;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean selected, boolean focused, int row, int column) {
        setBackground(Color.white);//color de fondo
        table.setForeground(Color.black);//color de texto
        //Si la celda corresponde a una fila con estado FALSE, se cambia el color de fondo a rojo
        int nota = 0;
        try {
            nota = Integer.parseInt(table.getValueAt(row, columna_patron).toString());
        } catch (NullPointerException ex) {
            nota = 0;
        } catch (NumberFormatException e) {
            table.setValueAt("0", row, columna_patron);
        }
        if (nota > 100) {
            nota = 0;
            table.setValueAt(nota, row, columna_patron);
        } else if(nota<0){
            nota=0;
            table.setValueAt(nota, row, columna_patron);
        } else if (nota >= 75) {
            table.setValueAt(nota, row, columna_patron);
            setBackground(new Color(204, 255, 204));
        } else if (nota >= 60) {
            table.setValueAt(nota, row, columna_patron);
            setBackground(new Color(255, 255, 102));
        } else if (nota > 0) {
            table.setValueAt(nota, row, columna_patron);
            setBackground(new Color(255, 186, 186));
        } else {
            table.setValueAt(nota, row, columna_patron);
            setBackground(new Color(255, 255, 255));
        }
        super.getTableCellRendererComponent(table, value, selected, focused, row, column);
        return this;
    }

    public void fillColor(JTable t, JLabel l, boolean isSelected) {
        if (isSelected) {
            l.setBackground(t.getSelectionBackground());
            l.setForeground(t.getSelectionForeground());
        } else {
            l.setBackground(t.getBackground());
            l.setForeground(t.getForeground());
        }
    }
}
