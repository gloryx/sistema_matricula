/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.grupo;

/**
 *
 * @author Gloriana
 */
public class clsHorario {

    private String codigo;
    private String grupo;
    private String dia;
    private String horaInicio;
    int indexHI;
    private String horaFin;
    int indexHF;
    private String aula;

    public clsHorario() {
        this("", "", "", "");

    }

    public clsHorario(String dia, String horaInicio, String horaFin, String aula) {
        this.dia = dia;
        this.horaInicio = horaInicio;
        this.horaFin = horaFin;
        if (aula.equals("POR ASIGNAR")|| aula.equals("")) {
            this.aula = "0000";
        } else {
            this.aula = aula;
        }
    }

    public int getIndexHI() {
        return indexHI;
    }

    public void setIndexHI(int indexHI) {
        this.indexHI = indexHI;
    }

    public int getIndexHF() {
        return indexHF;
    }

    public void setIndexHF(int indexHF) {
        this.indexHF = indexHF;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(String horaFin) {
        this.horaFin = horaFin;
    }

    public String getAula() {
        if (aula.equals("0000")) {
            return "POR ASIGNAR";
        } else {
            return aula;
        }
    }

    public String getCodigoAula() {
        return aula;
    }

    public void setAula(String aula) {
        if (aula.equals("POR ASIGNAR")) {
            this.aula = "0000";
        } else {
            this.aula = aula;
        }
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }
}
