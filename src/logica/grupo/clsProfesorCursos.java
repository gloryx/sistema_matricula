/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.grupo;

/**
 *
 * @author Gloriana
 */
public class clsProfesorCursos {

    private String codigo;
    private String curso;
    private int grupo;
    private String horario;
    private boolean incluir;

    public clsProfesorCursos() {
        this.incluir=true;
    }

    public clsProfesorCursos(String codigo, String curso, int grupo, String horario) {
        this.codigo = codigo;
        this.curso = curso;
        this.grupo = grupo;
        this.horario = horario;
        this.incluir =true;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public int getGrupo() {
        return grupo;
    }

    public void setGrupo(int grupo) {
        this.grupo = grupo;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    String getCodigoGrupo() {
        return codigo + "_" + grupo;
    }

    String getCodigoCurso() {
        return codigo + " " + curso;
    }

    public boolean isIncluir() {
        return incluir;
    }

    public void setIncluir(boolean incluir) {
        this.incluir = incluir;
    }
}
