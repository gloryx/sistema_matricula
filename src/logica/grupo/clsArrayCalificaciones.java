/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.grupo;

import java.util.ArrayList;

/**
 *
 * @author Gloriana
 */
public class clsArrayCalificaciones {
    private String codigo;
    private String grupo;
    ArrayList<clsCalificacion> listaCalificaciones;

    public clsArrayCalificaciones() {
        this.listaCalificaciones=new ArrayList();
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public ArrayList<clsCalificacion> getListaCalificaciones() {
        return listaCalificaciones;
    }

    public void setListaCalificaciones(ArrayList<clsCalificacion> listaCalificaciones) {
        this.listaCalificaciones = listaCalificaciones;
    }    

    public void agregarCalificacion(clsCalificacion calificacion) {
    listaCalificaciones.add(calificacion);
    }
}
