/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.grupo;

import java.awt.HeadlessException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import logica.clsConexion;
import logica.logger.UseLogger;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 *
 * @author Gloriana
 */
public class clsGestorCursoAbierto extends clsConexion {

    public boolean guardarCursoAbierto(clsCursoAbierto cursoAbierto) {
        boolean rpta1 = false, rpta2 = false, rpta3 = false;
        try {
            //Obtenemos la conexion
            conectarBD();
            //Decimos que vamos a crear una transaccion
            getConexion().setAutoCommit(false);

            //Preparamos la sentecia 1 inserccion en tblcursoabiertos
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_guardarCursoAbierto(?,?,?,?,?,?,?)}");
            obj_Procedimiento.setString(1, cursoAbierto.getCodigoCurso());
            obj_Procedimiento.setString(2, cursoAbierto.getCodigoPlan());
            obj_Procedimiento.setInt(3, cursoAbierto.getAnnio());
            obj_Procedimiento.setString(4, cursoAbierto.getPeriodo());
            obj_Procedimiento.setString(5, cursoAbierto.getCodigo());
            obj_Procedimiento.setInt(6, cursoAbierto.getCantidadMax());
            obj_Procedimiento.setInt(7, cursoAbierto.getCantidadMin());

            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta1 = obj_Procedimiento.executeUpdate() == 1;

            //Preparamos la sentecia 2 par insetar todos los grupos
            for (clsGrupo grupo : cursoAbierto.getListaGrupos()) {
                obj_Procedimiento = getConexion().prepareCall("{CALL proc_guardarGrupo(?,?,?,?)}");
                obj_Procedimiento.setString(1, grupo.getCodigo());
                obj_Procedimiento.setInt(2, grupo.getNumero());
                obj_Procedimiento.setString(3, grupo.getFechaApertura());
                obj_Procedimiento.setString(4, grupo.getIdentificacion());
                //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
                //registro de forma correcta los datos
                rpta2 = obj_Procedimiento.executeUpdate() == 1;
                if (!rpta2) {
                    break;
                }
            }
            //Preparamos la sentecia 2 par insetar todos los horarios
            for (clsGrupo grupo : cursoAbierto.getListaGrupos()) {
                for (clsHorario horario : grupo.getListaHorarios()) {
                    obj_Procedimiento = getConexion().prepareCall("{CALL proc_guardarHorarios(?,?,?,?,?,?)}");
                    obj_Procedimiento.setString(1, grupo.getCodigo());
                    obj_Procedimiento.setInt(2, grupo.getNumero());
                    obj_Procedimiento.setString(3, horario.getCodigoAula());
                    obj_Procedimiento.setString(4, horario.getDia());
                    obj_Procedimiento.setString(5, horario.getHoraInicio());
                    obj_Procedimiento.setString(6, horario.getHoraFin());
                    //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
                    //registro de forma correcta los datos
                    rpta3 = obj_Procedimiento.executeUpdate() == 1;
                    if (!rpta3) {
                        break;
                    }
                }
            }

            if (rpta1 && rpta2 && rpta3) {
                //Confirmamos la transaccion
                getConexion().commit();
            } else {
                //Negamos la transaccion
                getConexion().rollback();
            }
            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex) {
                new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            }
            desconectarBD();
        }
        return rpta1 && rpta2 && rpta3;
    }

    public String buscarProfesor(String identificacion) {
        String profesor = "";
        rs = seleccionar("SELECT PRIMERNOMBRE, SEGUNDONOMBRE, PRIMERAPELLIDO, SEGUNDOAPELLIDO FROM tblprofesor WHERE IDENTIFICACION LIKE '" + identificacion + "'");
        try {
            if (rs.next()) {
                profesor += rs.getString(1) + " ";
                profesor += rs.getString(2) + " ";
                profesor += rs.getString(3) + " ";
                profesor += rs.getString(4);
            } else {
                profesor = "POR NOMBRAR";
            }
        } catch (SQLException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        return profesor;
    }

    public boolean guardarGrupoAbierto(clsGrupo grupo) {
        boolean rpta1 = false, rpta2 = false;
        try {
            //Obtenemos la conexion
            conectarBD();
            //Decimos que vamos a crear una transaccion
            getConexion().setAutoCommit(false);

            //Preparamos la sentecia 2 par insetar todos los grupos
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_guardarGrupo(?,?,?,?)}");
            obj_Procedimiento.setString(1, grupo.getCodigo());
            obj_Procedimiento.setInt(2, grupo.getNumero());
            obj_Procedimiento.setString(3, grupo.getFechaApertura());
            obj_Procedimiento.setString(4, grupo.getIdentificacion());
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta1 = obj_Procedimiento.executeUpdate() == 1;

            //Preparamos la sentecia 2 par insetar todos los horarios
            for (clsHorario horario : grupo.getListaHorarios()) {
                obj_Procedimiento = getConexion().prepareCall("{CALL proc_guardarHorarios(?,?,?,?,?,?)}");
                obj_Procedimiento.setString(1, grupo.getCodigo());
                obj_Procedimiento.setInt(2, grupo.getNumero());
                obj_Procedimiento.setString(3, horario.getCodigoAula());
                obj_Procedimiento.setString(4, horario.getDia());
                obj_Procedimiento.setString(5, horario.getHoraInicio());
                obj_Procedimiento.setString(6, horario.getHoraFin());
                //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
                //registro de forma correcta los datos
                rpta2 = obj_Procedimiento.executeUpdate() == 1;
            }

            if (rpta1 && rpta2) {
                //Confirmamos la transaccion
                getConexion().commit();
            } else {
                //Negamos la transaccion
                getConexion().rollback();
            }
            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex) {
                new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            }
            desconectarBD();
        }
        return rpta1 && rpta2;
    }

    public boolean editarGrupoAbierto(clsGrupo grupo) {
        boolean rpta1 = false, rpta2 = false, rpta3 = false;
        try {
            //Obtenemos la conexion
            conectarBD();
            //Decimos que vamos a crear una transaccion
            getConexion().setAutoCommit(false);

            //Preparamos la sentecia 2 par insetar todos los grupos
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_editarGrupo(?,?,?)}");
            obj_Procedimiento.setString(1, grupo.getCodigo());
            obj_Procedimiento.setInt(2, grupo.getNumero());
            obj_Procedimiento.setString(3, grupo.getIdentificacion());
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta1 = obj_Procedimiento.executeUpdate() == 1;
            rpta2 = true;

            //Preparamos la sentecia 2 par insetar todos los horarios
            for (clsHorario horario : grupo.getListaHorarios()) {
                obj_Procedimiento = getConexion().prepareCall("{CALL proc_guardarHorarios(?,?,?,?,?,?)}");
                obj_Procedimiento.setString(1, grupo.getCodigo());
                obj_Procedimiento.setInt(2, grupo.getNumero());
                obj_Procedimiento.setString(3, horario.getCodigoAula());
                obj_Procedimiento.setString(4, horario.getDia());
                obj_Procedimiento.setString(5, horario.getHoraInicio());
                obj_Procedimiento.setString(6, horario.getHoraFin());
                //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
                //registro de forma correcta los datos
                rpta3 = obj_Procedimiento.executeUpdate() == 1;
            }

            if (rpta1 && rpta2 && rpta3) {
                //Confirmamos la transaccion
                getConexion().commit();
            } else {
                //Negamos la transaccion
                getConexion().rollback();
            }
            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex) {
                new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            }
            desconectarBD();
        }
        return rpta1 && rpta2 && rpta3;
    }

    public boolean cambiarCantidadMaxima(String codigo, String nuevaCantidad) {
        boolean rpta1 = false;
        conectarBD();
        try {
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_editarCantidadMax(?,?)}");
            obj_Procedimiento.setString(1, codigo);
            obj_Procedimiento.setString(2, nuevaCantidad);
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta1 = obj_Procedimiento.executeUpdate() == 1;
            desconectarBD();
        } catch (SQLException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        return rpta1;
    }

    public boolean cambiarCantidadMinima(String codigo, String nuevaCantidad) {
        boolean rpta1 = false;
        conectarBD();
        try {
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_editarCantidadMin(?,?)}");
            obj_Procedimiento.setString(1, codigo);
            obj_Procedimiento.setString(2, nuevaCantidad);
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta1 = obj_Procedimiento.executeUpdate() == 1;
            desconectarBD();
        } catch (SQLException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        return rpta1;
    }

    public boolean eliminarGrupoAbierto(String codigo, int numGrupo) {
        boolean rpta1 = false;

        //Preparamos la sentecia 2 par insetar todos los grupos
        try {
            conectarBD();
            //Decimos que vamos a crear una transaccion
            rs = seleccionar("SELECT COUNT(NUMERO) FROM tblcursoabiertosgrupos WHERE CODIGO LIKE '" + codigo + "'");
            int total = 0;
            if (rs.next()) {
                total = rs.getInt(1);
            }
            if (total == 1) {
                obj_Procedimiento = getConexion().prepareCall("{CALL proc_eliminarCursoAbierto(?)}");
                obj_Procedimiento.setString(1, codigo);
            } else {
                obj_Procedimiento = getConexion().prepareCall("{CALL proc_eliminarGrupoAbierto(?,?)}");
                obj_Procedimiento.setString(1, codigo);
                obj_Procedimiento.setInt(2, numGrupo);
            }
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta1 = obj_Procedimiento.executeUpdate() == 1;
            desconectarBD();
        } catch (SQLException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        return rpta1;
    }

    public int obtenerNumeroDeGrupo(String codigo) {
        int numero = 0;
        conectarBD();
        rs = seleccionar("SELECT (MAX(NUMERO)+1) FROM tblcursoabiertosgrupos where codigo like '" + codigo + "'");
        try {
            if (rs.next()) {
                numero = rs.getInt(1);
            }
        } catch (SQLException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        desconectarBD();
        return numero;
    }

    public void cargarReporteCursosAbiertosProfesor(String annio, String periodo, boolean conCant) {
        conectarBD();
        JasperReport reporte;
        JasperPrint reporte_view;
        try {
            //direccion del archivo JASPER
            URL in = null;
            if (conCant) {
                in = this.getClass().getResource("/reportes/matricula/rptMatriculadosCursosAbiertosProfesores.jasper");
            } else {
                in = this.getClass().getResource("/reportes/grupos/rptCursosAbiertosProfesores.jasper");
            }
            reporte = (JasperReport) JRLoader.loadObject(in);

            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();
            parametros.put("titulo", "UNIVERSIDAD DUAL INVENIO - CURSOS ABIERTOS " + periodo + "-" + annio);
            parametros.put("annio", Integer.parseInt(annio));
            parametros.put("periodo", periodo);

            reporte_view = JasperFillManager.fillReport(reporte, parametros, getConexion());
            JasperViewer.viewReport(reporte_view, false);

        } catch (JRException | NullPointerException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        desconectarBD();
    }

    public void cargarReporteCursosAbiertosCursos(String annio, String periodo, boolean conCant) {
        conectarBD();
        JasperReport reporte;
        JasperPrint reporte_view;
        try {
            //direccion del archivo JASPER
            URL in = null;
            if (conCant) {
                in = this.getClass().getResource("/reportes/matricula/rptMatriculadosCursosAbiertosCourse.jasper");
            } else {
                in = this.getClass().getResource("/reportes/grupos/rptCursosAbiertosCourse.jasper");
            }
            reporte = (JasperReport) JRLoader.loadObject(in);

            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();
            parametros.put("titulo", "UNIVERSIDAD DUAL INVENIO - CURSOS ABIERTOS " + periodo + "-" + annio);
            parametros.put("annio", Integer.parseInt(annio));
            parametros.put("periodo", periodo);

            reporte_view = JasperFillManager.fillReport(reporte, parametros, getConexion());
            JasperViewer.viewReport(reporte_view, false);

        } catch (JRException | NullPointerException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        desconectarBD();
    }

    public void cargarReporteCursosAbiertosDesgloce(String codigoPlan, String annio, String periodo, boolean conCant) {
        conectarBD();
        JasperReport reporte;
        JasperPrint reporte_view;
        try {
            //direccion del archivo JASPER
            URL in = null;
            if (conCant) {
                in = this.getClass().getResource("/reportes/matricula/rptMatriculadosCursosAbiertosDesgloce.jasper");
            } else {
                in = this.getClass().getResource("/reportes/grupos/rptCursosAbiertosDesgloce.jasper");
            }
            reporte = (JasperReport) JRLoader.loadObject(in);

            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();
            parametros.put("titulo", "UNIVERSIDAD DUAL INVENIO - CURSOS ABIERTOS " + periodo + "-" + annio);
            parametros.put("codigoplan", codigoPlan);
            parametros.put("annio", Integer.parseInt(annio));
            parametros.put("periodo", periodo);

            reporte_view = JasperFillManager.fillReport(reporte, parametros, getConexion());
            JasperViewer.viewReport(reporte_view, false);

        } catch (JRException | NullPointerException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        desconectarBD();
    }

    public void cargarReporteCursosAbiertosGrafico(String annio, String periodo) {
        conectarBD();
        JasperReport reporte;
        JasperPrint reporte_view;
        try {
            //direccion del archivo JASPER
            URL in = this.getClass().getResource("/reportes/grupos/rptGraficosCursosAbiertos.jasper");
            reporte = (JasperReport) JRLoader.loadObject(in);

            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();
            parametros.put("titulo", "UNIVERSIDAD DUAL INVENIO - CURSOS ABIERTOS " + periodo + "-" + annio);
            parametros.put("annio", Integer.parseInt(annio));
            parametros.put("periodo", periodo);

            reporte_view = JasperFillManager.fillReport(reporte, parametros, getConexion());
            JasperViewer.viewReport(reporte_view, false);

        } catch (JRException | NullPointerException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        desconectarBD();
    }

    public int contarMatriculados(String codigo, int numero) {
        int total = 0;
        try {
            //Nombre del procedimiento almacenado            
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_contarMatriculados(?,?)}");
            obj_Procedimiento.setString(1, codigo);
            obj_Procedimiento.setInt(2, numero);
            rs = obj_Procedimiento.executeQuery();
            if (rs.next()) {
                total = rs.getInt(1);
            }
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return total;

    }

    public ResultSet cargarCursosAbiertosPlan(String plan, String annio, String periodo) {
        try {
            //Nombre del procedimiento almacenado
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_cargarCursosAbiertos(?,?,?)}");
            obj_Procedimiento.setString(1, plan);
            obj_Procedimiento.setString(2, annio);
            obj_Procedimiento.setString(3, periodo);
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    public ResultSet cargarCursosConNotasRegistradas(String plan, String annio, String periodo, int estado) {
        try {
            //Nombre del procedimiento almacenado
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_cargarCursosConNotasRegistradas(?,?,?,?)}");
            obj_Procedimiento.setString(1, plan);
            obj_Procedimiento.setString(2, annio);
            obj_Procedimiento.setString(3, periodo);
            obj_Procedimiento.setInt(4, estado);
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    public ResultSet cargarCursosConNotasRegistradasTodas(String annio, String periodo, int estado) {
        try {
            //Nombre del procedimiento almacenado
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_cargarCursosConNotasRegistradasTodas(?,?,?)}");
            obj_Procedimiento.setString(1, annio);
            obj_Procedimiento.setString(2, periodo);
            obj_Procedimiento.setInt(3, estado);
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }
//==============================================================================

    public ResultSet cargarCursosAbiertosProfesor(String plan, String annio, String periodo) {
        try {
            //Nombre del procedimiento almacenado
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_cargarCursosAbiertosProfesor(?,?,?)}");
            obj_Procedimiento.setString(1, plan);
            obj_Procedimiento.setString(2, annio);
            obj_Procedimiento.setString(3, periodo);
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }
    //==========================================================================

    public ResultSet cargarCursosAbiertosProfesorList(String plan, String profesor, String annio, String periodo) {
        try {
            //Nombre del procedimiento almacenado
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_cargarCursosAbiertosProfesorList(?,?,?,?)}");
            obj_Procedimiento.setString(1, plan);
            obj_Procedimiento.setString(2, profesor);
            obj_Procedimiento.setString(3, annio);
            obj_Procedimiento.setString(4, periodo);
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }
    //=====================================================================

    public void imprimirCursosEstudiantes(String codigo, String curso, String periodo,
            String carrera, int numero, String horario, String profesor) {
        String logotipo = "/reportes/logoinvenio.png";
        conectarBD();
        JasperReport reporte;
        JasperPrint reporte_view;
        try {
            //direccion del archivo JASPER
            URL in = this.getClass().getResource("/reportes/grupos/rptListaDeEstudiantesGrupos.jasper");
            reporte = (JasperReport) JRLoader.loadObject(in);

            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();

            parametros.put("codigo", codigo);
            parametros.put("curso", curso);
            parametros.put("periodo", periodo);
            parametros.put("carrera", carrera);
            parametros.put("profesor", profesor);
            parametros.put("grupo", String.valueOf(numero));
            parametros.put("horario", horario);
            parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));

            reporte_view = JasperFillManager.fillReport(reporte, parametros, getConexion());
            JasperViewer.viewReport(reporte_view, false);

        } catch (JRException | NullPointerException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        desconectarBD();
    }

    public void exportarListaEstudiantesExcel(String codigo, String curso, String periodo,
            int numero, String horario, String profesor) {
        String logotipo = "/reportes/logoinvenio.png";
        conectarBD();
        JasperReport reporte;
        JasperPrint reporte_view;
        try {
            //direccion del archivo JASPER
            URL in = this.getClass().getResource("/reportes/grupos/rptListaDeEstudiantesGrupos.jasper");
            reporte = (JasperReport) JRLoader.loadObject(in);

            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();

            parametros.put("codigo", codigo);
            parametros.put("curso", curso);
            parametros.put("periodo", periodo);
            parametros.put("profesor", profesor);
            parametros.put("grupo", String.valueOf(numero));
            parametros.put("horario", horario);
            parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));

            reporte_view = JasperFillManager.fillReport(reporte, parametros, getConexion());
            JasperViewer.viewReport(reporte_view, false);

        } catch (JRException | NullPointerException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        desconectarBD();
    }

    public void imprimirCursosEstudiantesConFoto(String codigo, String curso, String periodo,
            int numero, String horario, String profesor) {
        String logotipo = "/reportes/logoinvenio.png";

        JasperReport reporte;
        JasperPrint reporte_view;
        try {
            //direccion del archivo JASPER
            URL in = this.getClass().getResource("/reportes/grupos/rptListaDeEstudiantesConFoto.jasper");
            reporte = (JasperReport) JRLoader.loadObject(in);

            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();

            parametros.put("codigo", codigo);
            parametros.put("curso", curso);
            parametros.put("periodo", periodo);
            parametros.put("profesor", profesor);
            parametros.put("grupo", String.valueOf(numero));
            parametros.put("horario", horario);
            parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));

            //==================================================================
            clsGestorEstudianteDataSource datasource = new clsGestorEstudianteDataSource();
            clsEstudianteFoto estudianteFoto;
            conectarBD();
            String nombre1, nombre2, apellido1, apellido2;
            rs = cargarEstudianteConFoto(codigo, numero);
            if (rs != null) {
                while (rs.next()) {
                    estudianteFoto = new clsEstudianteFoto();
                    estudianteFoto.setIdentificacion(rs.getString(1));
                    nombre1 = rs.getString(2);
                    nombre2 = rs.getString(3);
                    apellido1 = rs.getString(4);
                    apellido2 = rs.getString(5);
                    estudianteFoto.setNombreCompleto(apellido1 + " " + apellido2
                            + " " + nombre1 + " " + nombre2);
                    estudianteFoto.setMovil(rs.getString(6));
                    estudianteFoto.setCasa(rs.getString(7));
                    estudianteFoto.setTrabajo(rs.getString(8));
                    estudianteFoto.setCorreo(rs.getString(9));
                    Icon foto = cargarFoto(estudianteFoto.getIdentificacion());
                    File entrada = null;
                    if (foto != null) {
                        entrada = new File(foto.toString());
                    }

                    estudianteFoto.setFoto(entrada);

                    datasource.addclsEstudianteFoto(estudianteFoto);
                }

            }
            desconectarBD();
            //==================================================================
            parametros.put("total", datasource.getTotal());
            reporte_view = JasperFillManager.fillReport(reporte, parametros, datasource);
            JasperViewer.viewReport(reporte_view, false);

        } catch (JRException | NullPointerException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        } catch (SQLException ex) {
            Logger.getLogger(clsGestorCursoAbierto.class.getName()).log(Level.SEVERE, null, ex);
        }
        desconectarBD();
    }

    public ResultSet cargarEstudianteConFoto(String codigo, int numero) {
        String sql = "SELECT tblestudiante.IDENTIFICACION "
                + ", tblestudiante.PRIMERNOMBRE "
                + ", tblestudiante.SEGUNDONOMBRE "
                + ", tblestudiante.PRIMERAPELLIDO "
                + ", tblestudiante.SEGUNDOAPELLIDO "
                + ", tblestudiante.TELEFONOCELULAR "
                + ", tblestudiante.TELEFONOCASA "
                + ", tblestudiante.TELEFONOTRABAJO "
                + ", tblestudiante.CORREOELECTRONICO "
                + "FROM tblmatricula INNER JOIN tblestudiante "
                + "ON tblmatricula.identificacion = tblestudiante.IDENTIFICACION "
                + "INNER JOIN tbldetallematricula "
                + "ON tbldetallematricula.nummatricula = tblmatricula.nummatricula "
                + "WHERE tbldetallematricula.codigo LIKE '" + codigo + "' "
                + "AND tbldetallematricula.grupo =" + numero + " "
                + "ORDER BY tblestudiante.PRIMERAPELLIDO ";
        rs = seleccionar(sql);
        return rs;

    }

    public Icon cargarFoto(String identificacion) {
        Icon foto = null;
        File dir = new File(System.getProperty("user.dir") + "\\fotos\\");
        String[] ficheros = dir.list();
        if (ficheros == null) {
            System.out.println("No hay ficheros en el directorio especificado");
        } else {
            for (String fichero : ficheros) {
                if (fichero.startsWith(identificacion)) {
                    foto = new ImageIcon(System.getProperty("user.dir") + "\\fotos\\" + fichero);
                    break;
                }
            }
        }
        return foto;
    }

    public ResultSet cargarEstudiantesCalificaciones(String codigo, String grupo) {
        try {
            //Nombre del procedimiento almacenado
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_buscarCalificaciones(?,?)}");
            obj_Procedimiento.setString(1, codigo);
            obj_Procedimiento.setString(2, grupo);
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    public boolean guardarCalificaciones(clsArrayCalificaciones listaCalificaciones) {
        boolean rpta1 = false, rpta2 = false;
        try {
            //Obtenemos la conexion
            conectarBD();
            //Decimos que vamos a crear una transaccion
            getConexion().setAutoCommit(false);

            //Preparamos la sentecia 1 par insetar todas las calificaciones
            for (clsCalificacion calif : listaCalificaciones.getListaCalificaciones()) {
                obj_Procedimiento = getConexion().prepareCall("{CALL proc_editarCalificacion(?,?,?,?)}");
                obj_Procedimiento.setString(1, listaCalificaciones.getCodigo());
                obj_Procedimiento.setString(2, listaCalificaciones.getGrupo());
                obj_Procedimiento.setString(3, calif.getIdentificacion());
                obj_Procedimiento.setString(4, calif.getCalificacion());
                rpta1 = obj_Procedimiento.executeUpdate() == 1;
                if (!rpta1) {
                    break;
                }
            }
            stmt = conexion.createStatement();
            String sql = "update tblcursoabiertosgrupos set estado = 2 where codigo like '"
                    + listaCalificaciones.getCodigo() + "' and numero = " + listaCalificaciones.getGrupo();
            rpta2 = stmt.executeUpdate(sql) == 1;
            if (rpta1 & rpta2) {
                //Confirmamos la transaccion
                getConexion().commit();
            } else {
                //Negamos la transaccion
                getConexion().rollback();
            }
            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex) {
                new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            }
            desconectarBD();
        }
        return rpta1;
    }

    public void imprimirCursosEstudiantesNotas(String codigo, String curso, String periodo, String carrera, String grupo,
            String horario, String profesor) {
        String logotipo = "/reportes/logoinvenio.png";
        conectarBD();
        JasperReport reporte;
        JasperPrint reporte_view;
        try {
            //direccion del archivo JASPER
            URL in = this.getClass().getResource("/reportes/grupos/rptListaDeEstudiantesGruposNotas.jasper");
            reporte = (JasperReport) JRLoader.loadObject(in);

            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();

            parametros.put("codigo", codigo);
            parametros.put("curso", curso);
            parametros.put("periodo", periodo);
            parametros.put("carrera", carrera);
            parametros.put("profesor", profesor);
            parametros.put("grupo", grupo);
            parametros.put("horario", horario);
            parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));

            reporte_view = JasperFillManager.fillReport(reporte, parametros, getConexion());
            JasperViewer.viewReport(reporte_view, false);

        } catch (JRException | NullPointerException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        desconectarBD();
    }

    public boolean cambiarCalificacion(String identificacion, String codigo, String grupo, int nota) {
        boolean rpta1 = false;
        conectarBD();
        try {

            obj_Procedimiento = getConexion().prepareCall("{CALL proc_editarCalificacion(?,?,?,?)}");
            obj_Procedimiento.setString(1, codigo);
            obj_Procedimiento.setString(2, grupo);
            obj_Procedimiento.setString(3, identificacion);
            obj_Procedimiento.setInt(4, nota);
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta1 = obj_Procedimiento.executeUpdate() == 1;
            desconectarBD();
        } catch (SQLException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        return rpta1;
    }

    //nueva funcionalidad a exportar a excel
    public void generarArchivoExcel(String codigo, String curso, String periodo,
            int numero, String horario, String profesor, String nombre) {

        try {
            HSSFWorkbook wb = new HSSFWorkbook();
            HSSFSheet sheet = wb.createSheet(nombre);
            HSSFRow row = sheet.createRow(0);
            row.createCell(0).setCellValue("UNIVERSIDAD DUAL INVENIO");
            sheet.addMergedRegion(new org.apache.poi.ss.util.CellRangeAddress(6, 6, 0, 4));
            row = sheet.createRow(1);
            row.createCell(0).setCellValue("PERIODO: " + periodo);
            row = sheet.createRow(2);
            row.createCell(0).setCellValue("CURSO: " + curso);
            row.createCell(4).setCellValue("GRUPO #" + numero);

            row = sheet.createRow(3);
            row.createCell(0).setCellValue("FACILITADOR: " + profesor);

            row = sheet.createRow(4);
            row.createCell(0).setCellValue("HORARIO: " + horario);
            row = sheet.createRow(5);
            row = sheet.createRow(6);
            row.createCell(0).setCellValue("LISTA DE ESTUDIANTES");

            String sql = "SELECT "
                    + "     tblestudiante.`IDENTIFICACION`, "
                    + "     CONCAT(PRIMERAPELLIDO,' ' ,SEGUNDOAPELLIDO, ' ',PRIMERNOMBRE,' ',SEGUNDONOMBRE) AS ESTUDIANTE, "
                    + "     tblestudiante.`TELEFONOCELULAR` AS CELULAR, "
                    + "     tblestudiante.`TELEFONOCASA` AS CASA, "
                    + "     tblestudiante.`CORREOELECTRONICO`AS CORREO "
                    + "FROM "
                    + "     `tblmatricula` tblmatricula INNER JOIN `tblestudiante` tblestudiante ON tblmatricula.`identificacion` = tblestudiante.`IDENTIFICACION` "
                    + "     INNER JOIN `tbldetallematricula` tbldetallematricula ON tblmatricula.`nummatricula` = tbldetallematricula.`nummatricula` "
                    + "WHERE "
                    + "     tbldetallematricula.codigo LIKE '" + codigo + "' "
                    + "     and tbldetallematricula.grupo = " + numero
                    + " ORDER BY "
                    + "     tblestudiante.PRIMERAPELLIDO ASC";
            conectarBD();
            rs = seleccionar(sql);
            ResultSetMetaData Datos = rs.getMetaData();
            row = sheet.createRow(7);
            for (int i = 0; i < Datos.getColumnCount(); i++) {
                row.createCell(i).setCellValue(Datos.getColumnName(i + 1));
            }

            int fil = 8;
            Object dato;
            while (rs.next()) {
                row = sheet.createRow(fil++);
                for (int col = 0; col < Datos.getColumnCount(); col++) {
                    try {
                        dato = rs.getObject(col + 1);
                        row.createCell(col).setCellValue((String) dato);
                    } catch (SQLException e) {
                        row.createCell(col).setCellValue("");
                    }
                }
            }
            JFileChooser fc = new JFileChooser();

            fc.setSelectedFile(new File(nombre + ".xls"));
            int returnVal = fc.showSaveDialog(null);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                try (FileOutputStream fileOut = new FileOutputStream(file.getPath())) {
                    wb.write(fileOut);
                }

                //Ejecutar archivo de excel
//                String dir=file.getPath();
//                Runtime.getRuntime().exec("cmd /c start " +dir);
                JOptionPane.showMessageDialog(null, "Archivo " + fc.getSelectedFile().getName() + " guarado exitosamente");

            }
        } catch (SQLException | HeadlessException | IOException e) {
            javax.swing.JOptionPane.showMessageDialog(null, "Error al exportar la consulta: " + e);
            desconectarBD();
        }
        desconectarBD();
    }

    public void generarArchivoExcelTodos(String periodo, String annio, String plan, String profesor, ArrayList<clsProfesorCursos> lista) {
        try {
            HSSFWorkbook wb = new HSSFWorkbook();
            conectarBD();
            for (clsProfesorCursos p : lista) {
                if (p.isIncluir()) {
                    HSSFSheet sheet = wb.createSheet(p.getCodigoGrupo());
                    HSSFRow row = sheet.createRow(0);
                    row.createCell(0).setCellValue("UNIVERSIDAD DUAL INVENIO");
                    sheet.addMergedRegion(new org.apache.poi.ss.util.CellRangeAddress(6, 6, 0, 4));
                    row = sheet.createRow(1);
                    row.createCell(0).setCellValue("PERIODO: " + periodo + "-" + annio);
                    row = sheet.createRow(2);
                    row.createCell(0).setCellValue("CURSO: " + p.getCodigoCurso());
                    row.createCell(4).setCellValue("GRUPO #" + p.getGrupo());

                    row = sheet.createRow(3);
                    row.createCell(0).setCellValue("FACILITADOR: " + profesor);

                    row = sheet.createRow(4);
                    row.createCell(0).setCellValue("HORARIO: " + p.getHorario());
                    row = sheet.createRow(5);
                    row = sheet.createRow(6);
                    row.createCell(0).setCellValue("LISTA DE ESTUDIANTES");

                    String sql = "SELECT "
                            + "     tblestudiante.`IDENTIFICACION`, "
                            + "     CONCAT(PRIMERAPELLIDO,' ' ,SEGUNDOAPELLIDO, ' ',PRIMERNOMBRE,' ',SEGUNDONOMBRE) AS ESTUDIANTE, "
                            + "     tblestudiante.`TELEFONOCELULAR` AS CELULAR, "
                            + "     tblestudiante.`TELEFONOCASA` AS CASA, "
                            + "     tblestudiante.`CORREOELECTRONICO`AS CORREO "
                            + "FROM "
                            + "     `tblmatricula` tblmatricula INNER JOIN `tblestudiante` tblestudiante ON tblmatricula.`identificacion` = tblestudiante.`IDENTIFICACION` "
                            + "     INNER JOIN `tbldetallematricula` tbldetallematricula ON tblmatricula.`nummatricula` = tbldetallematricula.`nummatricula` "
                            + "WHERE "
                            + "     tbldetallematricula.codigo LIKE '" + annio + periodo + p.getCodigo() + plan + "' "
                            + "     and tbldetallematricula.grupo = " + p.getGrupo()
                            + " ORDER BY "
                            + "     tblestudiante.PRIMERAPELLIDO ASC";

                    rs = seleccionar(sql);
                    ResultSetMetaData Datos = rs.getMetaData();
                    row = sheet.createRow(7); // a partir de la fila 7 encabezados de fila
                    for (int i = 0; i < Datos.getColumnCount(); i++) {
                        row.createCell(i).setCellValue(Datos.getColumnName(i + 1));
                    }
                    int fil = 8; // a partir de la fila 8 llena la lista de estudiantes
                    Object dato;
                    while (rs.next()) {
                        row = sheet.createRow(fil++);
                        for (int col = 0; col < Datos.getColumnCount(); col++) {
                            try {
                                dato = rs.getObject(col + 1);
                                row.createCell(col).setCellValue((String) dato);
                            } catch (SQLException e) {
                                row.createCell(col).setCellValue("");
                            }
                        }
                    }
                }
            }
            desconectarBD();
            JFileChooser fc = new JFileChooser();

            fc.setSelectedFile(new File(profesor + ".xls"));
            int returnVal = fc.showSaveDialog(null);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                try (FileOutputStream fileOut = new FileOutputStream(file.getPath())) {
                    wb.write(fileOut);
                }

                //Ejecutar archivo de excel
//                String dir=file.getPath();
//                Runtime.getRuntime().exec("cmd /c start " +dir);
                JOptionPane.showMessageDialog(null, "Archivo " + fc.getSelectedFile().getName() + " guarado exitosamente");

            }
        } catch (SQLException | HeadlessException | IOException e) {
            javax.swing.JOptionPane.showMessageDialog(null, "Error al exportar la consulta: " + e);
            desconectarBD();
        }
    }

    //==========================================================================
    public void generarArchivoPendientesMatricular(DefaultTableModel modelo, String codigo, String curso) {
        try {
            HSSFWorkbook wb = new HSSFWorkbook();
            HSSFSheet sheet = wb.createSheet(codigo);
            HSSFRow row = sheet.createRow(0);
            row.createCell(0).setCellValue("INSTITUTO DE TECNOLOGÍAS EMERGENTES INVENIO");
            sheet.addMergedRegion(new org.apache.poi.ss.util.CellRangeAddress(6, 6, 0, 4));
            row = sheet.createRow(1);
            row.createCell(0).setCellValue("CODIGO: " + codigo);
            row = sheet.createRow(2);
            row.createCell(0).setCellValue("CURSO: " + curso);
            row = sheet.createRow(3);
            row = sheet.createRow(4);
            row.createCell(0).setCellValue("LISTA DE ESTUDIANTES CANDIDATOS A MATRICULAR");

            row = sheet.createRow(7);
            for (int i = 0; i < modelo.getColumnCount(); i++) {
                row.createCell(i).setCellValue(modelo.getColumnName(i));
            }

            int fil = 8;
            for (int i = 0; i < modelo.getRowCount(); i++) {
                row = sheet.createRow(fil++);
                for (int col = 0; col < modelo.getColumnCount(); col++) {
                    try {
                        row.createCell(col).setCellValue(modelo.getValueAt(i, col).toString());
                    } catch (Exception e) {
                        row.createCell(col).setCellValue("");
                    }
                }
            }
            JFileChooser fc = new JFileChooser();

            fc.setSelectedFile(new File(codigo + ".xls"));
            int returnVal = fc.showSaveDialog(null);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                //This is where a real application would open the file.
                FileOutputStream fileOut = new FileOutputStream(file.getPath());
                wb.write(fileOut);
                fileOut.close();

                //Ejecutar archivo de excel
//                String dir=file.getPath();
//                Runtime.getRuntime().exec("cmd /c start " +dir);
                JOptionPane.showMessageDialog(null, "Archivo " + fc.getSelectedFile().getName() + " guarado exitosamente");

            }
        } catch (Exception e) {
            javax.swing.JOptionPane.showMessageDialog(null, "Error al exportar la consulta: " + e);
            desconectarBD();
        }

    }

    public ResultSet cargarEstudiantesPendientesCurso(String plan, String codigo) {
        try {
            //Nombre del procedimiento almacenado
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_buscarEstudiantesCursoPendiente(?,?)}");
            obj_Procedimiento.setString(1, plan);
            obj_Procedimiento.setString(2, codigo);
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    //==========================================================================
    public void generarArchivoExcelProfesores(String annio, String periodo, String nombre) {

        try {
            HSSFWorkbook wb = new HSSFWorkbook();
            HSSFSheet sheet = wb.createSheet(nombre);
            HSSFRow row;

            String sql = "SELECT DISTINCT\n"
                    + "     tblcursos.CODIGOPLAN,\n"
                    + "     concat(tblprofesor.`PRIMERAPELLIDO`,' ',tblprofesor.`SEGUNDOAPELLIDO`,' ',\n"
                    + "     tblprofesor.`PRIMERNOMBRE` ,' ',tblprofesor.`SEGUNDONOMBRE` ) AS PROFESOR,\n"
                    + "     CONCAT(tblcursosabiertos.`CODIGOCURSO`,' ' ,tblcursos.`NOMBRECURSO`) AS CURSO,\n"
                    + "     tblcursosabiertoshorarios.`NUMERO` AS GRUPO,\n"
                    + "     group_concat(concat('[',`tblcursosabiertoshorarios`.`DIA`,'(',`tblcursosabiertoshorarios`.`HORAINICIO`,'-',`tblcursosabiertoshorarios`.`HORAFIN`,')] EA: ',`tblcursosabiertoshorarios`.CODIGOAULA)SEPARATOR '-') AS `HORARIO`\n"
                    + "     \n"
                    + "FROM\n"
                    + "     `tblcursosabiertoshorarios` tblcursosabiertoshorarios INNER JOIN `tblcursoabiertosgrupos` tblcursoabiertosgrupos ON tblcursosabiertoshorarios.`CODIGO` = tblcursoabiertosgrupos.`CODIGO`\n"
                    + "     AND tblcursosabiertoshorarios.`NUMERO` = tblcursoabiertosgrupos.`NUMERO`\n"
                    + "     INNER JOIN `tblcursosabiertos` tblcursosabiertos ON tblcursoabiertosgrupos.`CODIGO` = tblcursosabiertos.`CODIGO`\n"
                    + "     INNER JOIN `tblprofesor` tblprofesor ON tblcursoabiertosgrupos.`PROFESOR` = tblprofesor.`IDENTIFICACION`\n"
                    + "     INNER JOIN `tblcursos` tblcursos ON tblcursosabiertos.`CODIGOCURSO` = tblcursos.`CODIGOCURSO`\n"
                    + "     AND tblcursos.`CODIGOPLAN` = tblcursosabiertos.`CODIGOPLAN`\n"
                    + "WHERE ANNIO = "+annio+" AND PERIODO LIKE '"+periodo+"' \n"
                    + "GROUP BY\n"
                    + "     `tblcursosabiertoshorarios`.`CODIGO`,\n"
                    + "     `tblcursosabiertoshorarios`.`NUMERO`\n"
                    + "ORDER BY\n"
                    + "     tblcursosabiertos.CODIGOPLAN,\n"
                    + "     tblprofesor.PRIMERAPELLIDO,\n"
                    + "     tblcursoabiertosgrupos.NUMERO ASC,\n"
                    + "     tblcursosabiertos.codigocurso";
            conectarBD();
            rs = seleccionar(sql);
            ResultSetMetaData Datos = rs.getMetaData();
            row = sheet.createRow(0);
            for (int i = 0; i < Datos.getColumnCount(); i++) {
                row.createCell(i).setCellValue(Datos.getColumnName(i + 1));
            }

            int fil = 1;
            Object dato;
            while (rs.next()) {
                row = sheet.createRow(fil++);
                for (int col = 0; col < Datos.getColumnCount(); col++) {
                    try {
                        dato = rs.getObject(col + 1);
                        row.createCell(col).setCellValue((String) dato.toString());
                    } catch (SQLException e) {
                        row.createCell(col).setCellValue("");
                    }
                }
            }
            JFileChooser fc = new JFileChooser();

            fc.setSelectedFile(new File(nombre + ".xls"));
            int returnVal = fc.showSaveDialog(null);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                try (FileOutputStream fileOut = new FileOutputStream(file.getPath())) {
                    wb.write(fileOut);
                }

                //Ejecutar archivo de excel
//                String dir=file.getPath();
//                Runtime.getRuntime().exec("cmd /c start " +dir);
                JOptionPane.showMessageDialog(null, "Archivo " + fc.getSelectedFile().getName() + " guarado exitosamente");

            }
        } catch (SQLException | HeadlessException | IOException e) {
            javax.swing.JOptionPane.showMessageDialog(null, "Error al exportar la consulta: " + e);
            desconectarBD();
        }
        desconectarBD();
    }

}
