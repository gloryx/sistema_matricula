/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.grupo;

/**
 *
 * @author Gloriana
 */
public class clsCalificacion {
    private String identificacion;
    private String calificacion;

    public clsCalificacion() {
    }

    public clsCalificacion(String identificacion, String calificacion) {
        this.identificacion = identificacion;
        this.calificacion = calificacion;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(String calificacion) {
        this.calificacion = calificacion;
    }
    
}
