/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.grupo;

import java.util.ArrayList;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

/**
 *
 * @author Gloriana
 */
public class clsGestorEstudianteDataSource implements JRDataSource{

    private List<clsEstudianteFoto> listaclsEstudianteFotos = new ArrayList<>();
    private int indiceclsEstudianteFotoActual = -1;

    @Override
    public boolean next() throws JRException {
        return ++indiceclsEstudianteFotoActual < listaclsEstudianteFotos.size();
    }

    @Override
    public Object getFieldValue(JRField jrField) throws JRException {
        Object valor = null;
        switch (jrField.getName()) {
            case "identificacion":
                valor = listaclsEstudianteFotos.get(indiceclsEstudianteFotoActual).getIdentificacion();
                break;
            case "nombreCompleto":
                valor = listaclsEstudianteFotos.get(indiceclsEstudianteFotoActual).getNombreCompleto();
                break;
            case "movil":
                valor = listaclsEstudianteFotos.get(indiceclsEstudianteFotoActual).getMovil();
                break;
            case "casa":
                valor = listaclsEstudianteFotos.get(indiceclsEstudianteFotoActual).getCasa();
                break;
            case "trabajo":
                valor = listaclsEstudianteFotos.get(indiceclsEstudianteFotoActual).getTrabajo();
                break;
            case "correo":
                valor = listaclsEstudianteFotos.get(indiceclsEstudianteFotoActual).getCorreo();
                break;
            case "foto":
                valor = listaclsEstudianteFotos.get(indiceclsEstudianteFotoActual).getFoto();
                break;
                        }

        return valor;
    }

    public void addclsEstudianteFoto(clsEstudianteFoto estudianteFoto) {
        this.listaclsEstudianteFotos.add(estudianteFoto);
    }

    public int getTotal() {
        return listaclsEstudianteFotos.size();
    }
   
    
}
