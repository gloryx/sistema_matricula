/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.grupo;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Gloriana
 */
public class clsFormatoTabla extends DefaultTableCellRenderer {

    private final int columna_patron;

    public clsFormatoTabla(int Colpatron) {
        this.columna_patron = Colpatron;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean selected, boolean focused, int row, int column) {
        setBackground(Color.white);//color de fondo
        table.setForeground(Color.black);//color de texto
        //Si la celda corresponde a una fila con estado FALSE, se cambia el color de fondo a rojo
        String valor = table.getValueAt(row, columna_patron).toString();
        int nivel = Integer.parseInt(valor);
        if (nivel % 2 == 0) {

            setBackground(new Color(255, 255, 204));
        } else {
            setBackground(new Color(204, 255, 204));
        }

        super.getTableCellRendererComponent(table, value, selected, focused, row, column);
        return this;
    }

    public void fillColor(JTable t, JLabel l, boolean isSelected) {
        if (isSelected) {
            l.setBackground(t.getSelectionBackground());
            l.setForeground(t.getSelectionForeground());
        } else {
            l.setBackground(t.getBackground());
            l.setForeground(t.getForeground());
        }
    }
}
