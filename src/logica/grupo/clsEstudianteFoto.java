/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.grupo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import logica.logger.UseLogger;

/**
 *
 * @author Gloriana
 */
public class clsEstudianteFoto {
    String identificacion;
    String nombreCompleto;
    String movil;
    String casa;
    String trabajo;
    String correo;
    private  FileInputStream foto;

    public clsEstudianteFoto() {
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getMovil() {
        return movil;
    }

    public void setMovil(String movil) {
        this.movil = movil;
    }

    public String getCasa() {
        return casa;
    }

    public void setCasa(String casa) {
        this.casa = casa;
    }

    public String getTrabajo() {
        return trabajo;
    }

    public void setTrabajo(String trabajo) {
        this.trabajo = trabajo;
    }

    public FileInputStream getFoto() {
        return foto;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public void setFoto(File entrada) {
        if (entrada != null) {
                try {
                    foto= new FileInputStream(entrada);
                } catch (FileNotFoundException ex) {
                    new UseLogger().writeLog(this.getClass().getName(), ex.toString());
                }
            }
    }

}
