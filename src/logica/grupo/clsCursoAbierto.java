/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.grupo;

import java.util.ArrayList;

/**
 *
 * @author Gloriana
 */
public class clsCursoAbierto {

    private String codigoCurso;
    private String codigoPlan;
    private int annio;
    private String periodo;
    private String codigo;
    private int cantidadMax;
    private int cantidadMin;
    private int estado;
    private ArrayList<clsGrupo> listaGrupos;

    public clsCursoAbierto() {
        listaGrupos = new ArrayList();
    }

    public String getCodigoCurso() {
        return codigoCurso;
    }

    public void setCodigoCurso(String codigoCurso) {
        this.codigoCurso = codigoCurso;
    }

    public String getCodigoPlan() {
        return codigoPlan;
    }

    public void setCodigoPlan(String codigoPlan) {
        this.codigoPlan = codigoPlan;
    }

    public int getAnnio() {
        return annio;
    }

    public void setAnnio(int annio) {
        this.annio = annio;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getCantidadMax() {
        return cantidadMax;
    }

    public void setCantidadMax(int cantidadMax) {
        this.cantidadMax = cantidadMax;
    }

    public int getCantidadMin() {
        return cantidadMin;
    }

    public void setCantidadMin(int cantidadMin) {
        this.cantidadMin = cantidadMin;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public ArrayList<clsGrupo> getListaGrupos() {
        return listaGrupos;
    }

    public void setListaGrupos(ArrayList<clsGrupo> listaGrupos) {
        this.listaGrupos = listaGrupos;
    }

   
}
