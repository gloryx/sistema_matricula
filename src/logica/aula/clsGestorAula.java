/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.aula;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import logica.clsConexion;
import logica.logger.UseLogger;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Gloriana
 */
public class clsGestorAula extends clsConexion {

    public clsGestorAula() {
    }

    
    public ResultSet cargarAulas() {
        try {
            //Nombre del procedimiento almacenado y como espera tres parametros
            //le ponemos 3 interrogantes
            String call = "{CALL proc_AulasCargar}";
            //Preparamos la sentecia
            obj_Procedimiento = getConexion().prepareCall(call);
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
           new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    public boolean guardarAula(clsAula aula) {
        boolean rpta = false;
        try {
            //Nos conectamos 
            conectarBD();
            //Busca los datos del profesor
            String call = "{CALL proc_guardarAula(?,?,?)}";
            //Preparamos la sentecia
            obj_Procedimiento = getConexion().prepareCall(call);
            //Identificacion
            obj_Procedimiento.setString(1, aula.getCodigo());
            obj_Procedimiento.setString(2, aula.getNombre());
            obj_Procedimiento.setString(3, aula.getDescripcion());
            rpta = obj_Procedimiento.executeUpdate() == 1 ? true : false;

        } catch (SQLException ex) {
            desconectarBD();
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());

        } catch (Exception ex) {
            desconectarBD();
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
        }

        desconectarBD();
        return rpta;
    }

    public boolean eliminarAula(String codigoAula) {
        boolean rpta = false;
        try {
            //Obtenemos la conexion
            conectarBD();
            //Preparamos la inserccion 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_eliminarAula(?)}");
            obj_Procedimiento.setString(1, codigoAula);
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta = obj_Procedimiento.executeUpdate() == 1 ? true : false;

            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        return rpta;
    }

    public clsAula buscarAula(String codigo) {
        clsAula aula = null;        
        try {
            //Nos conectamos 
            conectarBD();
            //Busca los datos del profesor
            String call = "{CALL proc_buscarAula(?)}";
            //Preparamos la sentecia
            obj_Procedimiento = getConexion().prepareCall(call);
            //Identificacion
            obj_Procedimiento.setString(1, codigo);
            rs = obj_Procedimiento.executeQuery();
            if (rs.next()) {
                aula = new clsAula();
                aula.setCodigo(rs.getString(1));
                aula.setNombre(rs.getString(2));
                aula.setDescripcion(rs.getString(3));
            }
            desconectarBD();
        } catch (SQLException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        return aula;
    }

    public boolean editarAula(clsAula aula) {
        boolean rpta = false;
        try {
            //Obtenemos la conexion
            conectarBD();
            //Preparamos la inserccion 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_editarAula(?,?,?)}");
            obj_Procedimiento.setString(1, aula.getCodigo());
            obj_Procedimiento.setString(2, aula.getNombre());
            obj_Procedimiento.setString(3, aula.getDescripcion());
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta = obj_Procedimiento.executeUpdate() == 1 ? true : false;

            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            System.out.println("Error al eliminar aula");
            desconectarBD();
        } catch (Exception ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            System.out.println("Error al eliminar aula");
            desconectarBD();
        }
        return rpta;
    }

    public void cargarReporteAula() {
        String logotipo = "/reportes/logoinvenio.png";
        conectarBD();
        JasperReport reporte;
        JasperPrint reporte_view;
        try {
            //direccion del archivo JASPER
            URL in = this.getClass().getResource("/reportes/aula/rptAulas.jasper");
            reporte = (JasperReport) JRLoader.loadObject(in);

            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();           
            parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));            
            //-----------------------------------
            reporte_view = JasperFillManager.fillReport(reporte, parametros, getConexion());
            JasperViewer.viewReport(reporte_view, false);

        } catch (JRException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
        }
        desconectarBD();
    }
   
}
