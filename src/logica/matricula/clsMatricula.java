/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.matricula;

import java.util.ArrayList;

/**
 *
 * @author Gloriana
 */
public class clsMatricula {
    private String numMatricula;
    private String identificacion;
    private String realizadoPor;
    private String fecha;
    private String hora;
    private String codigoPlan;
    private String annio;
    private String periodo;   
    private String estudiante;
    private String carrera;
    
    private ArrayList<clsDetalleMatricula> listaDetalle;

    public clsMatricula() {
    listaDetalle=new ArrayList();
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getAnnio() {
        return annio;
    }

    public void setAnnio(String annio) {
        this.annio = annio;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getRealizadoPor() {
        return realizadoPor;
    }

    public void setRealizadoPor(String realizadoPor) {
        this.realizadoPor = realizadoPor;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(String estudiante) {
        this.estudiante = estudiante;
    }

    public String getCodigoPlan() {
        return codigoPlan;
    }

    public void setCodigoPlan(String codigoPlan) {
        this.codigoPlan = codigoPlan;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public ArrayList<clsDetalleMatricula> getListaDetalle() {
        return listaDetalle;
    }

    public void setListaDetalle(ArrayList<clsDetalleMatricula> listaDetalle) {
        this.listaDetalle = listaDetalle;
    }

    public String getNumMatricula() {
        return numMatricula;
    }

    public void setNumMatricula(String numMatricula) {
        this.numMatricula = numMatricula;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }
}
