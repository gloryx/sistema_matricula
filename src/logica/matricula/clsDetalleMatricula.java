/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.matricula;

/**
 *
 * @author Gloriana
 */
public class clsDetalleMatricula {

    private int numMatricula;
    private String identificacion;
    private String codigo;
    private String codigoCurso;
    private String codigoPlan;
    private String nombreCurso;
    private int numeroGrupo;
    private String horario;
    private String profesor;

    public clsDetalleMatricula() {
        this(0, "", "", 0);
    }

    public clsDetalleMatricula(int numMatricula, String identificacion, String codigo, int numeroGrupo) {
        this.numMatricula = numMatricula;
        this.identificacion = identificacion;
        this.codigo = codigo;
        this.numeroGrupo = numeroGrupo;
    }

    public int getNumMatricula() {
        return numMatricula;
    }

    public void setNumMatricula(int numMatricula) {
        this.numMatricula = numMatricula;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getNumeroGrupo() {
        return numeroGrupo;
    }

    public void setNumeroGrupo(int numeroGrupo) {
        this.numeroGrupo = numeroGrupo;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public String getProfesor() {
        return profesor;
    }

    public void setProfesor(String profesor) {
        this.profesor = profesor;
    }

    public String getCodigoCurso() {
        return codigoCurso;
    }

    public void setCodigoCurso(String codigoCurso) {
        this.codigoCurso = codigoCurso;
    }

    public String getNombreCurso() {
        return nombreCurso;
    }

    public void setNombreCurso(String nombreCurso) {
        this.nombreCurso = nombreCurso;
    }

    public String getCodigoPlan() {
        return codigoPlan;
    }

    public void setCodigoPlan(String codigoPlan) {
        this.codigoPlan = codigoPlan;
    }
}
