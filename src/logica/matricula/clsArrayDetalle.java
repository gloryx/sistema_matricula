/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.matricula;

import java.util.ArrayList;

/**
 *
 * @author Gloriana
 */
public class clsArrayDetalle {

    private static ArrayList<clsDetalleMatricula> detalleMatricula;

    public clsArrayDetalle() {
        if (detalleMatricula == null) {
            detalleMatricula = new ArrayList();
        }
    }

    public ArrayList<clsDetalleMatricula> getDetalleMatricula() {
        return detalleMatricula;
    }

    public void setDetalleMatricula(ArrayList<clsDetalleMatricula> detalleMatricula) {
        clsArrayDetalle.detalleMatricula = detalleMatricula;
    }

    public boolean agregarCursoAMatricular(clsDetalleMatricula detalle) {
        boolean agregado = false;
        for (clsDetalleMatricula d: detalleMatricula) {
            if(d.getCodigo().equals(detalle.getCodigo())){                
                return agregado;
            }                
        }
        detalleMatricula.add(detalle);
        agregado=true;
        return agregado;
    }
}
