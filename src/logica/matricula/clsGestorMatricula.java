/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.matricula;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import logica.clsConexion;
import logica.logger.UseLogger;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Gloriana
 */
public class clsGestorMatricula extends clsConexion {

    public ResultSet cargarDatosCursoAbierto(String annio, String periodo, String plan, String curso) {
        try {
            //Nombre del procedimiento almacenado
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_cargarDatosCursoAbierto(?,?,?,?)}");
            obj_Procedimiento.setString(1, annio);
            obj_Procedimiento.setString(2, periodo);
            obj_Procedimiento.setString(3, plan);
            obj_Procedimiento.setString(4, curso);
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    public boolean matricularCursosPrematriculados(clsMatricula matricula) {
        boolean rpta1 = false, rpta2 = false;

        try {
            //Obtenemos la conexion
            conectarBD();

            //Decimos que vamos a crear una transaccion
            getConexion().setAutoCommit(false);

            //Preparamos la inserccion 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_guardarMatricula(?,?,?,?,?,?,?,?)}");
            obj_Procedimiento.setString(1, matricula.getNumMatricula());
            obj_Procedimiento.setString(2, matricula.getIdentificacion());
            obj_Procedimiento.setString(3, matricula.getRealizadoPor());
            obj_Procedimiento.setString(4, matricula.getFecha());
            obj_Procedimiento.setString(5, matricula.getHora());
            obj_Procedimiento.setString(6, matricula.getCodigoPlan());
            obj_Procedimiento.setString(7, matricula.getAnnio());
            obj_Procedimiento.setString(8, matricula.getPeriodo());
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta1 = obj_Procedimiento.executeUpdate() == 1;

            //Preparamos la inserccion 2 estudios realizados
            for (clsDetalleMatricula d : matricula.getListaDetalle()) {
                obj_Procedimiento = getConexion().prepareCall("{CALL proc_guardarDetalleMatricula(?,?,?,?,?,?)}");
                obj_Procedimiento.setString(1, matricula.getNumMatricula());
                obj_Procedimiento.setString(2, matricula.getIdentificacion());
                obj_Procedimiento.setString(3, d.getCodigoCurso());
                obj_Procedimiento.setString(4, matricula.getCodigoPlan());
                obj_Procedimiento.setString(5, d.getCodigo());
                obj_Procedimiento.setInt(6, d.getNumeroGrupo());
                rpta2 = obj_Procedimiento.executeUpdate() == 1;
            }

            if (rpta1 && rpta2) {
                //Confirmamos la transaccion
                getConexion().commit();
            } else {
                //Negamos la transaccion
                getConexion().rollback();
            }
            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex) {
                new UseLogger().writeLog(this.getClass().getName(), e.toString());
            }
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex) {
                new UseLogger().writeLog(this.getClass().getName(), e.toString());
            }
            desconectarBD();
        }
        return rpta1 && rpta2;
    }

    public int obtenerNumeroMatSig() {
        int numMat = 0;
        conectarBD();
        rs = seleccionar("SELECT max(tblmatricula.nummatricula + 1) AS numMat FROM tblmatricula");
        try {
            if (rs.next()) {
                numMat = rs.getInt(1);
            } else {
                numMat = 1;
            }
            numMat = numMat == 0 ? 1 : numMat;
        } catch (SQLException ex) {
            desconectarBD();
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
        }
        desconectarBD();
        return numMat;
    }

    public void reporteMatricula(String numMatricula) {
        String logotipo = "/reportes/logoinvenio.png";
        String cursosRepetidos = obtenerCursosRepetidos(numMatricula);
        conectarBD();
        JasperReport reporte;
        JasperPrint reporte_view;
        try {
            //direccion del archivo JASPER
            URL in = this.getClass().getResource("/reportes/matricula/rptEstudianteMatricula.jasper");
            reporte = (JasperReport) JRLoader.loadObject(in);

            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();
            parametros.put("numMat", numMatricula);
            parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));
            parametros.put("cursosRepetidos", cursosRepetidos);

            //-----------------------------------
            reporte_view = JasperFillManager.fillReport(reporte, parametros, getConexion());
            JasperViewer.viewReport(reporte_view, false);

        } catch (JRException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        } catch (NullPointerException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        } catch (Exception ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        desconectarBD();
    }

    public ResultSet cargarMatriculasRealizadas(String identificacion, String annio, String periodo) {
        try {
            //Nombre del procedimiento almacenado
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_buscarMatriculas(?,?,?)}");
            obj_Procedimiento.setString(1, identificacion);
            obj_Procedimiento.setString(2, annio);
            obj_Procedimiento.setString(3, periodo);
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    public void reporteMatriculaRealizadas(String identificacion, String annio, String periodo) {

        String cursosRepetidos = "";

        try {
            conectarBD();
            String sql = "SELECT\n"
                    + "      DISTINCT(tblmatricula.`nummatricula`)\n"
                    + "FROM\n"
                    + "     `tblestudiante` tblestudiante INNER JOIN `tblmatricula` tblmatricula ON tblestudiante.`IDENTIFICACION` = tblmatricula.`identificacion`\n"
                    + "     INNER JOIN `tbldetallematricula` tbldetallematricula ON tblmatricula.`nummatricula` = tbldetallematricula.`nummatricula`\n"
                    + "     INNER JOIN `tblcursoabiertosgrupos` tblcursoabiertosgrupos ON tbldetallematricula.`codigo` = tblcursoabiertosgrupos.`CODIGO`\n"
                    + "     AND tblcursoabiertosgrupos.`NUMERO` = tbldetallematricula.`grupo`\n"
                    + "     INNER JOIN `tblcursosabiertoshorarios` tblcursosabiertoshorarios ON tblcursoabiertosgrupos.`CODIGO` = tblcursosabiertoshorarios.`CODIGO`\n"
                    + "     AND tblcursoabiertosgrupos.`NUMERO` = tblcursosabiertoshorarios.`NUMERO`\n"
                    + "     INNER JOIN `tblprofesor` tblprofesor ON tblcursoabiertosgrupos.`PROFESOR` = tblprofesor.`IDENTIFICACION`\n"
                    + "     INNER JOIN `tblcursosabiertos` tblcursosabiertos ON tblcursoabiertosgrupos.`CODIGO` = tblcursosabiertos.`CODIGO`\n"
                    + "     INNER JOIN `tblcursos` tblcursos ON tblcursosabiertos.`CODIGOCURSO` = tblcursos.`CODIGOCURSO`\n"
                    + "     AND tblcursos.`CODIGOPLAN` = tblcursosabiertos.`CODIGOPLAN`\n"
                    + "     INNER JOIN `tblplanesxcarrera` tblplanesxcarrera ON tblcursosabiertos.`CODIGOPLAN` = tblplanesxcarrera.`CODIGOPLAN`\n"
                    + "     AND tblplanesxcarrera.`CODIGOPLAN` = tblcursos.`CODIGOPLAN`\n"
                    + "     INNER JOIN `tblcarreras` tblcarreras ON tblplanesxcarrera.`CODIGOCARRERA` = tblcarreras.`CODIGOCARRERA`\n"
                    + "    \n"
                    + "WHERE\n"
                    + "     tblmatricula.identificacion like '"+identificacion+"' and\n"
                    + "     tblmatricula.annio = "+annio+" and\n"
                    + "     tblmatricula.periodo like '"+periodo+"'\n"
                    + "GROUP BY\n"
                    + "     `tblcursosabiertoshorarios`.`CODIGO`,\n"
                    + "     `tblcursosabiertoshorarios`.`NUMERO`\n"
                    + "ORDER BY tblmatricula.nummatricula";
            System.out.println(sql);
            rs = seleccionar(sql);
            ArrayList<String> numMatriculas = new ArrayList<String>();
            while (rs.next()) {
                numMatriculas.add(rs.getString(1));    
            }
            desconectarBD();
            
            for (String numMatricula : numMatriculas) {
                cursosRepetidos += obtenerCursosRepetidos(numMatricula);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(clsGestorMatricula.class.getName()).log(Level.SEVERE, null, ex);
        }
        
      

        String logotipo = "/reportes/logoinvenio.png";

        conectarBD();
        JasperReport reporte;
        JasperPrint reporte_view;
        try {
            //direccion del archivo JASPER
            URL in = this.getClass().getResource("/reportes/matricula/rptEstudianteMatriculaAll.jasper");
            reporte = (JasperReport) JRLoader.loadObject(in);

            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();
            parametros.put("identificacion", identificacion);
            parametros.put("annio", Integer.parseInt(annio));
            parametros.put("periodo", periodo);
            parametros.put("cursosRepetidos", cursosRepetidos);
            parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));

            //-----------------------------------
            reporte_view = JasperFillManager.fillReport(reporte, parametros, getConexion());
            JasperViewer.viewReport(reporte_view, false);

        } catch (JRException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        } catch (NullPointerException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        } catch (Exception ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        desconectarBD();
    }

    public boolean buscarNumMat(String numMat) {
        boolean encontrado = false;
        conectarBD();
        rs = seleccionar("SELECT tblmatricula.nummatricula FROM tblmatricula WHERE tblmatricula.nummatricula = " + numMat);
        try {
            if (rs.next()) {
                encontrado = true;
            }
        } catch (SQLException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        desconectarBD();
        return encontrado;
    }

    public boolean buscarNumMatEst(String identificacion, String annio, String periodo) {
        boolean encontrado = false;
        conectarBD();
        String sql = "SELECT tblmatricula.nummatricula FROM tblmatricula "
                + "WHERE "
                + "tblmatricula.identificacion LIKE '" + identificacion + "' "
                + "AND tblmatricula.annio = " + annio
                + " AND tblmatricula.periodo LIKE '" + periodo + "'";
        rs = seleccionar(sql);
        try {
            if (rs.next()) {
                encontrado = true;
            }
        } catch (SQLException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        desconectarBD();
        return encontrado;
    }

    public ResultSet cargarDetalleMatricula(String numMat) {
        try {
            //Nombre del procedimiento almacenado
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_buscarMatriculasDetalle(?)}");
            obj_Procedimiento.setString(1, numMat);
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    public ResultSet cargarDatosMatricula(String numMat) {
        try {
            //Nombre del procedimiento almacenado
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_buscarMatricula(?)}");
            obj_Procedimiento.setString(1, numMat);
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    public boolean eliminarMatricula(String numMat) {
        boolean rpta1 = false, rpta2 = false;
        try {
            //Obtenemos la conexion
            conectarBD();

            //Decimos que vamos a crear una transaccion
            getConexion().setAutoCommit(false);

            //Preparamos la inserccion 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_eliminarDetalleMatricula(?)}");
            obj_Procedimiento.setString(1, numMat);
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta1 = obj_Procedimiento.executeUpdate() > 0 ? true : false;

            //Preparamos la inserccion 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_eliminarMatricula(?)}");
            obj_Procedimiento.setString(1, numMat);
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta2 = obj_Procedimiento.executeUpdate() == 1 ? true : false;

            if (rpta1 && rpta2) {
                //Confirmamos la transaccion
                getConexion().commit();
            } else {
                //Negamos la transaccion
                getConexion().rollback();
            }
            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex) {
                new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            }
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex) {
                new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            }
            desconectarBD();
        }
        return rpta1 && rpta2;
    }

    public boolean eliminarMatriculaEst(String numMat, String codigoCurso) {
        boolean rpta1 = false;
        try {
            //Obtenemos la conexion
            conectarBD();
            //Preparamos la inserccion 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_eliminarDetalleMatriculaEsp(?,?)}");
            obj_Procedimiento.setString(1, numMat);
            obj_Procedimiento.setString(2, codigoCurso);
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta1 = obj_Procedimiento.executeUpdate() == 1 ? true : false;
            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());

            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rpta1;
    }

    public ResultSet buscarMatriculaFechas(String fechaInicio, String fechaFin) {
        try {
            //Nombre del procedimiento almacenado
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_buscarMatriculasFechas(?,?)}");
            obj_Procedimiento.setString(1, fechaInicio);
            obj_Procedimiento.setString(2, fechaFin);
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    public ResultSet buscarEstSinMat(String annio, String periodo) {
        try {
            //Nombre del procedimiento almacenado
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_buscarEstSinMat(?,?)}");
            obj_Procedimiento.setString(1, annio);
            obj_Procedimiento.setString(2, periodo);
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    public ResultSet buscarEstConMat(String annio, String periodo) {
        try {
            //Nombre del procedimiento almacenado
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_buscarEstConMat(?,?)}");
            obj_Procedimiento.setString(1, annio);
            obj_Procedimiento.setString(2, periodo);
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    public ResultSet buscarEstConMat(String annio, String periodo, String carrera) {
        try {
            //Nombre del procedimiento almacenado
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_buscarEstConMatCarrera(?,?,?)}");
            obj_Procedimiento.setString(1, annio);
            obj_Procedimiento.setString(2, periodo);
            obj_Procedimiento.setString(3, carrera);
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    public ResultSet buscarEstSinMat(String annio, String periodo, String carrera) {
        try {
            //Nombre del procedimiento almacenado
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_buscarEstSinMatCarrera(?,?,?)}");
            obj_Procedimiento.setString(1, annio);
            obj_Procedimiento.setString(2, periodo);
            obj_Procedimiento.setString(3, carrera);
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    public void reporteEstSinMat(String annio, String periodo) {
        String logotipo = "/reportes/logoinvenio.png";

        conectarBD();
        JasperReport reporte;
        JasperPrint reporte_view;
        try {
            //direccion del archivo JASPER
            URL in = this.getClass().getResource("/reportes/matricula/rptEstSinMat.jasper");
            reporte = (JasperReport) JRLoader.loadObject(in);

            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();
            parametros.put("annio", Integer.parseInt(annio));
            parametros.put("periodo", periodo);
            parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));

            //-----------------------------------
            reporte_view = JasperFillManager.fillReport(reporte, parametros, getConexion());
            JasperViewer.viewReport(reporte_view, false);

        } catch (JRException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        } catch (NullPointerException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        } catch (Exception ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        desconectarBD();
    }

    public void reporteEstSinMat(String annio, String periodo, String codigoCarrera) {
        String logotipo = "/reportes/logoinvenio.png";

        conectarBD();
        JasperReport reporte;
        JasperPrint reporte_view;
        try {
            //direccion del archivo JASPER
            URL in = this.getClass().getResource("/reportes/matricula/rptEstSinMatCarreras.jasper");
            reporte = (JasperReport) JRLoader.loadObject(in);

            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();
            parametros.put("annio", Integer.parseInt(annio));
            parametros.put("periodo", periodo);
            parametros.put("codigoCarrera", codigoCarrera);
            parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));

            //-----------------------------------
            reporte_view = JasperFillManager.fillReport(reporte, parametros, getConexion());
            JasperViewer.viewReport(reporte_view, false);

        } catch (JRException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        } catch (NullPointerException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        } catch (Exception ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        desconectarBD();
    }

    public String obtenerCursosRepetidos(String numMatricula) {
        String repetidos = "";
        String sql = "SELECT\n"
                + "     \n"
                + "    tblmatricula.identificacion,\n"
                + "    tbldetallematricula.`codigoCurso`,\n"
                + "    tblmatricula.`codigoplan`\n"
                + "FROM\n"
                + "     `tblestudiante` tblestudiante INNER JOIN `tblmatricula` tblmatricula ON tblestudiante.`IDENTIFICACION` = tblmatricula.`identificacion`\n"
                + "     INNER JOIN `tbldetallematricula` tbldetallematricula ON tblmatricula.`nummatricula` = tbldetallematricula.`nummatricula`\n"
                + "     INNER JOIN `tblcursoabiertosgrupos` tblcursoabiertosgrupos ON tbldetallematricula.`codigo` = tblcursoabiertosgrupos.`CODIGO`\n"
                + "     AND tblcursoabiertosgrupos.`NUMERO` = tbldetallematricula.`grupo`\n"
                + "     INNER JOIN `tblcursosabiertoshorarios` tblcursosabiertoshorarios ON tblcursoabiertosgrupos.`CODIGO` = tblcursosabiertoshorarios.`CODIGO`\n"
                + "     AND tblcursoabiertosgrupos.`NUMERO` = tblcursosabiertoshorarios.`NUMERO`\n"
                + "     INNER JOIN `tblprofesor` tblprofesor ON tblcursoabiertosgrupos.`PROFESOR` = tblprofesor.`IDENTIFICACION`\n"
                + "     INNER JOIN `tblcursosabiertos` tblcursosabiertos ON tblcursoabiertosgrupos.`CODIGO` = tblcursosabiertos.`CODIGO`\n"
                + "     INNER JOIN `tblcursos` tblcursos ON tblcursosabiertos.`CODIGOCURSO` = tblcursos.`CODIGOCURSO`\n"
                + "     AND tblcursos.`CODIGOPLAN` = tblcursosabiertos.`CODIGOPLAN`\n"
                + "     INNER JOIN `tblplanesxcarrera` tblplanesxcarrera ON tblcursosabiertos.`CODIGOPLAN` = tblplanesxcarrera.`CODIGOPLAN`\n"
                + "     AND tblplanesxcarrera.`CODIGOPLAN` = tblcursos.`CODIGOPLAN`\n"
                + "     INNER JOIN `tblcarreras` tblcarreras ON tblplanesxcarrera.`CODIGOCARRERA` = tblcarreras.`CODIGOCARRERA`\n"
                + "    \n"
                + "WHERE\n"
                + "     tblmatricula.`nummatricula` = " + numMatricula, sql2;
        try {
            conectarBD();
            String identificacion = "", planCarrera = "", codigoCurso = "";
            rs = seleccionar(sql);
            ResultSet rs2;
            ArrayList<Matriculados> listaMatriculados = new ArrayList<>();
            while (rs.next()) {
                identificacion = rs.getString(1);
                codigoCurso = rs.getString(2);
                planCarrera = rs.getString(3);
                listaMatriculados.add(new Matriculados(identificacion, codigoCurso, planCarrera));
            }
            desconectarBD();
            conectarBD();
            for (Matriculados m : listaMatriculados) {
                sql2 = "SELECT CODIGOCURSO FROM tblcalificaciones WHERE "
                        + "identificacion LIKE '" + m.identificacion
                        + "'  AND codigoplan LIKE '" + m.planCarrera + "' AND CODIGOCURSO LIKE '" + m.codigoCurso + "' "
                        + "AND CALIFICACION >0 AND CALIFICACION<75 ";

                rs2 = seleccionar(sql2);
                if (rs2.next()) {
                    repetidos = repetidos + rs2.getString(1) + " - ";
                }

            }
            desconectarBD();
        } catch (SQLException ex) {
            Logger.getLogger(clsGestorMatricula.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (repetidos.length() > 0) {
            repetidos = repetidos.substring(0, repetidos.length() - 3);
        }
        return repetidos;
    }

    class Matriculados {

        private String identificacion;
        private String codigoCurso;
        private String planCarrera;

        public Matriculados() {
        }

        public Matriculados(String identificacion, String codigoCurso, String planCarrera) {
            this.identificacion = identificacion;
            this.codigoCurso = codigoCurso;
            this.planCarrera = planCarrera;
        }

    }
}
