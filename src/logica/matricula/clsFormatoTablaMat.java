/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.matricula;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Gloriana
 */
public class clsFormatoTablaMat extends DefaultTableCellRenderer {

    private int columna_patron;

    public clsFormatoTablaMat(int Colpatron) {
        this.columna_patron = Colpatron;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean selected, boolean focused, int row, int column) {
        setBackground(Color.white);//color de fondo
        table.setForeground(Color.black);//color de texto
        //Si la celda corresponde a una fila con estado FALSE, se cambia el color de fondo a rojo
        String observaciones ="";
        try {
            observaciones = table.getValueAt(row, columna_patron).toString();
        } catch (NullPointerException ex) {
            observaciones = "";
        }        
        switch (observaciones) {
            case "CURSO APROBADO":
                setBackground(new Color(0,153,51));
                break;
            case "LO PUEDE MATRICULAR":
                setBackground(new Color(255,0,0));
                break;
            case "FALTA REQUISITO":
                setBackground(new Color(255,153,51));
                break;
            case "ACTUALMENTE MATRICULADO":
                setBackground(new Color(255,255,102));
                break;
            case "CURSO PENDIENTE":
                setBackground(new Color(255,0,0));
                break;
            case "REQUISITO ACTUALMENTE MATRICULADO":
                setBackground(new Color(255,153,51));
                break;
            default:
                setBackground(new Color(255, 255, 255));
                break;                
        }
        
        super.getTableCellRendererComponent(table, value, selected, focused, row, column);
        return this;
    }

    public void fillColor(JTable t, JLabel l, boolean isSelected) {
        if (isSelected) {
            l.setBackground(t.getSelectionBackground());
            l.setForeground(t.getSelectionForeground());
        } else {
            l.setBackground(t.getBackground());
            l.setForeground(t.getForeground());
        }
    }
}
