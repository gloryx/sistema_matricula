/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.mariadb.jdbc.MariaDbPoolDataSource;

/**
 *
 * @author Gloriana
 */
public class clsPoolConexionMySql {

    MariaDbPoolDataSource ds_con;
//     MysqlConnectionPoolDataSource ds_con;
    public clsPoolConexionMySql() {
        try {
            //ds_con = new MariaDbPoolDataSource("jdbc:mariadb://172.16.252.11:3306/matriculainveniouniversidad2?user=usr_sist_mat&password=$invenio2k17$&maxPoolSize=10");
            ds_con = new MariaDbPoolDataSource();
            //ds_con = new MariaDbPoolDataSource();
            ds_con.setServerName("201.203.230.249");
            ds_con.setPort(3306);
            ds_con.setDatabaseName("matriculainveniouniversidad2");
            ds_con.setUser("usr_sist_mat");
            ds_con.setPassword("$invenio2k17$");
        } catch (SQLException ex) {
            Logger.getLogger(clsPoolConexionMySql.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    clsPoolConexionMySql(int servidor) {
        try {
            ds_con = new MariaDbPoolDataSource();
            switch (servidor) {
               case 1:
                    //ds_con.setServerName("201.203.230.249");
                    //Solo Invenio
                    ds_con.setServerName("172.16.252.10");
                    ds_con.setPort(3306);
                    ds_con.setLoginTimeout(20);
                    ds_con.setDatabaseName("matriculainveniouniversidad2");
                    ds_con.setUser("usr_sist_mat");
                    ds_con.setPassword("$invenio2k17$");
                    
                    break;
                case 2:
                    //ds_con.setServerName("201.203.230.249");
                    //Solo en la nube
                    ds_con.setServerName("45.229.246.59");
                    ds_con.setPort(3306);
                    ds_con.setDatabaseName("matriculainveniouniversidad2");
                    ds_con.setUser("usr_sist_mat");
                    ds_con.setPassword("$invenio2k17$");
                    
                    break;
                case 3:
                    ds_con.setServerName("localhost");
                    ds_con.setPort(3306);
                    ds_con.setDatabaseName("matriculainveniouniversidad2");
                    ds_con.setUser("root");
                    ds_con.setPassword("root");
                    
                    break;
                case 4:
                    ds_con.setServerName("172.16.252.10");
                    ds_con.setPort(3306);
                    ds_con.setDatabaseName("matriculainveniouniversidad2");
                    ds_con.setUser("usr_sist_mat");
                    ds_con.setPassword("$invenio2k17$");
                    
                    System.out.println("4");
                    break;
            }
            System.out.println("Unica conexión establecida");
        } catch (SQLException ex) {
            Logger.getLogger(clsPoolConexionMySql.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Connection getConexion() {
        try {
            return ds_con.getConnection();
        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(null, "Se perdio la conexión con el servidor :(", "Conexion al servidor", JOptionPane.ERROR_MESSAGE);
            System.err.println(ex);
        }
        return null;
    }
}
