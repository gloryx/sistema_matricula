/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.sql.Date;
import java.text.SimpleDateFormat;

/**
 *
 * @author Gloriana
 */
public class clsValidar {

    //VALIDA QUE UN DATO SEA NUMERICO
    static public boolean esNumero(String dato) {
        try {
            Integer.parseInt(dato);
            return true;
        } catch (Exception e) {
            return false;
        }
    }//=========================================================================
    //metodo para validar si la fecha es correcta

    static public boolean esFecha(String fechax) {
        try {
            SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yy");
            Date fecha = (Date) formatoFecha.parse(fechax);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static boolean esVacio(String texto) {
        if (texto.equals("")) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean esGrande(String texto, int tam) {
        if (texto.length() > tam) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean esDoble(String dato) {
        try {
            Double.parseDouble(dato);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    public static String quitarCarEsp(String cadena){
    
       cadena = cadena.toUpperCase().replace("Á", "A")
               .replace("É", "E")
               .replace("Í", "I")
               .replace("Ó", "O")
               .replace("Ú", "U")
               .replace("Ñ", "N");
       return cadena;
    }
}
