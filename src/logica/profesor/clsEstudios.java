/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.profesor;

/**
 *
 * @author Gloriana
 */
public class clsEstudios {
    private String titulo;
    private String institucion;
    private String fecha;

    public clsEstudios() {
        this("", "", "");
    }

    public clsEstudios(String titulo, String institucion, String fecha) {
        this.titulo = titulo;
        this.institucion = institucion;
        this.fecha = fecha;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getInstitucion() {
        return institucion;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    
}
