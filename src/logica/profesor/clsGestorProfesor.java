/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.profesor;

import java.awt.HeadlessException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import logica.clsConexion;
import logica.logger.UseLogger;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 *
 * @author Gloriana
 */
public class clsGestorProfesor extends clsConexion {
    //Guarda un profesor

    public boolean guardarProfesor(clsProfesor profesor) {
        boolean rpta1 = false, rpta2 = false, rpta3 = false;

        try {
            //Obtenemos la conexion
            conectarBD();

            //Decimos que vamos a crear una transaccion
            getConexion().setAutoCommit(false);

            //Preparamos la inserccion 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_guardarProfesor(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
            obj_Procedimiento.setInt(1, profesor.getTipoIdentificacion());
            obj_Procedimiento.setString(2, profesor.getIdentificacion());
            obj_Procedimiento.setString(3, profesor.getPrimerNombre());
            obj_Procedimiento.setString(4, profesor.getSegundoNombre());
            obj_Procedimiento.setString(5, profesor.getPrimerApellido());
            obj_Procedimiento.setString(6, profesor.getSegundoApellido());
            obj_Procedimiento.setString(7, profesor.getFechaIngreso());
            obj_Procedimiento.setString(8, profesor.getEstadoCivil());
            obj_Procedimiento.setString(9, String.valueOf(profesor.getGenero()));
            obj_Procedimiento.setString(10, profesor.getFechaNacimiento());
            obj_Procedimiento.setInt(11, profesor.getIdProvincia());
            obj_Procedimiento.setInt(12, profesor.getIdCanton());
            obj_Procedimiento.setInt(13, profesor.getIdDistrito());
            obj_Procedimiento.setString(14, profesor.getOtrasSenas());
            obj_Procedimiento.setString(15, profesor.getCorreo());
            obj_Procedimiento.setString(16, profesor.getTelefonoCasa());
            obj_Procedimiento.setString(17, profesor.getTelefonoCelular());
            obj_Procedimiento.setString(18, profesor.getTelefonoTrabajo());
            obj_Procedimiento.setString(19, profesor.getCodigoCarrera());
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta1 = obj_Procedimiento.executeUpdate() == 1 ? true : false;

            //Preparamos la inserccion 2 estudios realizados
            if (profesor.getArrayEstudios().size() > 0) {
                for (clsEstudios estudios : profesor.getArrayEstudios()) {
                    obj_Procedimiento = getConexion().prepareCall("{CALL proc_guardarProfesorEstudios(?,?,?,?)}");
                    obj_Procedimiento.setString(1, profesor.getIdentificacion());
                    obj_Procedimiento.setString(2, estudios.getTitulo());
                    obj_Procedimiento.setString(3, estudios.getInstitucion());
                    obj_Procedimiento.setString(4, estudios.getFecha());
                    rpta2 = obj_Procedimiento.executeUpdate() == 1 ? true : false;
                }
            } else {
                rpta2 = true;
            }

            //Preparamos la inserccion 3 experiencia laboral
            if (profesor.getArrayExperiencia().size() > 0) {
                for (clsExperiencia experiencia : profesor.getArrayExperiencia()) {
                    obj_Procedimiento = getConexion().prepareCall("{CALL proc_guardarProfesorExperiencia(?,?,?,?)}");
                    obj_Procedimiento.setString(1, profesor.getIdentificacion());
                    obj_Procedimiento.setString(2, experiencia.getEmpresa());
                    obj_Procedimiento.setString(3, experiencia.getPuesto());
                    obj_Procedimiento.setString(4, experiencia.getFecha());
                    rpta3 = obj_Procedimiento.executeUpdate() == 1 ? true : false;
                }
            } else {
                rpta3 = true;
            }

            if (rpta1 && rpta2 && rpta3) {
                //Confirmamos la transaccion
                getConexion().commit();
            } else {
                //Negamos la transaccion
                getConexion().rollback();
            }
            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex) {
                new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            }
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex) {
                new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            }
            desconectarBD();
        }
        return rpta1 && rpta2 && rpta3;
    }//=========================================================================

    public boolean editarProfesor(clsProfesor profesor) {
        boolean rpta1 = false, rpta2 = false, rpta3 = false;

        try {
            //Obtenemos la conexion
            conectarBD();
            //Decimos que vamos a crear una transaccion
            getConexion().setAutoCommit(false);

            //Preparamos la edicion 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_editarProfesor(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
            obj_Procedimiento.setInt(1, profesor.getTipoIdentificacion());
            obj_Procedimiento.setString(2, profesor.getIdentificacion());
            obj_Procedimiento.setString(3, profesor.getPrimerNombre());
            obj_Procedimiento.setString(4, profesor.getSegundoNombre());
            obj_Procedimiento.setString(5, profesor.getPrimerApellido());
            obj_Procedimiento.setString(6, profesor.getSegundoApellido());
            obj_Procedimiento.setString(7, profesor.getFechaIngreso());
            obj_Procedimiento.setString(8, profesor.getEstadoCivil());
            obj_Procedimiento.setString(9, String.valueOf(profesor.getGenero()));
            obj_Procedimiento.setString(10, profesor.getFechaNacimiento());
            obj_Procedimiento.setInt(11, profesor.getIdProvincia());
            obj_Procedimiento.setInt(12, profesor.getIdCanton());
            obj_Procedimiento.setInt(13, profesor.getIdDistrito());
            obj_Procedimiento.setString(14, profesor.getOtrasSenas());
            obj_Procedimiento.setString(15, profesor.getCorreo());
            obj_Procedimiento.setString(16, profesor.getTelefonoCasa());
            obj_Procedimiento.setString(17, profesor.getTelefonoCelular());
            obj_Procedimiento.setString(18, profesor.getTelefonoTrabajo());
            obj_Procedimiento.setString(19, profesor.getCodigoCarrera());
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //edito de forma correcta los datos
            rpta1 = obj_Procedimiento.executeUpdate() == 1 ? true : false;

            //Preparamos la edicion 2 estudios realizados
            //en realidad no edita elimina e inserta
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_eliminarProfesorEstudios(?)}");
            obj_Procedimiento.setString(1, profesor.getIdentificacion());
            obj_Procedimiento.executeUpdate();
            if (profesor.getArrayEstudios().size() > 0) {
                for (clsEstudios estudios : profesor.getArrayEstudios()) {
                    obj_Procedimiento = getConexion().prepareCall("{CALL proc_guardarProfesorEstudios(?,?,?,?)}");
                    obj_Procedimiento.setString(1, profesor.getIdentificacion());
                    obj_Procedimiento.setString(2, estudios.getTitulo());
                    obj_Procedimiento.setString(3, estudios.getInstitucion());
                    obj_Procedimiento.setString(4, estudios.getFecha());
                    rpta2 = obj_Procedimiento.executeUpdate() == 1 ? true : false;

                }
            } else {
                rpta2 = true;
            }

            //Preparamos la edicion 3 experiencia laboral
            //en realidad no edita elimina e inserta
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_eliminarProfesorExperiencia(?)}");
            obj_Procedimiento.setString(1, profesor.getIdentificacion());
            obj_Procedimiento.executeUpdate();
            if (profesor.getArrayExperiencia().size() > 0) {
                for (clsExperiencia experiencia : profesor.getArrayExperiencia()) {
                    obj_Procedimiento = getConexion().prepareCall("{CALL proc_guardarProfesorExperiencia(?,?,?,?)}");
                    obj_Procedimiento.setString(1, profesor.getIdentificacion());
                    obj_Procedimiento.setString(2, experiencia.getEmpresa());
                    obj_Procedimiento.setString(3, experiencia.getPuesto());
                    obj_Procedimiento.setString(4, experiencia.getFecha());
                    rpta3 = obj_Procedimiento.executeUpdate() == 1 ? true : false;

                }
            } else {
                rpta3 = true;
            }

            if (rpta1 && rpta2 && rpta3) {
                //Confirmamos la transaccion
                getConexion().commit();
            } else {
                //Negamos la transaccion
                getConexion().rollback();
            }
            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex) {
                new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            }
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex) {
                new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            }
            desconectarBD();
        }
        return rpta1 && rpta2 && rpta3;
    }//=========================================================================

    public clsProfesor buscarProfesor(String identificacion) {
        clsProfesor profesor = null;
        boolean rpta1 = false;
        try {
            //Nos conectamos 
            conectarBD();
            //Busca los datos del profesor
            String call = "{CALL proc_buscarProfesor(?)}";
            //Preparamos la sentecia
            obj_Procedimiento = getConexion().prepareCall(call);
            //Identificacion
            obj_Procedimiento.setString(1, identificacion);
            rs = obj_Procedimiento.executeQuery();
            if (rs.next()) {
                profesor = new clsProfesor();
                profesor.setTipoIdentificacion(rs.getByte(1));
                profesor.setIdentificacion(rs.getString(2));
                profesor.setPrimerNombre(rs.getString(3));
                profesor.setSegundoNombre(rs.getString(4));
                profesor.setPrimerApellido(rs.getString(5));
                profesor.setSegundoApellido(rs.getString(6));
                profesor.setFechaIngreso(rs.getString(7));
                profesor.setEstadoCivil(rs.getString(8));
                profesor.setGenero(rs.getString(9).charAt(0));
                profesor.setFechaNacimiento(rs.getString(10));
                profesor.setIdProvincia(rs.getByte(11));
                profesor.setIdCanton(rs.getByte(12));
                profesor.setIdDistrito(rs.getByte(13));
                profesor.setOtrasSenas(rs.getString(14));
                profesor.setCorreo(rs.getString(15));
                profesor.setTelefonoCasa(rs.getString(16));
                profesor.setTelefonoCelular(rs.getString(17));
                profesor.setTelefonoTrabajo(rs.getString(18));
                profesor.setCodigoCarrera(rs.getString(19));
                rpta1 = true;
            }
        } catch (SQLException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
        }
        if (rpta1) {
            //Estudios realizados
            try {
                String call = "{CALL proc_buscarProfesorEstudios(?)}";
                //Preparamos la sentecia
                obj_Procedimiento = getConexion().prepareCall(call);
                obj_Procedimiento.setString(1, identificacion);
                rs = obj_Procedimiento.executeQuery();
                ArrayList<clsEstudios> listaEstudios = new ArrayList();
                clsEstudios estudios;
                while (rs.next()) {
                    estudios = new clsEstudios();
                    estudios.setTitulo(rs.getString(2));
                    estudios.setInstitucion(rs.getString(3));
                    estudios.setFecha(rs.getString(4));
                    listaEstudios.add(estudios);
                }
                profesor.setArrayEstudios(listaEstudios);
            } catch (SQLException ex) {
                new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            }
            //Experiencia laboral
            try {
                String call = "{CALL proc_buscarProfesorExperiencia(?)}";
                //Preparamos la sentecia
                obj_Procedimiento = getConexion().prepareCall(call);
                obj_Procedimiento.setString(1, identificacion);
                rs = obj_Procedimiento.executeQuery();
                ArrayList<clsExperiencia> listaExperiencia = new ArrayList();
                clsExperiencia experiencia;
                while (rs.next()) {
                    experiencia = new clsExperiencia();
                    experiencia.setEmpresa(rs.getString(2));
                    experiencia.setPuesto(rs.getString(3));
                    experiencia.setFecha(rs.getString(4));
                    listaExperiencia.add(experiencia);
                }
                profesor.setArrayExperiencia(listaExperiencia);
            } catch (SQLException ex) {
                new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            }
        }
        desconectarBD();
        return profesor;
    }//=========================================================================

    public boolean eliminarProfesor(String identificacion) {
        boolean rpta = false;
        try {
            //Obtenemos la conexion
            conectarBD();
            //Decimos que vamos a crear una transaccion
            getConexion().setAutoCommit(false);

            //Preparamos la inserccion 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_eliminarProfesor(?)}");
            obj_Procedimiento.setString(1, identificacion);
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta = obj_Procedimiento.executeUpdate() == 1 ? true : false;

            if (rpta) {
                //Confirmamos la transaccion
                getConexion().commit();
            } else {
                //Negamos la transaccion
                getConexion().rollback();
            }
            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex) {
                new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            }
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex) {
                new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            }
            desconectarBD();
        }
        return rpta;
    }//=========================================================================

    public boolean copiarFotografia(File entrada, File salida) {
        try {
            FileChannel out;
            try (FileChannel in = (new FileInputStream(entrada)).getChannel()) {
                out = (new FileOutputStream(salida)).getChannel();
                in.transferTo(0, entrada.length(), out);
            }
            out.close();
            return true;
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            return false;
        }
    }//=========================================================================

    public ResultSet buscarProfesoresCarreras(String codigoCarrera) {
        try {
            //Nombre del procedimiento almacenado y como espera tres parametros
            //le ponemos 3 interrogantes
            String call = "{CALL proc_cargarProfesoresCarreras(?)}";
            //Preparamos la sentecia
            obj_Procedimiento = getConexion().prepareCall(call);
            //Identificacion
            obj_Procedimiento.setString(1, codigoCarrera);
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }//=========================================================================

    public ResultSet cargarProfesores() {
        try {
            //Nombre del procedimiento almacenado y como espera tres parametros
            //le ponemos 3 interrogantes
            String call = "{CALL proc_cargarProfesores}";
            //Preparamos la sentecia
            obj_Procedimiento = getConexion().prepareCall(call);
            //Identificacion
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }//=========================================================================

    public void cargarReporteProfesor(String identificacion) {
        String logotipo = "/reportes/logoinvenio.png";

        Icon foto = cargarFoto(identificacion);
        File entrada = null;
        if (foto != null) {
            entrada = new File(foto.toString());
        }
        conectarBD();
        JasperReport reporte;
        JasperPrint reporte_view;
        try {
            //direccion del archivo JASPER
            URL in = this.getClass().getResource("/reportes/profesores/rptProfesor.jasper");
            reporte = (JasperReport) JRLoader.loadObject(in);

            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();
            parametros.put("identificacion", identificacion);
            parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));
            parametros.put("SUBREPORT_DIR", this.getClass().getResource("/reportes/profesores/"));
            if (entrada != null) {
                try {
                    parametros.put("foto", new FileInputStream(entrada));
                } catch (FileNotFoundException ex) {
                    new UseLogger().writeLog(this.getClass().getName(), ex.toString());
                }
            } else {
                parametros.put("foto", this.getClass().getResourceAsStream("/reportes/no-foto.png"));
            }
            //-----------------------------------
            reporte_view = JasperFillManager.fillReport(reporte, parametros, getConexion());
            JasperViewer.viewReport(reporte_view, false);

        } catch (JRException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        } catch (NullPointerException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        } catch (Exception ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        desconectarBD();
    }
    //=========================================================================

    public void cargarReporteProfesorCursosImpartidos(String identificacion) {
        String logotipo = "/reportes/logoinvenio.png";

        Icon foto = cargarFoto(identificacion);
        File entrada = null;
        if (foto != null) {
            entrada = new File(foto.toString());
        }
        conectarBD();
        JasperReport reporte;
        JasperPrint reporte_view;
        try {
            //direccion del archivo JASPER
            URL in = this.getClass().getResource("/reportes/profesores/rptProfesorCursosImpartidos.jasper");
            reporte = (JasperReport) JRLoader.loadObject(in);

            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();
            parametros.put("identificacion", identificacion);
            parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));
            reporte_view = JasperFillManager.fillReport(reporte, parametros, getConexion());
            JasperViewer.viewReport(reporte_view, false);
        } catch (JRException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        } catch (NullPointerException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        } catch (Exception ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        desconectarBD();
    }
    //=========================================================================
    
    public Icon cargarFoto(String identificacion) {
        Icon foto = null;
        File dir = new File(System.getProperty("user.dir") + "\\fotos\\");
        String[] ficheros = dir.list();
        if (ficheros == null) {
            System.out.println("No hay ficheros en el directorio especificado");
        } else {
            for (int x = 0; x < ficheros.length; x++) {
                if (ficheros[x].startsWith(identificacion)) {
                    foto = new ImageIcon(System.getProperty("user.dir") + "\\fotos\\" + ficheros[x]);
                    break;
                }
            }
        }
        return foto;
    }
    //Buscar si foto existe

    public void cargarReporteCarreras(String sql) {
        String logotipo = "/reportes/logoinvenio.png";

        conectarBD();
        rs = seleccionar(sql);

        JasperReport reporte;
        JasperPrint reporte_view;
        try {
            //direccion del archivo JASPER
            URL in = this.getClass().getResource("/reportes/profesores/rptProfesoresCarreras.jasper");
            reporte = (JasperReport) JRLoader.loadObject(in);
            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();
            parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));
            JRResultSetDataSource resultSetDataSource = new JRResultSetDataSource(rs);
            reporte_view = JasperFillManager.fillReport(reporte, parametros, resultSetDataSource);
            JasperViewer.viewReport(reporte_view, false);

        } catch (JRException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        } catch (NullPointerException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        } catch (Exception ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        desconectarBD();
    }

    public void cargarGraficoProfCarr() {
        conectarBD();
        JasperReport reporte;
        JasperPrint reporte_view;
        try {
            //direccion del archivo JASPER
            URL in = this.getClass().getResource("/reportes/profesores/rptGraficosProfesorCarreras.jasper");
            reporte = (JasperReport) JRLoader.loadObject(in);

            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();
            parametros.put("logoinvenio", this.getClass().getResourceAsStream("/reportes/logoinvenio.png"));
            reporte_view = JasperFillManager.fillReport(reporte, parametros, getConexion());
            JasperViewer.viewReport(reporte_view, false);

        } catch (JRException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        } catch (NullPointerException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        } catch (Exception ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        desconectarBD();
    }

    public void imprimirCursosProfesorConFoto(String condiciones, int estado) {
        String logotipo = "/reportes/logoinvenio.png";

        JasperReport reporte;
        JasperPrint reporte_view;
        try {
            //direccion del archivo JASPER
            URL in = this.getClass().getResource("/reportes/profesores/rptListaDeProfesoresConFotoCarne.jasper");
            reporte = (JasperReport) JRLoader.loadObject(in);

            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();
            parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));
             parametros.put("estado", estado==1?"ACTIVOS":"INACTIVOS");
            //==================================================================
            clsGestorProfesorDataSourceFoto datasource = new clsGestorProfesorDataSourceFoto();
            clsProfesorFotoLista profesorFoto;
            conectarBD();
            String nombre1, nombre2, apellido1, apellido2;
            rs = cargarProfesorConFoto(condiciones);
            if (rs != null) {
                while (rs.next()) {
                    profesorFoto = new clsProfesorFotoLista();
                    profesorFoto.setIdentificacion(rs.getString(1));
                    nombre1 = rs.getString(2);
                    nombre2 = rs.getString(3);
                    apellido1 = rs.getString(4);
                    apellido2 = rs.getString(5);
                    profesorFoto.setNombreCompleto(apellido1 + " " + apellido2
                            + " " + nombre1 + " " + nombre2);
                    profesorFoto.setFechaNacimiento(rs.getString(6));
                    profesorFoto.setCarrera(rs.getString(7));
                    Icon foto = cargarFoto(profesorFoto.getIdentificacion());
                    File entrada = null;
                    if (foto != null) {
                        entrada = new File(foto.toString());
                    }

                    profesorFoto.setFoto(entrada);

                    datasource.addclsEstudianteFoto(profesorFoto);
                }

            }
            desconectarBD();
            //==================================================================
            parametros.put("total", datasource.getTotal());
            reporte_view = JasperFillManager.fillReport(reporte, parametros, datasource);
            JasperViewer.viewReport(reporte_view, false);

        } catch (JRException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        } catch (NullPointerException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        } catch (Exception ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
    }

    private ResultSet cargarProfesorConFoto(String condiciones) {
        String sql = "SELECT "
                + " tblprofesor.`IDENTIFICACION`,"
                + " tblprofesor.`PRIMERNOMBRE` ,"
                + " tblprofesor.`SEGUNDONOMBRE` ,"
                + " tblprofesor.`PRIMERAPELLIDO` ,"
                + " tblprofesor.`SEGUNDOAPELLIDO`,"
                + " tblprofesor.`FECHANACIMIENTO`,"
                + " tblcarreras.`NOMBRECARRERA`"
                + " FROM tblprofesor"
                + " INNER JOIN tblcarreras "
                + " ON tblprofesor.CODIGOCARRERA = tblcarreras.CODIGOCARRERA "
                + condiciones
                + " ORDER BY tblcarreras.CODIGOCARRERA,tblprofesor.FECHAINGRESO";
        rs = seleccionar(sql);
        return rs;
    }

    public void generarArchivoExcel(String sql) {
        try {
            HSSFWorkbook wb = new HSSFWorkbook();
            HSSFSheet sheet = wb.createSheet("profesores");
            HSSFRow row = sheet.createRow(0);

            conectarBD();
            rs = seleccionar(sql);
            ResultSetMetaData Datos = rs.getMetaData();
            row = sheet.createRow(0);
            for (int i = 0; i < Datos.getColumnCount(); i++) {
                row.createCell(i).setCellValue(Datos.getColumnName(i + 1));
            }

            int fil = 1;
            while (rs.next()) {
                row = sheet.createRow(fil++);
                for (int col = 0; col < Datos.getColumnCount(); col++) {
                    try {
                        row.createCell(col).setCellValue(rs.getObject(col + 1).toString());
                    } catch (Exception e) {
                        row.createCell(col).setCellValue("");
                    }
                }
            }
            JFileChooser fc = new JFileChooser();

            fc.setSelectedFile(new File("Lista profesores.xls"));
            int returnVal = fc.showSaveDialog(null);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                try (FileOutputStream fileOut = new FileOutputStream(file.getPath())) {
                    wb.write(fileOut);
                }

                //Ejecutar archivo de excel
//                String dir=file.getPath();
//                Runtime.getRuntime().exec("cmd /c start " +dir);
                JOptionPane.showMessageDialog(null, "Archivo " + fc.getSelectedFile().getName() + " guarado exitosamente");

            }
        } catch (SQLException | HeadlessException | IOException e) {
            javax.swing.JOptionPane.showMessageDialog(null, "Error al exportar la consulta: " + e);
            desconectarBD();
        }


        desconectarBD();
    }

     public void generarArchivoExcel(String titulo, JTable tblDatos) {

        try {
            HSSFWorkbook wb = new HSSFWorkbook();
            HSSFSheet sheet = wb.createSheet(titulo);
            HSSFRow row = sheet.createRow(0);
            row.createCell(0).setCellValue(titulo);
            row = sheet.createRow(1);
            row = sheet.createRow(2);
            for (int i = 0; i < tblDatos.getColumnCount(); i++) {
                row.createCell(i).setCellValue(tblDatos.getColumnName(i));
            }

            int fil = 3;
            for (int i = 0; i < tblDatos.getRowCount(); i++) {
                row = sheet.createRow(fil++);
                for (int col = 0; col < tblDatos.getColumnCount(); col++) {
                    try {
                        row.createCell(col).setCellValue(tblDatos.getValueAt(i, col).toString());
                    } catch (Exception e) {
                        row.createCell(col).setCellValue("");
                    }
                }
            }
            JFileChooser fc = new JFileChooser();

            fc.setSelectedFile(new File(titulo + ".xls"));
            int returnVal = fc.showSaveDialog(null);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                try (FileOutputStream fileOut = new FileOutputStream(file.getPath())) {
                    wb.write(fileOut);
                }

                //Ejecutar archivo de excel
//                String dir=file.getPath();
//                Runtime.getRuntime().exec("cmd /c start " +dir);
                JOptionPane.showMessageDialog(null, "Archivo " + fc.getSelectedFile().getName() + " guarado exitosamente");

            }
        } catch (IOException | HeadlessException e) {
            javax.swing.JOptionPane.showMessageDialog(null, "Error al exportar la consulta: " + e);
            desconectarBD();
        }
        desconectarBD();
    }
    
    public boolean cambiarEstadoProfesor(String identificacion) {
         boolean rpta = false;
        try {
            //Obtenemos la conexion
            conectarBD();
            //Decimos que vamos a crear una transaccion
            getConexion().setAutoCommit(false);

            //Preparamos la inserccion 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_editarActivo(?)}");
            obj_Procedimiento.setString(1, identificacion);
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta = obj_Procedimiento.executeUpdate() == 1;

            if (rpta) {
                //Confirmamos la transaccion
                getConexion().commit();
            } else {
                //Negamos la transaccion
                getConexion().rollback();
            }
            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex) {
                new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            }
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex) {
                new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            }
            desconectarBD();
        }
        return rpta;
    }
}
