/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.profesor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import logica.logger.UseLogger;

/**
 *
 * @author Gloriana
 */
public class clsProfesorFotoLista {
    String identificacion;
    String nombreCompleto;
    String fechaNacimiento;
    String carrera;
    
    private  FileInputStream foto;

    public clsProfesorFotoLista() {
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public FileInputStream getFoto() {
        return foto;
    }

    public void setFoto(FileInputStream foto) {
        this.foto = foto;
    }

    public void setFoto(File entrada) {
        if (entrada != null) {
                try {
                    foto= new FileInputStream(entrada);
                } catch (FileNotFoundException ex) {
                    new UseLogger().writeLog(this.getClass().getName(), ex.toString());
                }
            }
    }

}
