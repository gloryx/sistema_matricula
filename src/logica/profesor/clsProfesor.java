/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.profesor;

import java.util.ArrayList;

/**
 *
 * @author Gloriana
 */
public class clsProfesor {
    private int tipoIdentificacion;
    private String identificacion;
    private String primerNombre;
    private String segundoNombre;
    private String primerApellido;
    private String segundoApellido;
    private String fechaIngreso;
    private String estadoCivil;
    private char genero;
    private String fechaNacimiento;//Date
    private int idProvincia;
    private int idCanton;
    private int idDistrito;
    private String otrasSenas;
    private String correo;
    private String telefonoCasa;
    private String telefonoCelular;
    private String telefonoTrabajo;
    private String codigoCarrera;
    private ArrayList<clsEstudios> arrayEstudios;
    private ArrayList<clsExperiencia> arrayExperiencia;

    public clsProfesor() {
        this.arrayEstudios=new ArrayList();
        this.arrayExperiencia=new ArrayList();
    }

    public clsProfesor(byte tipoIdentificacion, String identificacion, String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, String fechaIngreso, String estadoCivil, char genero, String fechaNacimiento, byte idProvincia, byte idCanton, byte idDistrito, String otrasSenas, String correo, String telefonoCasa, String telefonoCelular, String telefonoTrabajo, String codigoCarrera) {
        this.tipoIdentificacion = tipoIdentificacion;
        this.identificacion = identificacion;
        this.primerNombre = primerNombre;
        this.segundoNombre = segundoNombre;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.fechaIngreso = fechaIngreso;
        this.estadoCivil = estadoCivil;
        this.genero = genero;
        this.fechaNacimiento = fechaNacimiento;
        this.idProvincia = idProvincia;
        this.idCanton = idCanton;
        this.idDistrito = idDistrito;
        this.otrasSenas = otrasSenas;
        this.correo = correo;
        this.telefonoCasa = telefonoCasa;
        this.telefonoCelular = telefonoCelular;
        this.telefonoTrabajo = telefonoTrabajo;
        this.codigoCarrera = codigoCarrera;
    }

    public int getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(int tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public char getGenero() {
        return genero;
    }

    public void setGenero(char genero) {
        this.genero = genero;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public int getIdProvincia() {
        return idProvincia;
    }

    public void setIdProvincia(int idProvincia) {
        this.idProvincia = idProvincia;
    }

    public int getIdCanton() {
        return idCanton;
    }

    public void setIdCanton(int idCanton) {
        this.idCanton = idCanton;
    }

    public int getIdDistrito() {
        return idDistrito;
    }

    public void setIdDistrito(int idDistrito) {
        this.idDistrito = idDistrito;
    }

    public String getOtrasSenas() {
        return otrasSenas;
    }

    public void setOtrasSenas(String otrasSenas) {
        this.otrasSenas = otrasSenas;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefonoCasa() {
        return telefonoCasa;
    }

    public void setTelefonoCasa(String telefonoCasa) {
        this.telefonoCasa = telefonoCasa;
    }

    public String getTelefonoCelular() {
        return telefonoCelular;
    }

    public void setTelefonoCelular(String telefonoCelular) {
        this.telefonoCelular = telefonoCelular;
    }

    public String getTelefonoTrabajo() {
        return telefonoTrabajo;
    }

    public void setTelefonoTrabajo(String telefonoTrabajo) {
        this.telefonoTrabajo = telefonoTrabajo;
    }

    public String getCodigoCarrera() {
        return codigoCarrera;
    }

    public void setCodigoCarrera(String codigoCarrera) {
        this.codigoCarrera = codigoCarrera;
    }

    public ArrayList<clsEstudios> getArrayEstudios() {
        return arrayEstudios;
    }

    public void setArrayEstudios(ArrayList<clsEstudios> arrayEstudios) {
        this.arrayEstudios = arrayEstudios;
    }

    public ArrayList<clsExperiencia> getArrayExperiencia() {
        return arrayExperiencia;
    }

    public void setArrayExperiencia(ArrayList<clsExperiencia> arrayExperiencia) {
        this.arrayExperiencia = arrayExperiencia;
    }

    @Override
    public String toString() {
        return primerNombre + " " + primerApellido + " " + segundoApellido;
    }

}
