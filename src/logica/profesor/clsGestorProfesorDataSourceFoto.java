/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.profesor;

import java.util.ArrayList;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

/**
 *
 * @author Gloriana
 */
public class clsGestorProfesorDataSourceFoto implements JRDataSource {

    private List<clsProfesorFotoLista> listaclsEstudianteFotos = new ArrayList<>();
    private int indiceclsEstudianteFotoActual = -1;

    @Override
    public boolean next() throws JRException {
        return ++indiceclsEstudianteFotoActual < listaclsEstudianteFotos.size();
    }

    @Override
    public Object getFieldValue(JRField jrField) throws JRException {
        Object valor = null;
        switch (jrField.getName()) {
            case "identificacion":
                valor = listaclsEstudianteFotos.get(indiceclsEstudianteFotoActual).getIdentificacion();
                break;
            case "nombreCompleto":
                valor = listaclsEstudianteFotos.get(indiceclsEstudianteFotoActual).getNombreCompleto();
                break;
            case "fechaNacimiento":
                valor = listaclsEstudianteFotos.get(indiceclsEstudianteFotoActual).getFechaNacimiento();
                break;
            case "carrera":
                valor = listaclsEstudianteFotos.get(indiceclsEstudianteFotoActual).getCarrera();
                break;
            case "foto":
                valor = listaclsEstudianteFotos.get(indiceclsEstudianteFotoActual).getFoto();
                break;
        }

        return valor;
    }

    public void addclsEstudianteFoto(clsProfesorFotoLista estudianteFotoLista) {
        this.listaclsEstudianteFotos.add(estudianteFotoLista);
    }

    public int getTotal() {
        return listaclsEstudianteFotos.size();
    }
}
