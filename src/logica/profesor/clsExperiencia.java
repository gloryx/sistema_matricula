/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.profesor;

/**
 *
 * @author Gloriana
 */
public class clsExperiencia {
    private String empresa;
    private String puesto;
    private String fecha;

    public clsExperiencia() {
        this("", "", "");
    }

    public clsExperiencia(String empresa, String puesto, String fecha) {
        this.empresa = empresa;
        this.puesto = puesto;
        this.fecha = fecha;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    
}
