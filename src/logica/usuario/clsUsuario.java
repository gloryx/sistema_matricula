/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.usuario;

/**
 *
 * @author Gloriana
 */
public class clsUsuario {
    private String idUsuario;
    private String usuario;
    private String clave;
    private String nombre;
    private String apellidos;
    private int nivelSeguridad;
    private String nivel;
    private String estado;

    public clsUsuario() {
    }

    public clsUsuario(String idUsuario, String usuario, String clave, String nombre, String apellidos, int nivelSeguridad, String estado) {
        this.idUsuario = idUsuario;
        this.usuario = usuario;
        this.clave = clave;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.nivelSeguridad = nivelSeguridad;
        this.estado = estado;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public int getNivelSeguridad() {
        return nivelSeguridad;
    }

    public void setNivelSeguridad(int nivelSeguridad) {
        this.nivelSeguridad = nivelSeguridad;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }   

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }           
}
