package logica.usuario;

import gui.usuario.frmLogin;
import javax.swing.SwingWorker;

/**
 * @web http://www.jc-mouse.net/
 * @author Mouse
 */
public class MyWorker extends SwingWorker<Void, Void> {

    private frmLogin login;
    private String user;
    private String clave;
    clsUsuario usuario;

    public MyWorker(frmLogin login, String user, String clave) {
        this.login = login;
        this.user = user;
        this.clave = clave;

    }

    /**
     * Metodo que realiza la tarea pesada
     *
     * @throws java.lang.Exception
     */
    @Override
    protected Void doInBackground() throws Exception {
        login.setIcono();
        clsGestorUsuario gestorUsuario = new clsGestorUsuario();
        usuario = gestorUsuario.validarUsuario(user, clave);
        return null;
    }

    /**
     * Resultado en pantalla
     */
    @Override
    protected void done() {
        login.validarUsuario(usuario);
    }

}
