/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.usuario;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import logica.clsConexion;
import logica.logger.UseLogger;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Gloriana
 */
public class clsGestorUsuario extends clsConexion {

    public clsGestorUsuario() {
        super();
    }

    public clsUsuario validarUsuario(String usuario, String clave) {
        clsUsuario user = null;
        try {
            //Nos conectamos 
            conectarBD();
            //Busca los datos del profesor
            String call = "{CALL proc_buscarUsuario(?,?)}";
            //Preparamos la sentecia
            obj_Procedimiento = getConexion().prepareCall(call);
            //Identificacion
            obj_Procedimiento.setString(1, usuario);
            obj_Procedimiento.setString(2, clave);
            rs = obj_Procedimiento.executeQuery();
            if (rs.next()) {
                user = new clsUsuario();
                user.setUsuario(rs.getString(1));
                user.setEstado(rs.getString(3));
                user.setNivelSeguridad(rs.getInt(4));
            }
        } catch (SQLException ex) {
            desconectarBD();
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
        }

        desconectarBD();
        return user;
    }

    public boolean cambiarClaves(String usuario, String claveActual_, String claveNueva_) {
        boolean rpta = false;
        try {
            //Nombre del procedimiento almacenado y como espera tres parametros
            //le ponemos 3 interrogantes
            String call = "{CALL proc_cambiarClave(?,?,?)}";

            //Preparamos la sentecia
            conectarBD();
            obj_Procedimiento = getConexion().prepareCall(call);
            //tipo de identificacion
            obj_Procedimiento.setString(1, usuario);
            //Identificacion
            obj_Procedimiento.setString(2, claveActual_);
            //primer nombre
            obj_Procedimiento.setString(3, claveNueva_);
            //segundo nombre            
            rpta = obj_Procedimiento.executeUpdate() == 1;
            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rpta;
    }

    public ResultSet cargarUsuarios() {
        try {
            //Nombre del procedimiento almacenado
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_cargarUsuarios}");

            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    public clsUsuario buscarIdUsuario(String idUsuario) {
        clsUsuario usuario = null;
        try {
            //Nos conectamos 
            conectarBD();
            //Busca los datos del profesor
            String call = "{CALL proc_buscarDatosUsuario(?)}";
            //Preparamos la sentecia
            obj_Procedimiento = getConexion().prepareCall(call);
            //Identificacion
            obj_Procedimiento.setString(1, idUsuario);
            rs = obj_Procedimiento.executeQuery();
            if (rs.next()) {
                usuario = new clsUsuario();
                usuario.setIdUsuario(rs.getString(1));
                usuario.setUsuario(rs.getString(2));
                usuario.setClave(rs.getString(3));
                usuario.setNombre(rs.getString(4));
                usuario.setApellidos(rs.getString(5));
                if ("1".equals(rs.getString(6))) {
                    usuario.setEstado("Activo");
                } else {
                    usuario.setEstado("Inactivo");
                }
                usuario.setNivel(rs.getString(7));
            }
            desconectarBD();
        } catch (SQLException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        return usuario;
    }

    public boolean eliminarUsuario(String idUsuario) {
        boolean rpta = false;
        try {
            //Obtenemos la conexion
            conectarBD();
            //Preparamos la inserccion 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_eliminarUsuario(?)}");
            obj_Procedimiento.setString(1, idUsuario);
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta = obj_Procedimiento.executeUpdate() == 1 ? true : false;

            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        return rpta;
    }

    public boolean editarUsuario(clsUsuario usuario) {
        boolean rpta1 = false, rpta2 = false;
        try {
            //Obtenemos la conexion
            conectarBD();

            //Decimos que vamos a crear una transaccion
            getConexion().setAutoCommit(false);

            //Preparamos la inserccion 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_editarUsuario(?,?,?,?,?,?)}");
            obj_Procedimiento.setString(1, usuario.getIdUsuario());
            obj_Procedimiento.setString(2, usuario.getUsuario());
            obj_Procedimiento.setString(3, usuario.getClave());
            obj_Procedimiento.setString(4, usuario.getNombre());
            obj_Procedimiento.setString(5, usuario.getApellidos());
            obj_Procedimiento.setString(6, usuario.getEstado());

            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta1 = obj_Procedimiento.executeUpdate() == 1 ? true : false;

            rs = seleccionar("SELECT tblnivelesseguridad.IDNIVEL FROM tblnivelesseguridad WHERE tblnivelesseguridad.DESCRIPCIONNIVEL LIKE '" + usuario.getNivel() + "'");
            int idNivel = 0;
            if (rs.next()) {
                idNivel = rs.getInt(1);
            }
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_editarUsuarioNivel(?,?)}");
            obj_Procedimiento.setString(1, usuario.getIdUsuario());
            obj_Procedimiento.setInt(2, idNivel);
            rpta2 = obj_Procedimiento.executeUpdate() == 1 ? true : false;


            if (rpta1 && rpta2) {
                //Confirmamos la transaccion
                getConexion().commit();
            } else {
                //Negamos la transaccion
                getConexion().rollback();
            }
            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex) {
                new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            }
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex) {
                new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            }
            desconectarBD();
        }
        return rpta1 && rpta2;
    }

    public boolean guardarUsuario(clsUsuario usuario) {
        boolean rpta1 = false, rpta2 = false;
        try {
            //Obtenemos la conexion
            conectarBD();

            //Decimos que vamos a crear una transaccion
            getConexion().setAutoCommit(false);

            //Preparamos la inserccion 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_guardarUsuario(?,?,?,?,?,?)}");
            obj_Procedimiento.setString(1, usuario.getIdUsuario());
            obj_Procedimiento.setString(2, usuario.getUsuario());
            obj_Procedimiento.setString(3, usuario.getClave());
            obj_Procedimiento.setString(4, usuario.getNombre());
            obj_Procedimiento.setString(5, usuario.getApellidos());
            obj_Procedimiento.setString(6, usuario.getEstado());

            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta1 = obj_Procedimiento.executeUpdate() == 1 ? true : false;

            rs = seleccionar("SELECT tblnivelesseguridad.IDNIVEL FROM tblnivelesseguridad WHERE tblnivelesseguridad.DESCRIPCIONNIVEL LIKE '" + usuario.getNivel() + "'");
            int idNivel = 0;
            if (rs.next()) {
                idNivel = rs.getInt(1);
            }
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_guardarUsuarioNivel(?,?)}");
            obj_Procedimiento.setString(1, usuario.getIdUsuario());
            obj_Procedimiento.setInt(2, idNivel);
            rpta2 = obj_Procedimiento.executeUpdate() == 1 ? true : false;


            if (rpta1 && rpta2) {
                //Confirmamos la transaccion
                getConexion().commit();
            } else {
                //Negamos la transaccion
                getConexion().rollback();
            }
            desconectarBD();
        } catch (SQLException e) {
           new UseLogger().writeLog(this.getClass().getName(), e.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex) {
                new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            }
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex) {
                new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            }
            desconectarBD();
        }
        return rpta1 && rpta2;
    }

    public ResultSet cargarNivelesSeguridad() {

        rs = seleccionar("SELECT DESCRIPCIONNIVEL FROM tblnivelesseguridad");

        return rs;
    }

    public boolean comprobarUsuario(String usuario) {
        boolean resp = false;
        conectarBD();
        rs = seleccionar("SELECT tblusuarios.USUARIO FROM tblusuarios WHERE tblusuarios.USUARIO LIKE '" + usuario + "'");
        try {
            if (rs.next()) {
                resp = true;
            }
        } catch (SQLException ex) {
            desconectarBD();
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
        }
        desconectarBD();
        return resp;
    }

    public void cargarReporteUsuarios() {
        String logotipo = "/reportes/logoinvenio.png";
        conectarBD();
        JasperReport reporte;
        JasperPrint reporte_view;
        try {
            //direccion del archivo JASPER
            URL in = this.getClass().getResource("/reportes/usuarios/rptUsuarios.jasper");
            reporte = (JasperReport) JRLoader.loadObject(in);

            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();           
            parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));            
            //-----------------------------------
            reporte_view = JasperFillManager.fillReport(reporte, parametros, getConexion());
            JasperViewer.viewReport(reporte_view, false);

        } catch (JRException ex) {
           new UseLogger().writeLog(this.getClass().getName(), ex.toString());
        }
        desconectarBD();
    }

    public String buscarIdUsuarioXusuario(String usuario) {
        String nombre ="";
        try {
            //Nos conectamos 
            conectarBD();
            //Busca los datos del profesor
            String call = "{CALL proc_buscarUsuarioXUsuario(?)}";
            //Preparamos la sentecia
            obj_Procedimiento = getConexion().prepareCall(call);
            //Identificacion
            obj_Procedimiento.setString(1, usuario);
            ResultSet rs1 = obj_Procedimiento.executeQuery();
            if (rs1.next()) {
                nombre = rs1.getString(1);
            }
            desconectarBD();
        } catch (SQLException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        return nombre;        
    }

    public clsUsuario verificarNivelUsuario(String usuario) {
        clsUsuario user = null;
        try {
            //Nos conectamos 
            conectarBD();
            //Busca los datos del profesor
            String call = "{CALL proc_verificarNivelUsuario(?)}";
            //Preparamos la sentecia
            obj_Procedimiento = getConexion().prepareCall(call);
            //Identificacion
            obj_Procedimiento.setString(1, usuario);
            rs = obj_Procedimiento.executeQuery();
            if (rs.next()) {
                user = new clsUsuario();
                user.setUsuario(rs.getString(1));
                user.setNivelSeguridad(rs.getInt(2));
            }
        } catch (SQLException ex) {
            desconectarBD();
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
        }

        desconectarBD();
        return user;
    }
}
