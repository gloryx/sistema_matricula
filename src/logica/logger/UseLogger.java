/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.logger;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gloriana
 */
public class UseLogger {
    // Always use the classname, this way you can refactor  
    private final static Logger LOGGER = Logger.getLogger(UseLogger.class  
            .getName());  
  
    public void writeLog(String clase, String error) {  
        LOGGER.log(Level.INFO, "{0}\n{1}", new Object[]{error, clase});
    }  
  
   
}
