/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.logger;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 *
 * @author Gloriana
 */
public class MyLogger {

    private static FileHandler fileTxt;
    private static SimpleFormatter formatterTxt;

    static public void setUp() {
        // Create Logger          
        Logger logger = Logger.getLogger("");
        logger.setLevel(Level.INFO);
        try {
            fileTxt = new FileHandler(System.getProperty("user.dir") + "\\mylog.txt", true);
        } catch (IOException | SecurityException ex) {
            throw new RuntimeException("Error al inicializar el logger. " + ex.getLocalizedMessage());
        }
        // Create txt Formatter  
        formatterTxt = new SimpleFormatter();
        fileTxt.setFormatter(formatterTxt);
        logger.addHandler(fileTxt);
    }
}
