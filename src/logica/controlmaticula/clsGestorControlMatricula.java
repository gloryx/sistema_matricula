/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.controlmaticula;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import logica.clsConexion;
import logica.logger.UseLogger;

/**
 *
 * @author Gloriana
 */
public class clsGestorControlMatricula extends clsConexion {

    public ResultSet buscarEstudiante(String cedula) {
        try {
            //Nombre del procedimiento almacenado y como espera tres parametros
            //le ponemos 3 interrogantes
            String call = "{CALL proc_buscarEstudianteControl(?)}";
            //Preparamos la sentecia
            obj_Procedimiento = getConexion().prepareCall(call);
            //Identificacion
            obj_Procedimiento.setString(1, cedula);
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;

    }

    public boolean deshabilitarEstudianteMat(String identificacion) {
        boolean rpta = false;
        try {
            //Obtenemos la conexion
            conectarBD();
            //Preparamos la inserccion 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_deshabilitarEstMat(?)}");
            obj_Procedimiento.setString(1, identificacion);
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta = obj_Procedimiento.executeUpdate() == 1;

            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        return rpta;
    }

    public boolean habilitarEstudianteMat(String identificacion) {
        boolean rpta = false;
        try {
            //Obtenemos la conexion
            conectarBD();
            //Preparamos la inserccion 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_habilitarEstMat(?)}");
            obj_Procedimiento.setString(1, identificacion);
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta = obj_Procedimiento.executeUpdate() == 1;

            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        return rpta;
    }

    public boolean habilitarEstudianteMat(ArrayList<String> identificaciones) {
        try {
            //Obtenemos la conexion
            conectarBD();
            for (String id : identificaciones) {
                //Preparamos la inserccion 1
                rs = seleccionar("select * from tblcontrolmatricula where identificacion like '"
                        + id + "'");
                if (!rs.next()) {
                    obj_Procedimiento = getConexion().prepareCall("{CALL proc_habilitarEstMat(?)}");
                    obj_Procedimiento.setString(1, id);
                    //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
                    //registro de forma correcta los datos
                    obj_Procedimiento.executeUpdate();
                }
            }
            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        return true;
    }

    public synchronized DefaultTableModel cargarEstudiantesGrupoOrigen(ResultSet rs) {
        DefaultTableModel modelo = null;
        ResultSetMetaData metaDatos;
        Object[] etiquetas;

        final int numeroColumnas;
        try {
            metaDatos = rs.getMetaData();
            numeroColumnas = metaDatos.getColumnCount();
            etiquetas = new Object[numeroColumnas + 1];
            // Se obtiene cada una de las etiquetas para cada columna
            for (int i = 0; i < numeroColumnas; i++) {
                // Nuevamente, para ResultSetMetaData la primera columna es la 1.
                etiquetas[i] = metaDatos.getColumnLabel(i + 1).toUpperCase();
            }
            etiquetas[numeroColumnas] = "MOVER";
            modelo = new javax.swing.table.DefaultTableModel(
                    new Object[][]{},
                    etiquetas);

            while (rs.next()) {
                // Se crea un array que será una de las filas de la tabla.
                Object[] fila = new Object[numeroColumnas + 1]; // Hay tres columnas en la tabla

                // Se rellena cada posición del array con una de las columnas de la tabla en base de datos.
                int i;
                for (i = 0; i < numeroColumnas; i++) {
                    fila[i] = rs.getObject(i + 1); // El primer indice en rs es el 1, no el cero, por eso se suma 1.
                }
                fila[i] = Boolean.FALSE;
                // Se añade al modelo la fila completa.
                modelo.addRow(fila);
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return modelo;
    }//=========================================================================

    public boolean moverEstGrupos(ArrayList<String> identificaciones, String codigo, int numeroOrigen, int numeroDestino) {
        boolean rpta1 = false;
        try {
            //Obtenemos la conexion
            conectarBD();
            //Decimos que vamos a crear una transaccion
            getConexion().setAutoCommit(false);
            for (String identificacion : identificaciones) {
                obj_Procedimiento = getConexion().prepareCall("{CALL proc_MoverEstGrupo(?,?,?,?)}");
                obj_Procedimiento.setString(1, identificacion);
                obj_Procedimiento.setString(2, codigo);
                obj_Procedimiento.setInt(3, numeroOrigen);
                obj_Procedimiento.setInt(4, numeroDestino);
                //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
                //registro de forma correcta los datos
                rpta1 = obj_Procedimiento.executeUpdate() == 1;

                if (!rpta1) {
                    break;
                }
            }

            if (rpta1) {
                //Confirmamos la transaccion
                getConexion().commit();
            } else {
                //Negamos la transaccion
                getConexion().rollback();
            }
            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex) {
                new UseLogger().writeLog(this.getClass().getName(), e.toString());
            }
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex) {
                new UseLogger().writeLog(this.getClass().getName(), e.toString());
            }
            desconectarBD();
        }
        return rpta1;
    }
}
