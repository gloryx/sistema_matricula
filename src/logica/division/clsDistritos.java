/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.division;

/**
 *
 * @author Gloriana
 */
public class clsDistritos {

    int idDistrito;
    String nombreCanton;

    public clsDistritos() {
        this(0, "");
    }

    public clsDistritos(int idDistrito, String nombreCanton) {

        this.idDistrito = idDistrito;
        this.nombreCanton = nombreCanton;
    }

    public int getIdDistrito() {
        return idDistrito;
    }

    public void setIdDistrito(int idDistrito) {
        this.idDistrito = idDistrito;
    }

    public String getNombreCanton() {
        return nombreCanton;
    }

    public void setNombreCanton(String nombreCanton) {
        this.nombreCanton = nombreCanton;
    }
}
