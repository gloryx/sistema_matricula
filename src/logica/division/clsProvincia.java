/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.division;

import java.util.ArrayList;

/**
 *
 * @author Gloriana
 */
public class clsProvincia {

    int idProvincia;
    String provincia;
    ArrayList<clsCantones> cantones;

    public clsProvincia() {
    }

    public clsProvincia(int idProvincia, String provincia) {
        this.idProvincia = idProvincia;
        this.provincia = provincia;
        this.cantones = new ArrayList();

    }

    public int getIdProvincia() {
        return idProvincia;
    }

    public void setIdProvincia(int idProvincia) {
        this.idProvincia = idProvincia;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public ArrayList<clsCantones> getCantones() {
        return cantones;
    }

    public void setCantones(ArrayList<clsCantones> cantones) {
        this.cantones = cantones;
    }
}
