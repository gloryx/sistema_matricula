/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.division;

import java.sql.SQLException;
import java.util.ArrayList;
import logica.clsConexion;
import logica.logger.UseLogger;

/**
 *
 * @author Gloriana
 */
public final class clsGestorDivisionPolitica extends clsConexion {

    private static ArrayList<clsProvincia> provincias;

    public static ArrayList<clsCantones> getCantones(int i) {
        return provincias.get(i).getCantones();
    }

    public static ArrayList<clsDistritos> getDistritos(int provincia, int canton) {
        return provincias.get(provincia).getCantones().get(canton).getDistritos();
    }

    public clsGestorDivisionPolitica() {
        if (provincias == null) {
            clsGestorDivisionPolitica.provincias = new ArrayList<>();
            cargarDatosDivisionPolitica();
        }
    }

    public void cargarDatosDivisionPolitica() {
        try {
            // creamos la conexion
            conectarBD();
            // establecemos que no sea autocommit,
            // asi controlamos la transaccion de manera manual
            getConexion().setAutoCommit(false);
            /* instanciamos el objeto callable statement
             * que usaremos para invocar el SP
             * La cantidad de "?" determina la cantidad
             * parametros que recibe el procedimiento
             */
            // ejecutar el SP
            //carga provincias
            obj_Procedimiento = conexion.prepareCall("{call cargarProvincias}");
            //Definir la Entradas o parametros (?) del Procedimiento Almacenado           
            rs = obj_Procedimiento.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    provincias.add(new clsProvincia(rs.getInt(1), rs.getString(2)));
                }

            }
            //carga cantones
            for (clsProvincia provincia : provincias) {
                obj_Procedimiento = conexion.prepareCall("{call cargarCantones(?)}");
                //Definir la Entradas o parametros (?) del Procedimiento Almacenado     
                obj_Procedimiento.setInt(1, provincia.getIdProvincia());
                rs = obj_Procedimiento.executeQuery();
                if (rs != null) {
                    while (rs.next()) {
                        provincia.getCantones().add(new clsCantones(rs.getInt(1), rs.getString(2)));
                    }

                }
            }
            //carga distritos
            for (clsProvincia provincia : provincias) {
                for (clsCantones canton : provincia.getCantones()) {
                    obj_Procedimiento = conexion.prepareCall("{call cargarDistritos(?,?)}");
                    //Definir la Entradas o parametros (?) del Procedimiento Almacenado     
                    obj_Procedimiento.setInt(1, provincia.getIdProvincia());
                    obj_Procedimiento.setInt(2, canton.getIdCanton());
                    rs = obj_Procedimiento.executeQuery();
                    if (rs != null) {
                        while (rs.next()) {
                            canton.getDistritos().add(new clsDistritos(rs.getInt(1), rs.getString(2)));
                        }

                    }
                }
            }

            // confirmar si se ejecuto sin errores
            getConexion().commit();
            desconectarBD();
        } catch (SQLException e) {
            try {
                // deshacer la ejecucion en caso de error
                getConexion().rollback();
            } catch (SQLException ex) {
               new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            }
            // informar por consola
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
        }catch(Exception ex){
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
        }
    }//=========================================================================

    public static ArrayList<clsProvincia> getProvincias() {
        return provincias;
    }

    public static void setProvincias(ArrayList<clsProvincia> provincias) {
        clsGestorDivisionPolitica.provincias = provincias;
    }

    public int obtenerCodigoCanton(int provincia, int canton) {
        return provincias.get(provincia).cantones.get(canton).getIdCanton();
    }

    public int obtenerCodigoDistrito(int provincia, int canton, int distrito) {
        return provincias.get(provincia).cantones.get(canton).getDistritos().get(distrito).getIdDistrito();
    }
}
