/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.division;

import java.util.ArrayList;

/**
 *
 * @author Gloriana
 */
public class clsCantones {

    private int idCanton;
    private String nombreCanton;
    ArrayList<clsDistritos> distritos;

    public clsCantones() {
        this.idCanton = 0;
        this.nombreCanton = "";
        this.distritos = new ArrayList();
    }

    public clsCantones(int idCanton, String nombreCanton) {
        this.idCanton = idCanton;
        this.nombreCanton = nombreCanton;
        this.distritos = new ArrayList();
    }

    public ArrayList<clsDistritos> getDistritos() {
        return distritos;
    }

    public void setDistritos(ArrayList<clsDistritos> distritos) {
        this.distritos = distritos;
    }

    public int getIdCanton() {
        return idCanton;
    }

    public void setIdCanton(int idCanton) {
        this.idCanton = idCanton;
    }

    public String getNombreCanton() {
        return nombreCanton;
    }

    public void setNombreCanton(String nombreCanton) {
        this.nombreCanton = nombreCanton;
    }
}
