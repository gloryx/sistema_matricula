package logica;

import gui.estudiante.frmEstudianteReportes;
import java.awt.Cursor;
import javax.swing.SwingWorker;

/**
 * @web http://www.jc-mouse.net/
 * @author Mouse
 */
public class MyWorker extends SwingWorker<Void, Void> {

    private final frmEstudianteReportes frm;
    clsConexion conexion;
    int accion;

    public MyWorker(frmEstudianteReportes frm, int accion) {
        this.frm = frm;
        this.accion = accion;
    }

    /**
     * Metodo que realiza la tarea pesada
     *
     * @throws java.lang.Exception
     */
    @Override
    protected Void doInBackground() throws Exception {
        frm.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        if (accion == 1) {
            frm.generarConsulta();
        } else {
            frm.generarExcel();
        }

        return null;
    }

    /**
     * Resultado en pantalla
     */
    @Override
    protected void done() {
        frm.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }

}
