/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.carreras;

/**
 *
 * @author Gloriana
 */
public class clsCurso {
    private String codigoPlan;
    private String codigoCurso;
    private String nombreCurso;
    private String nivel;
    private String creditos;
    private double horasTeoria;
    private double horasLab;
    private double horasIndividuales;

    public clsCurso(String codigoPlan, String codigoCurso, String nombreCurso, String nivel, String creditos, double horasTeoria, double horasLab, double horasIndividuales) {
        this.codigoPlan = codigoPlan;
        this.codigoCurso = codigoCurso;
        this.nombreCurso = nombreCurso;
        this.nivel = nivel;
        this.creditos = creditos;
        this.horasTeoria = horasTeoria;
        this.horasLab = horasLab;
        this.horasIndividuales = horasIndividuales;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getCreditos() {
        return creditos;
    }

    public void setCreditos(String creditos) {
        this.creditos = creditos;
    }

   

    public clsCurso() {
    this("", "", "", "", "", 0.0, 0.0, 0.0);
    }

    public String getCodigoPlan() {
        return codigoPlan;
    }

    public void setCodigoPlan(String codigoPlan) {
        this.codigoPlan = codigoPlan;
    }

    public String getCodigoCurso() {
        return codigoCurso;
    }

    public void setCodigoCurso(String codigoCurso) {
        this.codigoCurso = codigoCurso;
    }

    public String getNombreCurso() {
        return nombreCurso;
    }

    public void setNombreCurso(String nombreCurso) {
        this.nombreCurso = nombreCurso;
    }

    public double getHorasTeoria() {
        return horasTeoria;
    }

    public void setHorasTeoria(double horasTeoria) {
        this.horasTeoria = horasTeoria;
    }

    public double getHorasLab() {
        return horasLab;
    }

    public void setHorasLab(double horasLab) {
        this.horasLab = horasLab;
    }

    public double getHorasIndividuales() {
        return horasIndividuales;
    }

    public void setHorasIndividuales(double horasIndividuales) {
        this.horasIndividuales = horasIndividuales;
    }    
            
}
