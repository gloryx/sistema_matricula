/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.carreras;

import java.awt.HeadlessException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import logica.clsConexion;
import logica.logger.UseLogger;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 *
 * @author Gloriana
 */
public class clsGestorCarreras extends clsConexion {

    public ResultSet cargarCarreras() {
        try {
            //Nombre del procedimiento almacenado
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_cargarCarreras}");

            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }
    
    public ResultSet cargarFacultades() {
        try {
            //Nombre del procedimiento almacenado            
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_cargarFacultades}");
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    public ResultSet cargarTodoFacultades() {
        try {
            //Nombre del procedimiento almacenado            
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_cargarTodoFacultades}");
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    public ResultSet cargarFacultadesCarreras(String facultad) {
        try {
            //Nombre del procedimiento almacenado            
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_cargarFacultadesCarreras(?)}");
            obj_Procedimiento.setString(1, facultad);
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    public ResultSet cargarPlanesCarreras(String carrera) {
        try {
            //Nombre del procedimiento almacenado            
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_cargarPlanesCarrera(?)}");
            obj_Procedimiento.setString(1, carrera);
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    public String buscarCodigoCarrera(String carrera) {
        String codigo = null;
        conectarBD();
        try {
            //Nombre del procedimiento almacenado            
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_buscarCodigoCarrera(?)}");
            obj_Procedimiento.setString(1, carrera);
            rs = obj_Procedimiento.executeQuery();
            if (rs != null) {
                if (rs.next()) {
                    codigo = rs.getString(1);
                }
            }
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        desconectarBD();
        return codigo;
    }

    public String buscarNombreCarrera(String codigo) {
        String nombre = null;
        conectarBD();
        try {
            //Nombre del procedimiento almacenado            
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_buscarNombreCarrera(?)}");
            obj_Procedimiento.setString(1, codigo);
            rs = obj_Procedimiento.executeQuery();
            if (rs != null) {
                if (rs.next()) {
                    nombre = rs.getString(1);
                }
            }
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        desconectarBD();
        return nombre;
    }

    public ResultSet cargarCursosCarreraPlan(String carrera, String codigoPlan) {

        try {
            //Nombre del procedimiento almacenado            
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_cargarCursosPlanesCarreraReq(?,?)}");
            obj_Procedimiento.setString(1, carrera);
            obj_Procedimiento.setString(2, codigoPlan);
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;


    }

    public boolean eliminarFacultad(String codigoFacultad) {
        boolean rpta = false;
        try {
            //Obtenemos la conexion
            conectarBD();
            //Preparamos la inserccion 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_eliminarFacultad(?)}");
            obj_Procedimiento.setString(1, codigoFacultad);
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta = obj_Procedimiento.executeUpdate() == 1 ? true : false;

            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        return rpta;
    }

    public clsFacultad buscarFacultad(String codigoFacultad) {
        clsFacultad facultad = null;
        try {
            //Nos conectamos 
            conectarBD();
            //Busca los datos del profesor
            String call = "{CALL proc_buscarFacultad(?)}";
            //Preparamos la sentecia
            obj_Procedimiento = getConexion().prepareCall(call);
            //Identificacion
            obj_Procedimiento.setString(1, codigoFacultad);
            rs = obj_Procedimiento.executeQuery();
            if (rs.next()) {
                facultad = new clsFacultad();
                facultad.setCodigo(rs.getString(1));
                facultad.setNombre(rs.getString(2));
                facultad.setDescripcion(rs.getString(3));
            }
            desconectarBD();
        } catch (SQLException ex) {
           new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        return facultad;
    }

    public boolean editarFacultad(clsFacultad facultad) {
        boolean rpta = false;
        try {
            //Obtenemos la conexion
            conectarBD();
            //Preparamos la inserccion 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_editarFacultad(?,?,?)}");
            obj_Procedimiento.setString(1, facultad.getCodigo());
            obj_Procedimiento.setString(2, facultad.getNombre());
            obj_Procedimiento.setString(3, facultad.getDescripcion());
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta = obj_Procedimiento.executeUpdate() == 1 ? true : false;

            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        return rpta;
    }

    public boolean guardarFacultad(clsFacultad facultad) {
        boolean rpta = false;
        try {
            //Obtenemos la conexion
            conectarBD();
            //Preparamos la inserccion 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_guardarFacultad(?,?,?)}");
            obj_Procedimiento.setString(1, facultad.getCodigo());
            obj_Procedimiento.setString(2, facultad.getNombre());
            obj_Procedimiento.setString(3, facultad.getDescripcion());
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta = obj_Procedimiento.executeUpdate() == 1 ? true : false;

            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        return rpta;
    }

    public ResultSet cargarTodoCarreras() {
        try {
            //Nombre del procedimiento almacenado            
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_cargarTodoCarreras}");
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    public ResultSet carcarCodigoFacultades() {
        rs = seleccionar("SELECT CODIGOFACULTAD FROM tblfacultad");

        return rs;
    }

    public ResultSet cargarCodigoCarreras() {
        try {
            //Nombre del procedimiento almacenado            
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_cargarCodigosCarrera}");
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    public clsCarrera buscarCarrera(String codigoCarrera) {
        clsCarrera carrera = null;
        try {
            //Nos conectamos 
            conectarBD();
            //Busca los datos del profesor
            String call = "{CALL proc_buscarCarrera(?)}";
            //Preparamos la sentecia
            obj_Procedimiento = getConexion().prepareCall(call);
            //Identificacion
            obj_Procedimiento.setString(1, codigoCarrera);
            rs = obj_Procedimiento.executeQuery();
            if (rs.next()) {
                carrera = new clsCarrera();
                carrera.setFacultad(rs.getString(1));
                carrera.setCodigo(rs.getString(2));
                carrera.setNombre(rs.getString(3));
                carrera.setDescripcion(rs.getString(4));
            }
            desconectarBD();
        } catch (SQLException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        return carrera;
    }

    public boolean eliminarCarrera(String codigoCarrera) {
        boolean rpta = false;
        try {
            //Obtenemos la conexion
            conectarBD();
            //Preparamos la inserccion 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_eliminarCarrera(?)}");
            obj_Procedimiento.setString(1, codigoCarrera);
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta = obj_Procedimiento.executeUpdate() == 1 ? true : false;

            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        return rpta;
    }

    public boolean editarCarrera(clsCarrera carrera) {
        boolean rpta = false;
        try {
            //Obtenemos la conexion
            conectarBD();
            //Preparamos la inserccion 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_editarCarrera(?,?,?,?)}");
            obj_Procedimiento.setString(1, carrera.getFacultad());
            obj_Procedimiento.setString(2, carrera.getCodigo());
            obj_Procedimiento.setString(3, carrera.getNombre());
            obj_Procedimiento.setString(4, carrera.getDescripcion());
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta = obj_Procedimiento.executeUpdate() == 1 ? true : false;

            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        return rpta;
    }

    public boolean guardarCarrera(clsCarrera carrera) {
        boolean rpta = false;
        try {
            //Obtenemos la conexion
            conectarBD();
            //Preparamos la inserccion 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_guardarCarrera(?,?,?,?)}");
            obj_Procedimiento.setString(1, carrera.getFacultad());
            obj_Procedimiento.setString(2, carrera.getCodigo());
            obj_Procedimiento.setString(3, carrera.getNombre());
            obj_Procedimiento.setString(4, carrera.getDescripcion());
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta = obj_Procedimiento.executeUpdate() == 1 ? true : false;

            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        return rpta;
    }

    public ResultSet cargarTodoPlanes() {
        try {
            //Nombre del procedimiento almacenado            
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_cargarTodoPlanes}");
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
           new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    public clsPlan buscarPlan(String codigoPlan) {
        clsPlan plan = null;
        try {
            //Nos conectamos 
            conectarBD();
            //Busca los datos del profesor
            String call = "{CALL proc_buscarPlan(?)}";
            //Preparamos la sentecia
            obj_Procedimiento = getConexion().prepareCall(call);
            //Identificacion
            obj_Procedimiento.setString(1, codigoPlan);
            rs = obj_Procedimiento.executeQuery();
            if (rs.next()) {
                plan = new clsPlan();
                plan.setCodigoPlan(rs.getString(1));
                plan.setCodigoCarrea(rs.getString(2));
                plan.setDescripcion(rs.getString(3));
                plan.setFecha(rs.getString(4));
            }
            desconectarBD();
        } catch (SQLException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            System.out.println("Error buscando carrera " + ex);
            
            desconectarBD();
        }
        return plan;
    }

    public boolean editarPlan(clsPlan plan) {
        boolean rpta = false;
        try {
            //Obtenemos la conexion
            conectarBD();
            //Preparamos la inserccion 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_editarPlan(?,?,?)}");
            obj_Procedimiento.setString(1, plan.getCodigoPlan());
            obj_Procedimiento.setString(2, plan.getCodigoCarrea());
            obj_Procedimiento.setString(3, plan.getDescripcion());
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta = obj_Procedimiento.executeUpdate() == 1 ? true : false;

            desconectarBD();
        } catch (SQLException e) {
           new UseLogger().writeLog(this.getClass().getName(), e.toString());
            System.out.println("Error al guardar plan");
            desconectarBD();
        } catch (Exception ex) {
            System.err.println(ex);
            System.out.println("Error al guardar plan");
            desconectarBD();
        }
        return rpta;
    }

    public boolean guardarPlan(clsPlan plan) {
        boolean rpta1 = false, rpta2 = false;
        try {
            //Obtenemos la conexion
            conectarBD();
            //Decimos que vamos a crear una transaccion
            getConexion().setAutoCommit(false);
            //Preparamos la inserccion 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_guardarPlan(?,?,?)}");
            obj_Procedimiento.setString(1, plan.getCodigoPlan());
            obj_Procedimiento.setString(2, plan.getCodigoCarrea());
            obj_Procedimiento.setString(3, plan.getDescripcion());
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta1 = obj_Procedimiento.executeUpdate() == 1 ? true : false;

            //Preparamos la inserccion 2
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_guardarPlanCarrera(?,?)}");
            obj_Procedimiento.setString(1, plan.getCodigoPlan());
            obj_Procedimiento.setString(2, plan.getCodigoCarrea());
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta2 = obj_Procedimiento.executeUpdate() == 1 ? true : false;

            if (rpta1 && rpta2) {
                //Confirmamos la transaccion
                getConexion().commit();
            } else {
                //Negamos la transaccion
                getConexion().rollback();
            }
            desconectarBD();
        } catch (SQLException e) {
           new UseLogger().writeLog(this.getClass().getName(), e.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex) {
                System.out.println("Error al guarda plan " + ex);
            }
            desconectarBD();
        } catch (Exception ex) {
            System.err.println(ex);
            try {
                getConexion().rollback();
            } catch (SQLException e) {
                System.out.println("Error al guarda profesor " + e);
            }
            desconectarBD();
        }
        return rpta1 && rpta2;
    }

    public boolean eliminarPlan(String codigoPlan) {
        boolean rpta = false;
        try {
            //Obtenemos la conexion
            conectarBD();
            //Preparamos la inserccion 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_eliminarPlan(?)}");
            obj_Procedimiento.setString(1, codigoPlan);
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta = obj_Procedimiento.executeUpdate() == 1 ? true : false;

            desconectarBD();
        } catch (SQLException e) {
           new UseLogger().writeLog(this.getClass().getName(), e.toString());
            System.out.println("Error al eliminar plan");
            desconectarBD();
        } catch (Exception ex) {
            System.err.println(ex);
            System.out.println("Error al eliminar plan");
            desconectarBD();
        }
        return rpta;
    }

    public void imprimirPlanesEstudio(String codigoPlan) {
        String logotipo = "/reportes/logoinvenio.png";
        conectarBD();
        JasperReport reporte;
        JasperPrint reporte_view;
        try {
            //direccion del archivo JASPER
            URL in = this.getClass().getResource("/reportes/carreras/rptPlanCarrera.jasper");
            reporte = (JasperReport) JRLoader.loadObject(in);

            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();
            parametros.put("codigoPlan", codigoPlan);
            parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));

            reporte_view = JasperFillManager.fillReport(reporte, parametros, getConexion());
            JasperViewer.viewReport(reporte_view, false);

        } catch (JRException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            System.out.println(ex);
        }
        desconectarBD();
    }


    public void cargarReporteCarreras() {
        String logotipo = "/reportes/logoinvenio.png";
        conectarBD();
        JasperReport reporte;
        JasperPrint reporte_view;
        try {
            //direccion del archivo JASPER
            URL in = this.getClass().getResource("/reportes/carreras/rptCarreras.jasper");
            reporte = (JasperReport) JRLoader.loadObject(in);

            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();
            parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));
            //-----------------------------------
            reporte_view = JasperFillManager.fillReport(reporte, parametros, getConexion());
            JasperViewer.viewReport(reporte_view, false);

        } catch (JRException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            System.err.print(ex);
        }
        desconectarBD();
    }

    public void cargarReportePlanEstudio() {
        String logotipo = "/reportes/logoinvenio.png";
        conectarBD();
        JasperReport reporte;
        JasperPrint reporte_view;
        try {
            //direccion del archivo JASPER
            URL in = this.getClass().getResource("/reportes/carreras/rptPlanEstudio.jasper");
            reporte = (JasperReport) JRLoader.loadObject(in);

            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();
            parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));
            //-----------------------------------
            reporte_view = JasperFillManager.fillReport(reporte, parametros, getConexion());
            JasperViewer.viewReport(reporte_view, false);

        } catch (JRException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            System.err.print(ex);
        }
        desconectarBD();
    }

    public void cargarReporteFacultades() {
        String logotipo = "/reportes/logoinvenio.png";
        conectarBD();
        JasperReport reporte;
        JasperPrint reporte_view;
        try {
            //direccion del archivo JASPER
            URL in = this.getClass().getResource("/reportes/carreras/rptFacultades.jasper");
            reporte = (JasperReport) JRLoader.loadObject(in);

            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();
            parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));
            //-----------------------------------
            reporte_view = JasperFillManager.fillReport(reporte, parametros, getConexion());
            JasperViewer.viewReport(reporte_view, false);

        } catch (JRException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            System.err.print(ex);
        }
        desconectarBD();
    }

    public ResultSet cargarParaMatricular(String identificacion, String plan) {
        try {
            //Nombre del procedimiento almacenado            
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_paraMatricular(?,?)}");
            obj_Procedimiento.setString(1, identificacion);
            obj_Procedimiento.setString(2, plan);
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
           new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
           new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    public ResultSet buscarCursoAbierto(String plan, String codigoCurso, String annio, String periodo) {
        try {
            //Nombre del procedimiento almacenado            
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_estaAbierto(?,?,?,?)}");
            obj_Procedimiento.setString(1, plan);
            obj_Procedimiento.setString(2, codigoCurso);
            obj_Procedimiento.setString(3, annio);
            obj_Procedimiento.setString(4, periodo);
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
           new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
           new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    public ResultSet buscarRequisitoAprobado(String identificacion, String requisito, String plan) {
        try {
            //Nombre del procedimiento almacenado            
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_buscarRequisitoAprobado(?,?,?)}");
            obj_Procedimiento.setString(1, identificacion);
            obj_Procedimiento.setString(2, requisito);
            obj_Procedimiento.setString(3, plan);
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
           new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
           new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    public ResultSet buscarRequisitos(String codigoPlan,String codigoCurso) {
        try {
            //Nombre del procedimiento almacenado            
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_cargarCursosRequisitos(?,?)}");
            obj_Procedimiento.setString(1, codigoPlan);
            obj_Procedimiento.setString(2, codigoCurso);
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
           new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
           new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    public void estadisticasReporteNotas(String annio, String periodo, String codigoPlan, String carrera) {
        String logotipo = "/reportes/logoinvenio.png";
        conectarBD();
        JasperReport reporte;
        JasperPrint reporte_view;
        try {
            //direccion del archivo JASPER
            URL in = this.getClass().getResource("/reportes/grupos/rptListaDeEstadisticasGruposNotas.jasper");
            reporte = (JasperReport) JRLoader.loadObject(in);

            //Se crea un objeto HashMap
            Map parametros = new HashMap();
            parametros.clear();
            parametros.put("logoinvenio", this.getClass().getResourceAsStream(logotipo));
            parametros.put("annio",annio);
            parametros.put("periodo",periodo);
            parametros.put("codigoPlan",codigoPlan);
            parametros.put("carrera",carrera);
            //-----------------------------------
            reporte_view = JasperFillManager.fillReport(reporte, parametros, getConexion());
            JasperViewer.viewReport(reporte_view, false);

        } catch (JRException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            System.err.print(ex);
        }
        desconectarBD();
    }
    
    public void generarArchivoExcel(String sql) {
        try {
            HSSFWorkbook wb = new HSSFWorkbook();
            HSSFSheet sheet = wb.createSheet("hoja1");
            HSSFRow row = sheet.createRow(0);

            conectarBD();
            rs = seleccionar(sql);
            ResultSetMetaData Datos = rs.getMetaData();
            row = sheet.createRow(0);
            for (int i = 0; i < Datos.getColumnCount(); i++) {
                row.createCell(i).setCellValue(Datos.getColumnName(i + 1));
            }

            int fil = 1;
            while (rs.next()) {
                row = sheet.createRow(fil++);
                for (int col = 0; col < Datos.getColumnCount(); col++) {
                    try {
                        row.createCell(col).setCellValue(rs.getObject(col + 1).toString());
                    } catch (Exception e) {
                        row.createCell(col).setCellValue("");
                    }
                }
            }
            JFileChooser fc = new JFileChooser();

            fc.setSelectedFile(new File("Estadistica.xls"));
            int returnVal = fc.showSaveDialog(null);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                try (FileOutputStream fileOut = new FileOutputStream(file.getPath())) {
                    wb.write(fileOut);
                }

                //Ejecutar archivo de excel
//                String dir=file.getPath();
//                Runtime.getRuntime().exec("cmd /c start " +dir);
                JOptionPane.showMessageDialog(null, "Archivo " + fc.getSelectedFile().getName() + " guarado exitosamente");

            }
        } catch (SQLException | HeadlessException | IOException e) {
            javax.swing.JOptionPane.showMessageDialog(null, "Error al exportar la consulta: " + e);
            desconectarBD();
        }

        desconectarBD();
    }

     public ResultSet estadisticasNotasCursos(String plan, String annio, String periodo) {
        try {
            //Nombre del procedimiento almacenado            
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_estadisticasNotasCursos(?,?,?)}");
            obj_Procedimiento.setString(1, plan);
            obj_Procedimiento.setString(2, annio);
            obj_Procedimiento.setString(3, periodo);
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
           new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
           new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

    public ResultSet buscarRequisitosAprobados(String identificacion, String codigoCurso, String plan) {
        try {
            //Nombre del procedimiento almacenado            
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_buscarRequisitosAprobados(?,?,?)}");
            obj_Procedimiento.setString(1, identificacion);
            obj_Procedimiento.setString(2, codigoCurso);
            obj_Procedimiento.setString(3, plan);
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
           new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
           new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }
    
    public ResultSet buscarRequisitosAprobadosMatriculados(String identificacion, String codigoCurso, String plan) {
        try {
            //Nombre del procedimiento almacenado            
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_buscarRequisitosAprobadosMatriculados(?,?,?)}");
            obj_Procedimiento.setString(1, identificacion);
            obj_Procedimiento.setString(2, codigoCurso);
            obj_Procedimiento.setString(3, plan);
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
           new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
           new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }

}
