/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.carreras;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import logica.clsConexion;
import logica.logger.UseLogger;

/**
 *
 * @author Gloriana
 */
public class clsGestorCurso extends clsConexion {

    public ArrayList<String> cargarCodigoCursos(String carrera, String codigoPlan) {
        ArrayList<String> listaCodigo = new ArrayList();
        conectarBD();
        try {
            //Nombre del procedimiento almacenado            
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_cargarCursosPlanesCarrera(?,?)}");
            obj_Procedimiento.setString(1, carrera);
            obj_Procedimiento.setString(2, codigoPlan);
            rs = obj_Procedimiento.executeQuery();
            while (rs.next()) {
                listaCodigo.add(rs.getString(1));
            }

        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        desconectarBD();
        return listaCodigo;
    }

    public boolean editarCurso(clsCurso curso, ArrayList listaRequisitos) {
        boolean rpta = false, rpta1 = true;
        boolean encontrado = false;
        try {
            //Obtenemos la conexion
            conectarBD();
            getConexion().setAutoCommit(false);
            //Preparamos la inserccion 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_editarCurso(?,?,?,?,?,?,?,?)}");
            obj_Procedimiento.setString(1, curso.getCodigoPlan());
            obj_Procedimiento.setString(2, curso.getCodigoCurso());
            obj_Procedimiento.setString(3, curso.getNombreCurso());
            obj_Procedimiento.setString(4, curso.getNivel());
            obj_Procedimiento.setString(5, curso.getCreditos());
            obj_Procedimiento.setDouble(6, curso.getHorasTeoria());
            obj_Procedimiento.setDouble(7, curso.getHorasLab());
            obj_Procedimiento.setDouble(8, curso.getHorasIndividuales());
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta = obj_Procedimiento.executeUpdate() == 1 ? true : false;

            for (Object o : listaRequisitos) {
                obj_Procedimiento = getConexion().prepareCall("{CALL proc_guardarCursoRequisito(?,?,?)}");
                obj_Procedimiento.setString(1, curso.getCodigoPlan());
                obj_Procedimiento.setString(2, curso.getCodigoCurso());
                obj_Procedimiento.setString(3, o.toString());
                //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
                //registro de forma correcta los datos
                rpta1 = obj_Procedimiento.executeUpdate() == 1 ? true : false;
                if (!rpta) {
                    break;
                }
            }
            if (rpta && rpta1) {
                //Confirmamos la transaccion
                getConexion().commit();
            } else {
                //Negamos la transaccion
                getConexion().rollback();
            }
            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(clsGestorCurso.class.getName()).log(Level.SEVERE, null, ex);
            }
            desconectarBD();
        } catch (Exception ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(clsGestorCurso.class.getName()).log(Level.SEVERE, null, ex);
            }
            desconectarBD();
        }
        return rpta;
    }

    public clsCurso buscarCurso(String codigoPlan, String codigoCurso) {
        clsCurso curso = null;
        try {
            //Nos conectamos 
            conectarBD();
            //Busca los datos del profesor
            String call = "{CALL proc_buscarCurso(?,?)}";
            //Preparamos la sentecia
            obj_Procedimiento = getConexion().prepareCall(call);
            //Identificacion
            obj_Procedimiento.setString(1, codigoPlan);
            obj_Procedimiento.setString(2, codigoCurso);
            rs = obj_Procedimiento.executeQuery();
            if (rs.next()) {
                curso = new clsCurso();
                curso.setCodigoPlan(rs.getString(1));
                curso.setCodigoCurso(rs.getString(2));
                curso.setNombreCurso(rs.getString(3));
                curso.setNivel(rs.getString(4));
                curso.setCreditos(rs.getString(5));
                curso.setHorasTeoria(rs.getDouble(6));
                curso.setHorasLab(rs.getDouble(7));
                curso.setHorasIndividuales(rs.getDouble(8));
            }
            desconectarBD();
        } catch (SQLException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        return curso;
    }

    public boolean guardarCurso(clsCurso curso, ArrayList<String> listaRequisitos) {
        boolean rpta = false, rpta1 = true;
        try {
            //Obtenemos la conexion
            conectarBD();
            getConexion().setAutoCommit(false);
            //Preparamos la inserccion 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_guardarCurso(?,?,?,?,?,?,?,?)}");
            obj_Procedimiento.setString(1, curso.getCodigoPlan());
            obj_Procedimiento.setString(2, curso.getCodigoCurso());
            obj_Procedimiento.setString(3, curso.getNombreCurso());
            obj_Procedimiento.setString(4, curso.getNivel());
            obj_Procedimiento.setString(5, curso.getCreditos());
            obj_Procedimiento.setDouble(6, curso.getHorasTeoria());
            obj_Procedimiento.setDouble(7, curso.getHorasLab());
            obj_Procedimiento.setDouble(8, curso.getHorasIndividuales());
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta = obj_Procedimiento.executeUpdate() == 1 ? true : false;

            for (String o : listaRequisitos) {
                rpta1 = false;
                obj_Procedimiento = getConexion().prepareCall("{CALL proc_guardarCursoRequisito(?,?,?)}");
                obj_Procedimiento.setString(1, curso.getCodigoPlan());
                obj_Procedimiento.setString(2, curso.getCodigoCurso());
                obj_Procedimiento.setString(3, o);
                //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
                //registro de forma correcta los datos
                rpta1 = obj_Procedimiento.executeUpdate() == 1 ? true : false;
                if (!rpta) {
                    break;
                }
            }
            if (rpta && rpta1) {
                //Confirmamos la transaccion
                getConexion().commit();
            } else {
                //Negamos la transaccion
                getConexion().rollback();
            }
            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(clsGestorCurso.class.getName()).log(Level.SEVERE, null, ex);
            }
            desconectarBD();
        } catch (Exception ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(clsGestorCurso.class.getName()).log(Level.SEVERE, null, ex1);
            }
            desconectarBD();
        }
        return rpta;
    }

    public boolean eliminarCurso(String codigoPlan, String codigoCurso) {
        boolean rpta = false, rpta1 = true;
        try {
            //Obtenemos la conexion
            conectarBD();
            //Decimos que vamos a crear una transaccion
            getConexion().setAutoCommit(false);

            rs = seleccionar("SELECT * FROM tblcursosrequisitos WHERE CODIGOPLAN LIKE '" + codigoPlan + "' AND CODIGOCURSO LIKE '" + codigoCurso + "'");
            if (rs.next()) {
                //Preparamos la inserccion 1
                obj_Procedimiento = getConexion().prepareCall("{CALL proc_eliminarRequisitosDelCurso(?,?)}");
                obj_Procedimiento.setString(1, codigoPlan);
                obj_Procedimiento.setString(2, codigoCurso);
                //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
                //registro de forma correcta los datos
                rpta1 = obj_Procedimiento.executeUpdate() == 1 ? true : false;
            }
            //Preparamos la inserccion 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_eliminarCurso(?,?)}");
            obj_Procedimiento.setString(1, codigoPlan);
            obj_Procedimiento.setString(2, codigoCurso);
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta = obj_Procedimiento.executeUpdate() == 1 ? true : false;

            //Decimos que vamos a crear una transaccion
            if (rpta && rpta1) {
                getConexion().setAutoCommit(true);
            } else {
                getConexion().setAutoCommit(false);
            }
            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex1) {
                System.out.println("Error al guarda plan " + ex1);
            }
            desconectarBD();
        } catch (Exception ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            try {
                getConexion().rollback();
            } catch (SQLException ex1) {
                System.out.println("Error al guarda plan " + ex);
            }
            desconectarBD();
        }
        return rpta;
    }

    public String buscarCursosRequisitos(String codigoPlan, String codigoCurso) {
        String listaCodigo = "";
        conectarBD();
        try {
            //Nombre del procedimiento almacenado            
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_buscarCursosRequisitos(?,?)}");
            obj_Procedimiento.setString(1, codigoPlan);
            obj_Procedimiento.setString(2, codigoCurso);
            rs = obj_Procedimiento.executeQuery();
            while (rs.next()) {
                listaCodigo += "\n" + rs.getString(1);
            }

        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        desconectarBD();
        return listaCodigo;
    }

    public ArrayList<String> cargarRequisitosCurso(String codigoPlan, String codigoCurso) {
        ArrayList<String> listaCodigo = new ArrayList();
        conectarBD();
        try {
            //Nombre del procedimiento almacenado            
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_cargarCursosRequisitos(?,?)}");
            obj_Procedimiento.setString(1, codigoPlan);
            obj_Procedimiento.setString(2, codigoCurso);
            rs = obj_Procedimiento.executeQuery();
            while (rs.next()) {
                listaCodigo.add(rs.getString(1));
            }

        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        desconectarBD();
        return listaCodigo;
    }

    public boolean eliminarCursoRequisito(String codigoPlan, String codigoCurso, String codigoRequisito) {
        boolean rpta = false;
        try {
            //Obtenemos la conexion
            conectarBD();

            //Preparamos la inserccion 1
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_eliminarCursoRequisito(?,?,?)}");
            obj_Procedimiento.setString(1, codigoPlan);
            obj_Procedimiento.setString(2, codigoCurso);
            obj_Procedimiento.setString(3, codigoRequisito);
            //Ejecutamos la sentencia y si nos devuelve el valor de 1 es porque
            //registro de forma correcta los datos
            rpta = obj_Procedimiento.executeUpdate() == 1 ? true : false;

            desconectarBD();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        return rpta;
    }

    public boolean buscarUnCursoRequisito(String codigoPlan, String codigoCurso, String codigoRequisito) {
        boolean curso = false;
        try {
            //Nos conectamos 
            conectarBD();
            //Busca los datos del profesor
            String call = "{CALL proc_buscarUnCursoRequisito(?,?,?)}";
            //Preparamos la sentecia
            obj_Procedimiento = getConexion().prepareCall(call);
            //Identificacion
            obj_Procedimiento.setString(1, codigoPlan);
            obj_Procedimiento.setString(2, codigoCurso);
            obj_Procedimiento.setString(3, codigoRequisito);
            rs = obj_Procedimiento.executeQuery();
            if (rs.next()) {
                curso = true;
            }
            desconectarBD();
        } catch (SQLException ex) {
            new UseLogger().writeLog(this.getClass().getName(), ex.toString());
            desconectarBD();
        }
        return curso;
    }

    public ResultSet buscarRequisitosDependen(String codigoPlan, String codigoCurso) {
        try {
            //Nombre del procedimiento almacenado            
            obj_Procedimiento = getConexion().prepareCall("{CALL proc_buscarRequisitosDependen(?,?)}");
            obj_Procedimiento.setString(1, codigoPlan);
            obj_Procedimiento.setString(2, codigoCurso);
            rs = obj_Procedimiento.executeQuery();
        } catch (SQLException e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        } catch (Exception e) {
            new UseLogger().writeLog(this.getClass().getName(), e.toString());
            desconectarBD();
        }
        return rs;
    }
}
