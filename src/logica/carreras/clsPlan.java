/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.carreras;

/**
 *
 * @author Gloriana
 */
public class clsPlan {
    String codigoPlan;
    String codigoCarrea;
    String descripcion;
    String fecha;

    public clsPlan() {
        this("", "", "", "");
    }

    public clsPlan(String codigoPlan, String codigoCarrea, String descripcion, String fecha) {
        this.codigoPlan = codigoPlan;
        this.codigoCarrea = codigoCarrea;
        this.descripcion = descripcion;
        this.fecha = fecha;
    }

    public String getCodigoPlan() {
        return codigoPlan;
    }

    public void setCodigoPlan(String codigoPlan) {
        this.codigoPlan = codigoPlan;
    }

    public String getCodigoCarrea() {
        return codigoCarrea;
    }

    public void setCodigoCarrea(String codigoCarrea) {
        this.codigoCarrea = codigoCarrea;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }    
}
