/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.profesor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import logica.carreras.clsGestorCarreras;
import logica.grupo.clsGestorCursoAbierto;
import logica.logger.UseLogger;
import logica.profesor.clsGestorProfesor;

/**
 *
 * @author Gloriana
 */
public class frmListaDeProfesoresActivos extends javax.swing.JInternalFrame {

    /**
     * Creates new form frmListaDeGruposAbiertos
     */
    public frmListaDeProfesoresActivos() {
        initComponents();
        Calendar fechaAct = Calendar.getInstance();
        int mes = fechaAct.get(Calendar.MONTH) + 1;
        int dia = fechaAct.get(Calendar.DAY_OF_MONTH);
        int annio = fechaAct.get(Calendar.YEAR);

        for (int i = annio - 20; i <= annio; i++) {
            cboAnnio.addItem(i);
        }

        for (int i = annio + 1; i <= annio + 20; i++) {
            cboAnnio.addItem(i);
        }
        cboAnnio.setSelectedItem(annio);
        if (mes >= 1 && mes <= 4) {
            cboPeriodo.setSelectedIndex(0);
            if (mes == 4) {
                cboPeriodo.setSelectedIndex(1);
            }
        } else if (mes >= 5 && mes <= 8) {
            cboPeriodo.setSelectedIndex(1);
            if (mes == 8) {
                cboPeriodo.setSelectedIndex(2);
            }
        } else {
            cboPeriodo.setSelectedIndex(2);
            if (mes == 12) {
                cboPeriodo.setSelectedIndex(0);
            }
        }
        cargarCarreras();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        cboCarreras = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblProfesores = new javax.swing.JTable();
        btnBuscarCursosAbiertos = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        cboAnnio = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        cboPeriodo = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        cboPlanEstudios = new javax.swing.JComboBox();
        lblTotal = new javax.swing.JLabel();
        btnExportarExcel = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Lista de profesores activos por periodo");
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/img/win/teacher.png"))); // NOI18N

        jLabel1.setText("Carrera:");

        cboCarreras.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cboCarrerasItemStateChanged(evt);
            }
        });
        cboCarreras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboCarrerasActionPerformed(evt);
            }
        });

        tblProfesores.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "IDENTIFICACION", "PRIMERAPELLIDO", "SEGUNDOAPELLIDO", "NOMBRE", "CORREOELECTRONICO"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, true, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblProfesores.getTableHeader().setReorderingAllowed(false);
        tblProfesores.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblProfesoresMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblProfesores);

        btnBuscarCursosAbiertos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/tool/find.png"))); // NOI18N
        btnBuscarCursosAbiertos.setText("Buscar");
        btnBuscarCursosAbiertos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarCursosAbiertosActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Periodo"));

        jLabel3.setText("Periodo:");

        jLabel4.setText("Año:");

        cboPeriodo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "I", "II", "III", "IV" }));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(jLabel3))
                .addGap(0, 0, 0)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cboPeriodo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cboAnnio, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(cboAnnio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(cboPeriodo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5))
        );

        jLabel2.setText("Plan de estudios:");

        cboPlanEstudios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboPlanEstudiosActionPerformed(evt);
            }
        });

        lblTotal.setText("TOTAL: 0");

        btnExportarExcel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/tool/exportoexcel.png"))); // NOI18N
        btnExportarExcel.setText("Exportar");
        btnExportarExcel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExportarExcelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblTotal)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cboCarreras, 0, 563, Short.MAX_VALUE)
                            .addComponent(cboPlanEstudios, 0, 563, Short.MAX_VALUE))
                        .addGap(27, 27, 27)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnBuscarCursosAbiertos, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnExportarExcel, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel1, jLabel2});

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnBuscarCursosAbiertos, btnExportarExcel});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(cboCarreras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(cboPlanEstudios, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnExportarExcel)
                    .addComponent(btnBuscarCursosAbiertos))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 352, Short.MAX_VALUE)
                .addGap(6, 6, 6)
                .addComponent(lblTotal))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cboCarrerasItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cboCarrerasItemStateChanged

    }//GEN-LAST:event_cboCarrerasItemStateChanged

    private void cboCarrerasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboCarrerasActionPerformed
        if (cboCarreras.getSelectedIndex() > 0) {
            clsGestorCarreras gestorCarreras = new clsGestorCarreras();
            gestorCarreras.conectarBD();
            ResultSet rs = gestorCarreras.cargarPlanesCarreras(cboCarreras.getSelectedItem().toString());
            if (rs != null) {
                try {
                    cboPlanEstudios.removeAllItems();
                    cboPlanEstudios.addItem("ELEGIR...");
                    while (rs.next()) {
                        cboPlanEstudios.addItem(rs.getString(1));
                    }
                } catch (SQLException ex) {
                    new UseLogger().writeLog(this.getName(), ex.toString());
                    gestorCarreras.desconectarBD();
                }
            }
            gestorCarreras.desconectarBD();
        }
    }//GEN-LAST:event_cboCarrerasActionPerformed

    private void tblProfesoresMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblProfesoresMouseClicked

    }//GEN-LAST:event_tblProfesoresMouseClicked

    private void btnBuscarCursosAbiertosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarCursosAbiertosActionPerformed
        cboPlanEstudiosActionPerformed(evt);
    }//GEN-LAST:event_btnBuscarCursosAbiertosActionPerformed

    private void cboPlanEstudiosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboPlanEstudiosActionPerformed
        if (cboCarreras.getSelectedIndex() > 0 && cboPlanEstudios.getSelectedIndex() > 0) {
            cargarCursosAbiertosCarreraProfesor(cboPlanEstudios.getSelectedItem().toString(), cboAnnio.getSelectedItem().toString(),
                    cboPeriodo.getSelectedItem().toString());
        } else {
            DefaultTableModel modeloT = new javax.swing.table.DefaultTableModel(
                    new Object[][]{},
                    new String[]{
                        "IDENTIFICACION", "PRIMERAPELLIDO", "SEGUNDOAPELLIDO", "NOMBRE", "CORREOELECTRONICO"
                    }) {
                boolean[] canEdit = new boolean[]{
                    false, false, false
                };

                @Override
                public boolean isCellEditable(int rowIndex, int columnIndex) {
                    return canEdit[columnIndex];
                }
            };
            tblProfesores.setModel(modeloT);
        }
    }//GEN-LAST:event_cboPlanEstudiosActionPerformed

    private void btnExportarExcelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExportarExcelActionPerformed
        if (tblProfesores.getRowCount() > 0) {
            new clsGestorProfesor().generarArchivoExcel(cboCarreras.getSelectedItem().toString(), tblProfesores);
        } else {
            JOptionPane.showMessageDialog(rootPane, "No hay datos que exportar", "Lista de profesores activos",
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnExportarExcelActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscarCursosAbiertos;
    private javax.swing.JButton btnExportarExcel;
    private javax.swing.JComboBox cboAnnio;
    private javax.swing.JComboBox cboCarreras;
    private javax.swing.JComboBox cboPeriodo;
    private javax.swing.JComboBox cboPlanEstudios;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JTable tblProfesores;
    // End of variables declaration//GEN-END:variables

    private void cargarCarreras() {
        clsGestorCarreras gestorCarreras = new clsGestorCarreras();
        gestorCarreras.conectarBD();
        ResultSet rs = gestorCarreras.cargarCarreras();
        if (rs != null) {
            cboCarreras.addItem("ELEGIR...");
            try {
                while (rs.next()) {
                    cboCarreras.addItem(rs.getString(1));
                }
            } catch (SQLException ex) {
                new UseLogger().writeLog(this.getName(), ex.toString());
            }
        }
        gestorCarreras.desconectarBD();
    }
//==============================================================================

    private void cargarCursosAbiertosCarreraProfesor(String plan,
            String annio, String periodo) {
        clsGestorCursoAbierto gestorCursoAbierto = new clsGestorCursoAbierto();
        gestorCursoAbierto.conectarBD();
        ResultSet rs = gestorCursoAbierto.cargarCursosAbiertosProfesor(plan, annio, periodo);
        if (rs != null) {
            tblProfesores.setModel(gestorCursoAbierto.cargarEnTabla(rs));
            lblTotal.setText("TOTAL: " + tblProfesores.getRowCount());
        }
        gestorCursoAbierto.desconectarBD();
    }
    //==========================================================================

}
