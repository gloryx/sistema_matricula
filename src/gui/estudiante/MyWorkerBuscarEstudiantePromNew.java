/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.estudiante;

import java.awt.Cursor;
import javax.swing.SwingWorker;
import logica.clsConexion;

/**
 *
 * @author Gloriana
 */
public class MyWorkerBuscarEstudiantePromNew extends SwingWorker<Void, Void> {

    private frmBuscarEstudiantePromediosNew frm;
    clsConexion conexion;

    public MyWorkerBuscarEstudiantePromNew(frmBuscarEstudiantePromediosNew frm) {
        this.frm = frm;
    }

    /**
     * Metodo que realiza la tarea pesada
     *
     * @throws java.lang.Exception
     */
    @Override
    protected Void doInBackground() throws Exception {
        frm.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        frm.otrosFiltrosEstudiantesEmpadronados();
        return null;
    }

    /**
     * Resultado en pantalla
     */
    @Override
    protected void done() {
        frm.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }
}