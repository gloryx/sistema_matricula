/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.estudiante;

import gui.frmPrincipal;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;
import java.io.File;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.DefaultFormatterFactory;
import logica.clsValidar;
import logica.division.clsCantones;
import logica.division.clsDistritos;
import logica.division.clsGestorDivisionPolitica;
import logica.division.clsProvincia;
import logica.estudiante.clsEstudiante;
import logica.estudiante.clsGestorEstudiante;
import logica.logger.UseLogger;

/**
 *
 * @author Gloriana
 */
public class frmEstudianteNuevo extends javax.swing.JInternalFrame {

    clsGestorDivisionPolitica gdp;
    clsGestorEstudiante gestorEst;
    UseLogger logger;

    
    public frmEstudianteNuevo() {
        initComponents();
        logger = new UseLogger();
        Calendar fechaAct = Calendar.getInstance();
        fechaAct.getTime().getTime();
        txtAnnioIngreso.setText("" + fechaAct.get(Calendar.YEAR));
        int mes = fechaAct.get(Calendar.MONTH) + 1;
        if (mes >= 1 && mes <= 4) {
            cboPeriodoIngreso.setSelectedIndex(0);
        } else if (mes >= 5 && mes <= 8) {
            cboPeriodoIngreso.setSelectedIndex(1);
        } else {
            cboPeriodoIngreso.setSelectedIndex(2);
        }

        gdp = new clsGestorDivisionPolitica();
        cboProvincia.removeAllItems();
        cboProvincia.addItem("ELEGIR...");
        for (clsProvincia p : clsGestorDivisionPolitica.getProvincias()) {
            cboProvincia.addItem(p.getProvincia());
        }
        
        btnGuardarFoto.setVisible(false);
        btnImprimir.setVisible(false);
        btnEliminar.setVisible(false);
        gestorEst = new clsGestorEstudiante();
        if(this.getTitle().equals("Estudiante nuevo")){
            cargarCarnetSiguiente();
        }
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tabbedEstudiante = new javax.swing.JTabbedPane();
        pnlDatosPersonales = new javax.swing.JPanel();
        fotoEstudiante = new jcFoto.jcFoto();
        btnGuardarFoto = new javax.swing.JButton();
        pnlDatosP = new javax.swing.JPanel();
        cboGenero = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        txtPrimerNombre = new javax.swing.JTextField();
        dateFechaNac = new datechooser.beans.DateChooserCombo();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtSegundoNombre = new javax.swing.JTextField();
        txtPrimerApellido = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        cboTipoIdentificacion = new javax.swing.JComboBox();
        txtIdentificacion = new javax.swing.JFormattedTextField();
        txtSegundoApellido = new javax.swing.JTextField();
        lblCarnet = new javax.swing.JLabel();
        txtCarnet = new javax.swing.JTextField();
        pnlDatosAcademicos = new javax.swing.JPanel();
        pnlDatosA = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        dateFechaIngreso = new datechooser.beans.DateChooserCombo();
        txtAnnioIngreso = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        cboPeriodoIngreso = new javax.swing.JComboBox();
        jLabel12 = new javax.swing.JLabel();
        txtInstitucionProcedencia = new javax.swing.JTextField();
        txtUltimoAnnioColegio = new javax.swing.JTextField();
        txtUltimoGrado = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        pnlDatosContacto = new javax.swing.JPanel();
        pnlDatosC = new javax.swing.JPanel();
        jLabel21 = new javax.swing.JLabel();
        txtCorreElectronico = new javax.swing.JTextField();
        txtTelefonoTrabajo = new javax.swing.JTextField();
        txtTelefonoMovil = new javax.swing.JTextField();
        txtTelefonoCasa = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        cboCanton = new javax.swing.JComboBox();
        cboDistrito = new javax.swing.JComboBox();
        jLabel18 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtOtrasSenas = new javax.swing.JTextArea();
        jLabel22 = new javax.swing.JLabel();
        cboProvincia = new javax.swing.JComboBox();
        jLabel20 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        pnlOtraInformacion = new javax.swing.JPanel();
        pnlDatosOtra = new javax.swing.JPanel();
        jLabel33 = new javax.swing.JLabel();
        txtProfesionPadre = new javax.swing.JTextField();
        txtNombreEncargado = new javax.swing.JTextField();
        txtNombrePadre = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        cboEstadoCivil = new javax.swing.JComboBox();
        txtParentesco = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        txtProfesionMadre = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        txtViveCon = new javax.swing.JTextField();
        txtNombreMadre = new javax.swing.JTextField();
        pnlDatosOtra1 = new javax.swing.JPanel();
        txtPadecimientoMedico = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        txtPersonaEmergencia = new javax.swing.JTextField();
        jLabel34 = new javax.swing.JLabel();
        txtMedicamentos = new javax.swing.JTextField();
        jLabel35 = new javax.swing.JLabel();
        txtTelefonoEmergencia = new javax.swing.JTextField();
        jLabel37 = new javax.swing.JLabel();
        jToolBar2 = new javax.swing.JToolBar();
        jButton1 = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnImprimir = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Estudiante nuevo");
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/img/win/student.png"))); // NOI18N

        tabbedEstudiante.setToolTipText("");

        pnlDatosPersonales.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        fotoEstudiante.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnGuardarFoto.setText("Guardar foto");
        btnGuardarFoto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarFotoActionPerformed(evt);
            }
        });

        cboGenero.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Mujer", "Hombre" }));
        cboGenero.setToolTipText("Genero");

        jLabel3.setText("Primer nombre:");

        txtPrimerNombre.setToolTipText("Primer nombre");

        dateFechaNac.setNothingAllowed(false);
        dateFechaNac.setBehavior(datechooser.model.multiple.MultyModelBehavior.SELECT_SINGLE);

        jLabel6.setText("Segundo apellido:");

        jLabel7.setText("Genero:");

        jLabel1.setText("Tipo de identificación:");

        jLabel27.setText("Fecha nacimiento:");

        jLabel4.setText("Segundo nombre:");

        jLabel5.setText("Primer apellido:");

        txtSegundoNombre.setToolTipText("Segundo nombre");

        txtPrimerApellido.setToolTipText("Primer apellido");

        jLabel2.setText("Identificación:");

        cboTipoIdentificacion.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Nacional", "Extranjero" }));
        cboTipoIdentificacion.setToolTipText("Tipo de identificación");
        cboTipoIdentificacion.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cboTipoIdentificacionItemStateChanged(evt);
            }
        });
        cboTipoIdentificacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboTipoIdentificacionActionPerformed(evt);
            }
        });

        try {
            txtIdentificacion.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("#-####-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtIdentificacion.setToolTipText("Ejp: 1-0111-0111");

        txtSegundoApellido.setToolTipText("Segundo apellido");

        lblCarnet.setText("Carné:");

        txtCarnet.setEditable(false);
        txtCarnet.setToolTipText("Segundo apellido");

        javax.swing.GroupLayout pnlDatosPLayout = new javax.swing.GroupLayout(pnlDatosP);
        pnlDatosP.setLayout(pnlDatosPLayout);
        pnlDatosPLayout.setHorizontalGroup(
            pnlDatosPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDatosPLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDatosPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlDatosPLayout.createSequentialGroup()
                        .addGroup(pnlDatosPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlDatosPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, Short.MAX_VALUE))
                    .addGroup(pnlDatosPLayout.createSequentialGroup()
                        .addComponent(lblCarnet, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(64, 64, 64)))
                .addGroup(pnlDatosPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(dateFechaNac, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboGenero, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSegundoApellido, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPrimerApellido, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSegundoNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPrimerNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtIdentificacion, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboTipoIdentificacion, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCarnet, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pnlDatosPLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel2, jLabel27, jLabel3, jLabel4, jLabel5, jLabel6, jLabel7});

        pnlDatosPLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {cboGenero, cboTipoIdentificacion, dateFechaNac, txtIdentificacion, txtPrimerApellido, txtPrimerNombre, txtSegundoApellido, txtSegundoNombre});

        pnlDatosPLayout.setVerticalGroup(
            pnlDatosPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlDatosPLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDatosPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(lblCarnet, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCarnet))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlDatosPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cboTipoIdentificacion, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlDatosPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtIdentificacion)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlDatosPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtPrimerNombre)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlDatosPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtSegundoNombre)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlDatosPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtPrimerApellido)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlDatosPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtSegundoApellido)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlDatosPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cboGenero)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlDatosPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dateFechaNac, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pnlDatosPLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {cboTipoIdentificacion, txtCarnet});

        javax.swing.GroupLayout pnlDatosPersonalesLayout = new javax.swing.GroupLayout(pnlDatosPersonales);
        pnlDatosPersonales.setLayout(pnlDatosPersonalesLayout);
        pnlDatosPersonalesLayout.setHorizontalGroup(
            pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDatosPersonalesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlDatosP, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(fotoEstudiante, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnGuardarFoto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(425, 425, 425))
        );
        pnlDatosPersonalesLayout.setVerticalGroup(
            pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDatosPersonalesLayout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlDatosP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(pnlDatosPersonalesLayout.createSequentialGroup()
                        .addComponent(fotoEstudiante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnGuardarFoto)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tabbedEstudiante.addTab("Datos personales", pnlDatosPersonales);

        pnlDatosAcademicos.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel8.setText("Último año de colegio:");

        jLabel13.setText("Periodo ingreso:");

        dateFechaIngreso.setNothingAllowed(false);

        txtAnnioIngreso.setToolTipText("Año de ingreso a INVENIO");

        jLabel10.setText("Institución procedencia:");

        jLabel9.setText("Año de ingreso:");

        cboPeriodoIngreso.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "I", "II", "III", "IV" }));

        jLabel12.setText("Fecha ingreso:");

        txtInstitucionProcedencia.setToolTipText("Nombre del colegio de procedencia");
        txtInstitucionProcedencia.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtInstitucionProcedenciaKeyTyped(evt);
            }
        });

        txtUltimoAnnioColegio.setToolTipText("Año que finalizó colegio");

        txtUltimoGrado.setText("BACHILLERATO SECUNDARIA");
        txtUltimoGrado.setToolTipText("Último grado obtenido");

        jLabel11.setText("Último grado académico:");

        javax.swing.GroupLayout pnlDatosALayout = new javax.swing.GroupLayout(pnlDatosA);
        pnlDatosA.setLayout(pnlDatosALayout);
        pnlDatosALayout.setHorizontalGroup(
            pnlDatosALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDatosALayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDatosALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9)
                    .addComponent(jLabel13)
                    .addComponent(jLabel8)
                    .addComponent(jLabel11)
                    .addComponent(jLabel10)
                    .addComponent(jLabel12))
                .addGap(18, 18, 18)
                .addGroup(pnlDatosALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtUltimoAnnioColegio)
                    .addComponent(txtUltimoGrado)
                    .addComponent(txtAnnioIngreso)
                    .addComponent(dateFechaIngreso, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cboPeriodoIngreso, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtInstitucionProcedencia))
                .addContainerGap())
        );
        pnlDatosALayout.setVerticalGroup(
            pnlDatosALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDatosALayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDatosALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel9)
                    .addComponent(txtAnnioIngreso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlDatosALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel12)
                    .addComponent(dateFechaIngreso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlDatosALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel13)
                    .addComponent(cboPeriodoIngreso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlDatosALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel8)
                    .addComponent(txtUltimoAnnioColegio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlDatosALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel11)
                    .addComponent(txtUltimoGrado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlDatosALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel10)
                    .addComponent(txtInstitucionProcedencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout pnlDatosAcademicosLayout = new javax.swing.GroupLayout(pnlDatosAcademicos);
        pnlDatosAcademicos.setLayout(pnlDatosAcademicosLayout);
        pnlDatosAcademicosLayout.setHorizontalGroup(
            pnlDatosAcademicosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDatosAcademicosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlDatosA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(404, 404, 404))
        );
        pnlDatosAcademicosLayout.setVerticalGroup(
            pnlDatosAcademicosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDatosAcademicosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlDatosA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tabbedEstudiante.addTab("Datos académicos", pnlDatosAcademicos);

        pnlDatosContacto.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel21.setText("Teléfono trabajo:");

        jLabel17.setText("Correo eléctronico:");

        jLabel15.setText("Teléfono casa:");

        jLabel19.setText("Teléfono móvil:");

        javax.swing.GroupLayout pnlDatosCLayout = new javax.swing.GroupLayout(pnlDatosC);
        pnlDatosC.setLayout(pnlDatosCLayout);
        pnlDatosCLayout.setHorizontalGroup(
            pnlDatosCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDatosCLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDatosCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel17)
                    .addComponent(jLabel15)
                    .addComponent(jLabel19)
                    .addComponent(jLabel21))
                .addGap(41, 41, 41)
                .addGroup(pnlDatosCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtTelefonoCasa, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTelefonoTrabajo, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCorreElectronico, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTelefonoMovil, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pnlDatosCLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txtCorreElectronico, txtTelefonoCasa, txtTelefonoMovil, txtTelefonoTrabajo});

        pnlDatosCLayout.setVerticalGroup(
            pnlDatosCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDatosCLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDatosCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtCorreElectronico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel17))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlDatosCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(txtTelefonoCasa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlDatosCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(txtTelefonoMovil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlDatosCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel21)
                    .addComponent(txtTelefonoTrabajo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(209, Short.MAX_VALUE))
        );

        cboCanton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboCantonActionPerformed(evt);
            }
        });

        jLabel18.setText("Provincia:");

        txtOtrasSenas.setColumns(20);
        txtOtrasSenas.setLineWrap(true);
        txtOtrasSenas.setRows(5);
        jScrollPane1.setViewportView(txtOtrasSenas);

        jLabel22.setText("Cantón:");

        cboProvincia.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cboProvinciaItemStateChanged(evt);
            }
        });
        cboProvincia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboProvinciaActionPerformed(evt);
            }
        });

        jLabel20.setText("Distrito:");

        jLabel23.setText("Otras señas:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel18)
                    .addComponent(jLabel20)
                    .addComponent(jLabel22)
                    .addComponent(jLabel23))
                .addGap(41, 41, 41)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cboProvincia, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboDistrito, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboCanton, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cboProvincia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cboCanton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel22))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cboDistrito, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel20))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel23)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(133, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout pnlDatosContactoLayout = new javax.swing.GroupLayout(pnlDatosContacto);
        pnlDatosContacto.setLayout(pnlDatosContactoLayout);
        pnlDatosContactoLayout.setHorizontalGroup(
            pnlDatosContactoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDatosContactoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlDatosC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        pnlDatosContactoLayout.setVerticalGroup(
            pnlDatosContactoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDatosContactoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDatosContactoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pnlDatosC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(84, Short.MAX_VALUE))
        );

        tabbedEstudiante.addTab("Datos contacto", pnlDatosContacto);

        pnlOtraInformacion.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel33.setText("Profesión padre:");

        txtNombreEncargado.setToolTipText("Correo electrónico");

        jLabel32.setText("Nombre padre:");

        jLabel25.setText("Nombre madre:");

        jLabel28.setText("Estado civil:");

        jLabel24.setText("Nombre encargado:");

        cboEstadoCivil.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "SOLTER@", "CASAD@", "VIUD@", "DIVORCIAD@", "UNION LIBRE" }));

        jLabel26.setText("Vive con:");

        jLabel16.setText("Parentesco:");

        jLabel31.setText("Profesión madre:");

        jLabel36.setText("Persona emergencia:");

        jLabel34.setText("Medicamentos:");

        jLabel35.setText("Padeciento médico:");

        jLabel37.setText("Teléfono emergencia:");

        javax.swing.GroupLayout pnlDatosOtra1Layout = new javax.swing.GroupLayout(pnlDatosOtra1);
        pnlDatosOtra1.setLayout(pnlDatosOtra1Layout);
        pnlDatosOtra1Layout.setHorizontalGroup(
            pnlDatosOtra1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDatosOtra1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDatosOtra1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel35)
                    .addComponent(jLabel34)
                    .addComponent(jLabel36)
                    .addComponent(jLabel37))
                .addGap(41, 41, 41)
                .addGroup(pnlDatosOtra1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtTelefonoEmergencia, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPersonaEmergencia, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtMedicamentos)
                    .addComponent(txtPadecimientoMedico))
                .addGap(4, 4, 4))
        );
        pnlDatosOtra1Layout.setVerticalGroup(
            pnlDatosOtra1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDatosOtra1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDatosOtra1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel34)
                    .addComponent(txtPadecimientoMedico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlDatosOtra1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel35)
                    .addComponent(txtMedicamentos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlDatosOtra1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel36)
                    .addComponent(txtPersonaEmergencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlDatosOtra1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel37, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtTelefonoEmergencia, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout pnlDatosOtraLayout = new javax.swing.GroupLayout(pnlDatosOtra);
        pnlDatosOtra.setLayout(pnlDatosOtraLayout);
        pnlDatosOtraLayout.setHorizontalGroup(
            pnlDatosOtraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDatosOtraLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDatosOtraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel24)
                    .addComponent(jLabel16)
                    .addComponent(jLabel26)
                    .addComponent(jLabel28)
                    .addComponent(jLabel25)
                    .addComponent(jLabel31)
                    .addComponent(jLabel32)
                    .addComponent(jLabel33))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDatosOtraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtViveCon, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNombreMadre, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtParentesco, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboEstadoCivil, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtProfesionPadre, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtProfesionMadre, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNombreEncargado, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNombrePadre, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(pnlDatosOtra1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pnlDatosOtraLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {cboEstadoCivil, txtNombreEncargado, txtNombreMadre, txtNombrePadre, txtParentesco, txtProfesionMadre, txtProfesionPadre, txtViveCon});

        pnlDatosOtraLayout.setVerticalGroup(
            pnlDatosOtraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDatosOtraLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDatosOtraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtNombreEncargado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel24))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlDatosOtraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel16)
                    .addComponent(txtParentesco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlDatosOtraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel26)
                    .addComponent(txtViveCon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlDatosOtraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel28, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(cboEstadoCivil, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlDatosOtraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtNombreMadre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel25))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlDatosOtraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel31)
                    .addComponent(txtProfesionMadre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlDatosOtraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel32)
                    .addComponent(txtNombrePadre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlDatosOtraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtProfesionPadre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel33))
                .addContainerGap())
            .addComponent(pnlDatosOtra1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout pnlOtraInformacionLayout = new javax.swing.GroupLayout(pnlOtraInformacion);
        pnlOtraInformacion.setLayout(pnlOtraInformacionLayout);
        pnlOtraInformacionLayout.setHorizontalGroup(
            pnlOtraInformacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOtraInformacionLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlDatosOtra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        pnlOtraInformacionLayout.setVerticalGroup(
            pnlOtraInformacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOtraInformacionLayout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(pnlDatosOtra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tabbedEstudiante.addTab("Otra información", pnlOtraInformacion);

        jToolBar2.setFloatable(false);
        jToolBar2.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jButton1.setFocusable(false);
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToolBar2.add(jButton1);

        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/tool/save.png"))); // NOI18N
        btnGuardar.setText("Aceptar");
        btnGuardar.setFocusable(false);
        btnGuardar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnGuardar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        jToolBar2.add(btnGuardar);

        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/tool/cancel.png"))); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.setFocusable(false);
        btnCancelar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCancelar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        jToolBar2.add(btnCancelar);

        btnImprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/tool/printmgr.png"))); // NOI18N
        btnImprimir.setText("Imprimir");
        btnImprimir.setFocusable(false);
        btnImprimir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnImprimir.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnImprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprimirActionPerformed(evt);
            }
        });
        jToolBar2.add(btnImprimir);

        btnEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/tool/delete.png"))); // NOI18N
        btnEliminar.setText("Eliminar");
        btnEliminar.setFocusable(false);
        btnEliminar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnEliminar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });
        jToolBar2.add(btnEliminar);

        btnLimpiar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/tool/limpiar.png"))); // NOI18N
        btnLimpiar.setText(" Limpiar");
        btnLimpiar.setActionCommand("Limpiar");
        btnLimpiar.setFocusable(false);
        btnLimpiar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnLimpiar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });
        jToolBar2.add(btnLimpiar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(tabbedEstudiante, javax.swing.GroupLayout.PREFERRED_SIZE, 945, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jToolBar2, javax.swing.GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(53, 53, 53)
                .addComponent(jToolBar2, javax.swing.GroupLayout.PREFERRED_SIZE, 394, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(tabbedEstudiante)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        String mensaje = validarInformacion();
        if (mensaje.equals("")) {
            clsEstudiante est = new clsEstudiante();
            est.setTipoIdentificacion(cboTipoIdentificacion.getSelectedIndex() + 1);
            est.setIdentificacion(txtIdentificacion.getText().replace("-", ""));
            est.setPrimerNombre(clsValidar.quitarCarEsp(txtPrimerNombre.getText()));
            est.setSegundoNombre(clsValidar.quitarCarEsp(txtSegundoNombre.getText()));
            est.setPrimerApellido(clsValidar.quitarCarEsp(txtPrimerApellido.getText()));
            est.setSegundoApellido(clsValidar.quitarCarEsp(txtSegundoApellido.getText()));
            est.setGenero(cboGenero.getSelectedItem().toString().charAt(0));
            String fechaNac = new SimpleDateFormat("dd/MM/yyyy").format(dateFechaNac.getSelectedDate().getTime());
            est.setFechaNac(fechaNac);
            est.setAnnioIngreso(Integer.parseInt(txtAnnioIngreso.getText()));
            String fechaIngreso = new SimpleDateFormat("dd/MM/yyyy").format(dateFechaIngreso.getSelectedDate().getTime());
            est.setFechaIngreso(fechaIngreso);
            est.setPeriodoIngreso(cboPeriodoIngreso.getSelectedItem().toString());
            if (txtUltimoAnnioColegio.getText().equals("")) {
                est.setUltimoAnnioColegio("0");
            } else {
                est.setUltimoAnnioColegio(txtUltimoAnnioColegio.getText());
            }
            est.setUltimoGradoAcademico(txtUltimoGrado.getText().toUpperCase());
            est.setInstitucionProcedencia(clsValidar.quitarCarEsp(txtInstitucionProcedencia.getText()));
            est.setCorreo(txtCorreElectronico.getText().toLowerCase());
            est.setTelefonoCasa(txtTelefonoCasa.getText());
            est.setTelefonoTrabajo(txtTelefonoTrabajo.getText());
            est.setTelefonoMovil(txtTelefonoMovil.getText());
            est.setProvincia(cboProvincia.getSelectedIndex());
            est.setCanton(cboCanton.getSelectedIndex());
            est.setDistrito(cboDistrito.getSelectedIndex());
            est.setOtrasSenas(clsValidar.quitarCarEsp(txtOtrasSenas.getText()));
            est.setNombreEncargado(clsValidar.quitarCarEsp(txtNombreEncargado.getText()));
            est.setParentesco(txtParentesco.getText().toUpperCase());
            est.setViveCon(clsValidar.quitarCarEsp(txtViveCon.getText()));
            est.setEstadoCivil(cboEstadoCivil.getSelectedItem().toString().toUpperCase());
            est.setNombreMadre(clsValidar.quitarCarEsp(txtNombreMadre.getText()));
            est.setProfesionMadre(clsValidar.quitarCarEsp(txtProfesionMadre.getText()));
            est.setNombrePadre(clsValidar.quitarCarEsp(txtNombrePadre.getText()));
            est.setProfesionPadre(clsValidar.quitarCarEsp(txtProfesionPadre.getText()));
            est.setPadecimiento(clsValidar.quitarCarEsp(txtPadecimientoMedico.getText()));
            est.setMedicamentos(clsValidar.quitarCarEsp(txtMedicamentos.getText()));
            est.setPersonaEmergencia(clsValidar.quitarCarEsp(txtPersonaEmergencia.getText()));
            est.setTelefonoEmergencia(txtTelefonoEmergencia.getText());
            est.setCarnet(txtCarnet.getText());
            boolean encontrado = false;
            gestorEst.conectarBD();
            ResultSet rs = gestorEst.buscarEstudiante(est.getIdentificacion());
            try {
                if (rs.next()) {
                    encontrado = true;
                } else {
                    encontrado = false;
                }
            } catch (SQLException ex) {
                logger.writeLog(this.getName(), ex.toString());
                gestorEst.desconectarBD();
            }
            gestorEst.desconectarBD();

            if ("Estudiante nuevo".equals(this.getTitle())) {
                if (!encontrado) {//Si la identificación no se encuentra registrada
                    boolean rpta = gestorEst.insertarEst(est);
                    if (rpta) {
                        JOptionPane.showMessageDialog(rootPane, "Estudiante almacenado exitosamente", "Guardar",
                                JOptionPane.INFORMATION_MESSAGE);
                        gestorEst.registrarBitacora(frmPrincipal.lblUsuario.getText().substring(9, frmPrincipal.lblUsuario.getText().length()),
                                "Registro estudiante " + est.getIdentificacion());
                        limpiarCampos();
                        if (fotoEstudiante.getPathFoto() != null) { //guardar foto escogida
                            if (guardarFoto(est.getIdentificacion())) {
                                JOptionPane.showMessageDialog(rootPane, "Fotografia almacenada exitosamente", "Guardar",
                                        JOptionPane.INFORMATION_MESSAGE);
                            }
                        } else {
                            JOptionPane.showMessageDialog(rootPane, "Estudiante sin fotografía", "Guardar",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                        int resp = JOptionPane.showConfirmDialog(rootPane, "Desea empadronar al estudiante nuevo: " + est.getIdentificacion(),
                                this.getTitle(), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                        if (resp == JOptionPane.YES_OPTION) {
                            cargarEstudianteParaEmpadranar(est.getTipoIdentificacion(), est.getIdentificacion());
                        }

                    }
                } else {//Entra al else si la identificacion ya se encuentra registrada
                    JOptionPane.showMessageDialog(rootPane, "Identificación ya se encuentra registrada", "Guardar estudiante",
                            JOptionPane.ERROR_MESSAGE);
                }
                //EDITAR ESTUDIANTE
            }
            if ("Estudiante modificar".equals(this.getTitle())) {

                if (encontrado) {

                    boolean rpta = gestorEst.editarEst(est);
                    if (rpta) {
                        JOptionPane.showMessageDialog(rootPane, "Estudiante editado exitosamente");
                        gestorEst.registrarBitacora(frmPrincipal.lblUsuario.getText().substring(9, frmPrincipal.lblUsuario.getText().length()),
                                "Edito estudiante " + est.getIdentificacion());
                        if (fotoEstudiante.getPathFoto() != null) {
                            guardarFoto(est.getIdentificacion());
                        }
                    }
                } else {
                    JOptionPane.showMessageDialog(rootPane, "Identificación no encontrada para editar", "Editar Estudiante",
                            JOptionPane.ERROR_MESSAGE);
                }
            }

        } else {//LA VALIDACIÓN ES INCORRECTA
            JOptionPane.showMessageDialog(rootPane, mensaje, this.getTitle(),
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnGuardarActionPerformed
    private void cboCantonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboCantonActionPerformed
        if (cboProvincia.getSelectedIndex() > 0 && cboCanton.getSelectedIndex() > 0) {
            cboDistrito.removeAllItems();
            cboDistrito.addItem("ELEGIR...");
            for (clsDistritos d : clsGestorDivisionPolitica.getDistritos(cboProvincia.getSelectedIndex() - 1, cboCanton.getSelectedIndex() - 1)) {
                cboDistrito.addItem(d.getNombreCanton());
            }
        }
    }//GEN-LAST:event_cboCantonActionPerformed

    private void cboProvinciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboProvinciaActionPerformed
        if (cboProvincia.getSelectedIndex() > 0) {
            cboCanton.removeAllItems();
            cboCanton.addItem("ELEGIR...");
            for (clsCantones c : clsGestorDivisionPolitica.getCantones(cboProvincia.getSelectedIndex() - 1)) {
                cboCanton.addItem(c.getNombreCanton());
            }
        }
    }//GEN-LAST:event_cboProvinciaActionPerformed

    private void cboTipoIdentificacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboTipoIdentificacionActionPerformed
        if (cboTipoIdentificacion.getSelectedIndex() == 0) {
            try {
                txtIdentificacion.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("#-####-####")));
                txtIdentificacion.setToolTipText("Ejp: 1-0111-0111");
            } catch (java.text.ParseException ex) {
                logger.writeLog(this.getName(), ex.toString());
            }
        } else {
            txtIdentificacion.setToolTipText("Pasaporte");
            txtIdentificacion.setFormatterFactory(new DefaultFormatterFactory());
            txtIdentificacion.setText("");
        }
    }//GEN-LAST:event_cboTipoIdentificacionActionPerformed

    private void cboTipoIdentificacionItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cboTipoIdentificacionItemStateChanged
        txtIdentificacion.setText("");
    }//GEN-LAST:event_cboTipoIdentificacionItemStateChanged

    private void cboProvinciaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cboProvinciaItemStateChanged
        cboCanton.removeAllItems();
        cboDistrito.removeAllItems();
    }//GEN-LAST:event_cboProvinciaItemStateChanged

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        limpiarCampos();
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void btnGuardarFotoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarFotoActionPerformed
        if (fotoEstudiante.getPathFoto() != null
                && guardarFoto(txtIdentificacion.getText().replace("-", "").trim())) {
            JOptionPane.showMessageDialog(rootPane, "Fotografia almacenada exitosamente", "Guardar",
                    JOptionPane.INFORMATION_MESSAGE);

        } else {
            JOptionPane.showMessageDialog(rootPane, "No incluye fotografía", "Guardar",
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnGuardarFotoActionPerformed

    private void btnImprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprimirActionPerformed
        gestorEst.cargarReporteEstudiante(txtIdentificacion.getText().replace("-", "").trim());
    }//GEN-LAST:event_btnImprimirActionPerformed

    private void txtInstitucionProcedenciaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtInstitucionProcedenciaKeyTyped
        txtPrimerNombre.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(final KeyEvent e) {
            }
        });
    }//GEN-LAST:event_txtInstitucionProcedenciaKeyTyped

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        int resp = JOptionPane.showConfirmDialog(rootPane, "Esta seguro de elimianr al estudiante " + txtIdentificacion.getText().replace("-", "").trim(), "Eliminar",
                JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (resp == JOptionPane.YES_OPTION) {
            boolean rpta = gestorEst.eliminarEstudiante(txtIdentificacion.getText().replace("-", "").trim());
            if (rpta) {
                JOptionPane.showMessageDialog(rootPane, "Estudiante eliminado exitosamente", "Eliminar",
                        JOptionPane.INFORMATION_MESSAGE);
                gestorEst.registrarBitacora(frmPrincipal.lblUsuario.getText().substring(9, frmPrincipal.lblUsuario.getText().length()),
                        "Elimino estudiante " + txtIdentificacion.getText().replace("-", "").trim());
                limpiarCampos();
                this.dispose();
            } else {
                JOptionPane.showMessageDialog(rootPane, "Error al intentar eliminar", "Eliminar",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_btnEliminarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnGuardarFoto;
    private javax.swing.JButton btnImprimir;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JComboBox cboCanton;
    private javax.swing.JComboBox cboDistrito;
    private javax.swing.JComboBox cboEstadoCivil;
    private javax.swing.JComboBox cboGenero;
    private javax.swing.JComboBox cboPeriodoIngreso;
    private javax.swing.JComboBox cboProvincia;
    private javax.swing.JComboBox cboTipoIdentificacion;
    private datechooser.beans.DateChooserCombo dateFechaIngreso;
    private datechooser.beans.DateChooserCombo dateFechaNac;
    private jcFoto.jcFoto fotoEstudiante;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToolBar jToolBar2;
    private javax.swing.JLabel lblCarnet;
    private javax.swing.JPanel pnlDatosA;
    private javax.swing.JPanel pnlDatosAcademicos;
    private javax.swing.JPanel pnlDatosC;
    private javax.swing.JPanel pnlDatosContacto;
    private javax.swing.JPanel pnlDatosOtra;
    private javax.swing.JPanel pnlDatosOtra1;
    private javax.swing.JPanel pnlDatosP;
    private javax.swing.JPanel pnlDatosPersonales;
    private javax.swing.JPanel pnlOtraInformacion;
    private javax.swing.JTabbedPane tabbedEstudiante;
    private javax.swing.JTextField txtAnnioIngreso;
    private javax.swing.JTextField txtCarnet;
    private javax.swing.JTextField txtCorreElectronico;
    private javax.swing.JFormattedTextField txtIdentificacion;
    private javax.swing.JTextField txtInstitucionProcedencia;
    private javax.swing.JTextField txtMedicamentos;
    private javax.swing.JTextField txtNombreEncargado;
    private javax.swing.JTextField txtNombreMadre;
    private javax.swing.JTextField txtNombrePadre;
    private javax.swing.JTextArea txtOtrasSenas;
    private javax.swing.JTextField txtPadecimientoMedico;
    private javax.swing.JTextField txtParentesco;
    private javax.swing.JTextField txtPersonaEmergencia;
    private javax.swing.JTextField txtPrimerApellido;
    private javax.swing.JTextField txtPrimerNombre;
    private javax.swing.JTextField txtProfesionMadre;
    private javax.swing.JTextField txtProfesionPadre;
    private javax.swing.JTextField txtSegundoApellido;
    private javax.swing.JTextField txtSegundoNombre;
    private javax.swing.JTextField txtTelefonoCasa;
    private javax.swing.JTextField txtTelefonoEmergencia;
    private javax.swing.JTextField txtTelefonoMovil;
    private javax.swing.JTextField txtTelefonoTrabajo;
    private javax.swing.JTextField txtUltimoAnnioColegio;
    private javax.swing.JTextField txtUltimoGrado;
    private javax.swing.JTextField txtViveCon;
    // End of variables declaration//GEN-END:variables

    public void limpiarCampos() {
        limpiarCampos(this.pnlDatosP);
        limpiarCampos(this.pnlDatosC);
        limpiarCampos(this.pnlDatosA);
        limpiarCampos(this.pnlDatosOtra);
        limpiarCampos(this.pnlDatosOtra1);
        txtOtrasSenas.setText("");
        cboTipoIdentificacion.setSelectedIndex(0);
        tabbedEstudiante.setSelectedIndex(0);

        try {
            txtIdentificacion.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("#-####-####")));
            txtIdentificacion.setToolTipText("Ejp: 1-0111-0111");
        } catch (java.text.ParseException ex) {
            logger.writeLog(this.getName(), ex.toString());
        }
        txtIdentificacion.setText("");
        fotoEstudiante.setFoto(new javax.swing.ImageIcon(getClass().getResource("/img/tool/showfoto.jpg")));
        Calendar fechaAct = Calendar.getInstance();
        txtAnnioIngreso.setText("" + fechaAct.get(Calendar.YEAR));
        cboProvincia.setSelectedIndex(0);
        cboEstadoCivil.setSelectedIndex(0);
    }

    public void limpiarCampos(JPanel jPanel) {
        for (int i = 0; jPanel.getComponents().length > i; i++) {
            //Limpia todos los JTextField de un JPanel
            if (jPanel.getComponents()[i] instanceof JTextField) {
                ((JTextField) jPanel.getComponents()[i]).setText("");
            }
        }
    }

    private String validarInformacion() {
        String mensaje = "";
        if (clsValidar.esVacio(txtIdentificacion.getText().replace("-", "").trim())) {
            mensaje = "Ingrese una identificacion valida";
            tabbedEstudiante.setSelectedIndex(0);
            txtIdentificacion.requestFocus();
            return mensaje;
        }
        //VALIDA EL PRIMER NOMBRE REQUERIDO Y NO MAYOR A 25 CARACTERES
        if (clsValidar.esVacio(txtPrimerNombre.getText())) {
            mensaje = "Ingrese primer nombre";
            tabbedEstudiante.setSelectedIndex(0);
            txtPrimerNombre.requestFocus();
            return mensaje;
        } else {
            if (clsValidar.esGrande(txtPrimerNombre.getText(), 25)) {
                txtPrimerNombre.setText(txtPrimerNombre.getText().substring(0, 24));
            }
        }
        //VALIDA EL SEGUNDO NOMBRE NO MAYOR A 25 CARACTERES
        if (!clsValidar.esVacio(txtSegundoNombre.getText())) {
            if (clsValidar.esGrande(txtSegundoNombre.getText(), 25)) {
                txtSegundoNombre.setText(txtSegundoNombre.getText().substring(0, 24));
            }
        }
        //VALIDA EL PRIMER APELLIDO REQUERIDO Y NO MAYOR A 25 CARACTERES
        if (clsValidar.esVacio(txtPrimerApellido.getText())) {
            mensaje = "Ingrese primer apellido";
            tabbedEstudiante.setSelectedIndex(0);
            txtPrimerApellido.requestFocus();
            return mensaje;
        } else {
            if (clsValidar.esGrande(txtPrimerApellido.getText(), 25)) {
                txtPrimerApellido.setText(txtPrimerApellido.getText().substring(0, 24));
            }
        }
        //VALIDA EL SEGUNDO NOMBRE NO MAYOR A 25 CARACTERES
        if (!clsValidar.esVacio(txtSegundoApellido.getText())) {
            if (clsValidar.esGrande(txtSegundoApellido.getText(), 25)) {
                txtSegundoApellido.setText(txtSegundoApellido.getText().substring(0, 24));
            }
        }
        //VALIDA EL ANO DE INGRESO REQUERIDO Y TAMANO 4 CARACTERES
        if (clsValidar.esVacio(txtAnnioIngreso.getText())) {
            mensaje = "Ingrese año de ingreso";
            tabbedEstudiante.setSelectedIndex(1);
            txtAnnioIngreso.requestFocus();
            return mensaje;
        } else {
            if (!clsValidar.esNumero(txtAnnioIngreso.getText())) {
                mensaje = "El año de ingreso debe ser numérico";
                tabbedEstudiante.setSelectedIndex(1);
                txtAnnioIngreso.requestFocus();
                return mensaje;
            } else {
                if (clsValidar.esGrande(txtAnnioIngreso.getText(), 4)) {
                    mensaje = "Formato de año incorrecto";
                    tabbedEstudiante.setSelectedIndex(1);
                    txtAnnioIngreso.requestFocus();
                    return mensaje;
                }
            }
        }
        //VALIDA EL ULTIMO ANNIO COLEGIO NO MAYOR A 11 CARACTERES
        if (!clsValidar.esVacio(txtUltimoAnnioColegio.getText())) {
            if (!clsValidar.esNumero(txtUltimoAnnioColegio.getText())) {
                mensaje = "El año de egreso de colegio debe ser numérico";
                tabbedEstudiante.setSelectedIndex(1);
                txtUltimoAnnioColegio.requestFocus();
                return mensaje;
            } else if (clsValidar.esGrande(txtUltimoAnnioColegio.getText(), 4)) {
                mensaje = "El formato de año de egreso incorrecto";
                tabbedEstudiante.setSelectedIndex(1);
                txtUltimoAnnioColegio.requestFocus();
            }
        }
        //VALIDA EL ULTIMO GRADO ACADEMICO NO MAYOR A 75 CARACTERES
        if (!clsValidar.esVacio(txtUltimoGrado.getText())) {
            if (clsValidar.esGrande(txtUltimoGrado.getText(), 75)) {
                txtUltimoGrado.setText(txtUltimoGrado.getText().substring(0, 74));
            }
        }
        //VALIDA INSTITUCION PROCEDENCIA NO MAYOR A 150 CARACTERES
        if (!clsValidar.esVacio(txtInstitucionProcedencia.getText())) {
            if (clsValidar.esGrande(txtInstitucionProcedencia.getText(), 150)) {
                txtInstitucionProcedencia.setText(txtInstitucionProcedencia.getText().substring(0, 150));
            }
        }
        //VALIDA EL FORMATO DEL CORREO Y EL TAMANIO
        if (!txtCorreElectronico.getText().equals("")) {
            if (clsValidar.esGrande(txtCorreElectronico.getText(), 50)) {
                mensaje = "Correo demasiado grande, no valido";
                tabbedEstudiante.setSelectedIndex(2);
                txtCorreElectronico.requestFocus();
                return mensaje;
            }
        }
        //VALIDA TELEFONO DE LA CASA NO MAYOR A 15 CARACTERES
        if (!clsValidar.esVacio(txtTelefonoCasa.getText())) {
            if (clsValidar.esGrande(txtTelefonoCasa.getText(), 15)) {
                txtTelefonoCasa.setText(txtTelefonoCasa.getText().substring(0, 14));
            }
        }
        //VALIDA TELEFONO MOVIL NO MAYOR A 15 CARACTERES
        if (!clsValidar.esVacio(txtTelefonoMovil.getText())) {
            if (clsValidar.esGrande(txtTelefonoMovil.getText(), 15)) {
                txtTelefonoMovil.setText(txtTelefonoMovil.getText().substring(0, 14));
            }
        }
        //VALIDA TELEFONO TRABAJO NO MAYOR A 15 CARACTERES
        if (!clsValidar.esVacio(txtTelefonoTrabajo.getText())) {
            if (clsValidar.esGrande(txtTelefonoTrabajo.getText(), 15)) {
                txtTelefonoTrabajo.setText(txtTelefonoTrabajo.getText().substring(0, 14));
            }
        }
        if (clsValidar.esVacio(txtTelefonoCasa.getText()) && clsValidar.esVacio(txtTelefonoTrabajo.getText())
                && clsValidar.esVacio(txtTelefonoMovil.getText())) {
            mensaje = "Ingrese al menos un número de teléfono";
            tabbedEstudiante.setSelectedIndex(2);
            txtTelefonoCasa.requestFocus();

            return mensaje;
        }
        if (cboProvincia.getSelectedIndex() == 0) {
            mensaje = "Seleccione una provincia";
            tabbedEstudiante.setSelectedIndex(2);
            cboProvincia.requestFocus();
            cboProvincia.showPopup();
            return mensaje;
        } else if (cboCanton.getSelectedIndex() == 0) {
            mensaje = "Seleccione un cantón";
            tabbedEstudiante.setSelectedIndex(2);
            cboCanton.requestFocus();
            cboCanton.showPopup();
            return mensaje;
        } else if (cboDistrito.getSelectedIndex() == 0) {
            mensaje = "Seleccione un distrito";
            tabbedEstudiante.setSelectedIndex(2);
            cboCanton.requestFocus();
            cboDistrito.showPopup();
            return mensaje;
        }
        //VALIDA OTRAS SENAS NO MAYOR A 150 CARACTERES
        if (!clsValidar.esVacio(txtOtrasSenas.getText())) {
            if (clsValidar.esGrande(txtOtrasSenas.getText(), 150)) {
                txtOtrasSenas.setText(txtOtrasSenas.getText().substring(0, 149));
            }
        }
        //VALIDA NOMBRE ENCARGADO NO MAYOR A 50 CARACTERES
        if (!clsValidar.esVacio(txtNombreEncargado.getText())) {
            if (clsValidar.esGrande(txtNombreEncargado.getText(), 50)) {
                txtNombreEncargado.setText(txtNombreEncargado.getText().substring(0, 49));
            }
        }
        //VALIDA NOMBRE ENCARGADO NO MAYOR A 50 CARACTERES
        if (!clsValidar.esVacio(txtParentesco.getText())) {
            if (clsValidar.esGrande(txtParentesco.getText(), 50)) {
                txtParentesco.setText(txtParentesco.getText().substring(0, 49));
            }
        }
        //VALIDA VIVE CON NO MAYOR A 50 CARACTERES
        if (!clsValidar.esVacio(txtViveCon.getText())) {
            if (clsValidar.esGrande(txtViveCon.getText(), 50)) {
                txtViveCon.setText(txtViveCon.getText().substring(0, 49));
            }
        }
        //VALIDA NOMBRE MADRE NO MAYOR A 75 CARACTERES
        if (!clsValidar.esVacio(txtNombreMadre.getText())) {
            if (clsValidar.esGrande(txtNombreMadre.getText(), 75)) {
                txtNombreMadre.setText(txtNombreMadre.getText().substring(0, 74));
            }
        }
        //VALIDA PROFESION MADRE NO MAYOR A 75 CARACTERES
        if (!clsValidar.esVacio(txtProfesionMadre.getText())) {
            if (clsValidar.esGrande(txtProfesionMadre.getText(), 75)) {
                txtProfesionMadre.setText(txtProfesionMadre.getText().substring(0, 74));
            }
        }
        //VALIDA NOMBRE PADRE NO MAYOR A 75 CARACTERES
        if (!clsValidar.esVacio(txtNombrePadre.getText())) {
            if (clsValidar.esGrande(txtNombrePadre.getText(), 75)) {
                txtNombrePadre.setText(txtNombrePadre.getText().substring(0, 74));
            }
        }
        //VALIDA PROFESION PADRE NO MAYOR A 75 CARACTERES
        if (!clsValidar.esVacio(txtProfesionPadre.getText())) {
            if (clsValidar.esGrande(txtProfesionPadre.getText(), 75)) {
                txtProfesionPadre.setText(txtProfesionPadre.getText().substring(0, 74));
            }
        }
        //VALIDA MEDICAMENTOS NO MAYOR A 100 CARACTERES
        if (!clsValidar.esVacio(txtMedicamentos.getText())) {
            if (clsValidar.esGrande(txtMedicamentos.getText(), 100)) {
                txtMedicamentos.setText(txtMedicamentos.getText().substring(0, 99));
            }
        }
        //VALIDA PADECIMIENTO MEDICO NO MAYOR A 100 CARACTERES
        if (!clsValidar.esVacio(txtPadecimientoMedico.getText())) {
            if (clsValidar.esGrande(txtPadecimientoMedico.getText(), 100)) {
                txtPadecimientoMedico.setText(txtPadecimientoMedico.getText().substring(0, 99));
            }
        }
        //VALIDA PERSONA EMERGENCIA NO MAYOR A 50 CARACTERES
        if (!clsValidar.esVacio(txtPersonaEmergencia.getText())) {
            if (clsValidar.esGrande(txtPersonaEmergencia.getText(), 50)) {
                txtPersonaEmergencia.setText(txtPersonaEmergencia.getText().substring(0, 49));
            }
        }
        if (!clsValidar.esVacio(txtTelefonoEmergencia.getText())) {
            if (clsValidar.esGrande(txtTelefonoEmergencia.getText(), 15)) {
                txtTelefonoEmergencia.setText(txtTelefonoEmergencia.getText().substring(0, 14));
            }
        }
        return mensaje;
    }

    public void cargarEstudiante(clsEstudiante est) {
        txtIdentificacion.setEditable(false);
        btnGuardarFoto.setVisible(true);
        btnImprimir.setVisible(true);
        btnEliminar.setVisible(true);
        if (est != null) {
            if (est.getTipoIdentificacion() - 1 == 0) {
                cboTipoIdentificacion.setSelectedIndex(0);
            } else {
                cboTipoIdentificacion.setSelectedIndex(1);
            }
            txtIdentificacion.setText(est.getIdentificacion());
            txtPrimerNombre.setText(est.getPrimerNombre());
            txtSegundoNombre.setText(est.getSegundoNombre());
            txtPrimerApellido.setText(est.getPrimerApellido());
            txtSegundoApellido.setText(est.getSegundoApellido());
            if (est.getGenero() == 'M') {
                cboGenero.setSelectedIndex(0);
            } else {
                cboGenero.setSelectedIndex(1);
            }
            java.sql.Date date = Date.valueOf(est.getFechaNac());
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            dateFechaNac.setSelectedDate(cal);
            txtAnnioIngreso.setText("" + est.getAnnioIngreso());
            date = Date.valueOf(est.getFechaIngreso());
            cal = Calendar.getInstance();
            cal.setTime(date);
            dateFechaIngreso.setSelectedDate(cal);
            cboPeriodoIngreso.setSelectedItem(est.getPeriodoIngreso());
            txtUltimoAnnioColegio.setText(est.getUltimoAnnioColegio());
            txtUltimoGrado.setText(est.getUltimoGradoAcademico());
            txtInstitucionProcedencia.setText(est.getInstitucionProcedencia());
            txtCorreElectronico.setText(est.getCorreo());
            txtTelefonoCasa.setText(est.getTelefonoCasa());
            txtTelefonoMovil.setText(est.getTelefonoMovil());
            txtTelefonoTrabajo.setText(est.getTelefonoTrabajo());
            cboProvincia.setSelectedIndex(est.getProvincia());
            cboCanton.setSelectedIndex(est.getCanton());
            cboDistrito.setSelectedIndex(est.getDistrito());
            txtOtrasSenas.setText(est.getOtrasSenas());
            txtNombreEncargado.setText(est.getNombreEncargado());
            txtParentesco.setText(est.getParentesco());
            txtViveCon.setText(est.getViveCon());
            cboEstadoCivil.setSelectedItem(est.getEstadoCivil());
            txtNombreMadre.setText(est.getNombreMadre());
            txtProfesionMadre.setText(est.getProfesionMadre());
            txtNombrePadre.setText(est.getNombrePadre());
            txtProfesionPadre.setText(est.getProfesionPadre());
            txtPadecimientoMedico.setText(est.getPadecimiento());
            txtMedicamentos.setText(est.getMedicamentos());
            txtPersonaEmergencia.setText(est.getPersonaEmergencia());
            txtTelefonoEmergencia.setText(est.getTelefonoEmergencia());
            txtCarnet.setText(est.getCarnet());
            Icon foto = gestorEst.cargarFoto(est.getIdentificacion());
            if (foto != null) {
                fotoEstudiante.setFoto(foto);
            }
        }
    }

    private boolean guardarFoto(String identificacion) {
        int dotPos = fotoEstudiante.getPathFoto().lastIndexOf(".");
        String extension = fotoEstudiante.getPathFoto().substring(dotPos);
        String dir = System.getProperty("user.dir") + "\\fotos\\"
                + identificacion + extension;
        File entrada = new File(fotoEstudiante.getPathFoto());
        File salida = new File(dir);
        boolean resp = gestorEst.copyFile(entrada, salida) ? true : false;
        return resp;
    }

    private void cargarEstudianteParaEmpadranar(int tipo, String identificacion) {
        this.dispose();
        boolean estaAbierta = false;
        frmEmpadronarEstudiante frm = null;
        JInternalFrame objetos[] = frmPrincipal.escritorio.getAllFrames();
        for (JInternalFrame v : objetos) {
            if (v instanceof frmEmpadronarEstudiante) {
                frm = (frmEmpadronarEstudiante) v;
                estaAbierta = true;
                break;
            }
        }

        if (!estaAbierta) {
            frm = new frmEmpadronarEstudiante();
            frm.setSize(883, 470);
            frm.show();
            int x = (int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth() / 2 - frm.getWidth() / 2);
            frm.setLocation(x, 0);
            frmPrincipal.escritorio.add(frm);
        }

        try {
            frm.setSelected(true);
            frm.cargarCedula(tipo, identificacion);
        } catch (PropertyVetoException ex) {
            logger.writeLog(this.getName(), ex.toString());
        }
    }

    private void cargarCarnetSiguiente() {
        gestorEst.conectarBD();
        ResultSet rs=gestorEst.seleccionar("select concat(\"UI-SC-\",YEAR(NOW()),\"-\",ifnull(MAX(CONVERT(SUBSTRING(CARNET,12),UNSIGNED INTEGER))+1,1)) AS CARNET from tblestudiante where SUBSTRING(CARNET,7,10) = YEAR(now())");
        try {
            if (rs.next()) {
                txtCarnet.setText(rs.getString(1));
            }
            gestorEst.desconectarBD();
        } catch (SQLException ex) {
            System.err.println(ex.toString());
            gestorEst.desconectarBD();
        }
    }
    
   
   
}
