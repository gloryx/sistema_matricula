/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.estudiante;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.DefaultFormatterFactory;
import logica.TextTransfer;
import logica.estudiante.clsGestorEstudiante;
import logica.logger.UseLogger;

/**
 *
 * @author Gloriana
 */
public class frmEliminarCarreraEmpadronada extends javax.swing.JInternalFrame {

    UseLogger logger;

    /**
     * Creates new form frmEliminarCarreraEmpadronada
     */
    public frmEliminarCarreraEmpadronada() {
        initComponents();
        logger = new UseLogger();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        popPortapapeles = new javax.swing.JPopupMenu();
        popPegarCedula = new javax.swing.JMenuItem();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtPrimerNombre = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        btnBuscarEstudiante = new javax.swing.JButton();
        chkExtranjero = new javax.swing.JCheckBox();
        txtIdentificacion = new javax.swing.JFormattedTextField();
        btnMatricularEstudianteCarrera = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblCursos = new javax.swing.JTable();

        popPegarCedula.setText("Pegar");
        popPegarCedula.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                popPegarCedulaActionPerformed(evt);
            }
        });
        popPortapapeles.add(popPegarCedula);

        setClosable(true);
        setIconifiable(true);
        setTitle("Eliminar carrera empadronada");
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/img/win/student.png"))); // NOI18N

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Estudiante"));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("Identificación:");

        txtPrimerNombre.setEditable(false);
        txtPrimerNombre.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel2.setText("Nombre:");

        btnBuscarEstudiante.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/tool/find.png"))); // NOI18N
        btnBuscarEstudiante.setText("Buscar");
        btnBuscarEstudiante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarEstudianteActionPerformed(evt);
            }
        });

        chkExtranjero.setText("Extranjero");
        chkExtranjero.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                chkExtranjeroStateChanged(evt);
            }
        });
        chkExtranjero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkExtranjeroActionPerformed(evt);
            }
        });

        try {
            txtIdentificacion.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("#-####-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtIdentificacion.setComponentPopupMenu(popPortapapeles);
        txtIdentificacion.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N

        btnMatricularEstudianteCarrera.setText("Eliminar carrera empadronada");
        btnMatricularEstudianteCarrera.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMatricularEstudianteCarreraActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(chkExtranjero)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(txtIdentificacion, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 513, Short.MAX_VALUE))
                    .addComponent(txtPrimerNombre))
                .addGap(10, 10, 10))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnMatricularEstudianteCarrera, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscarEstudiante, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(chkExtranjero)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(txtIdentificacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2)
                            .addComponent(txtPrimerNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnBuscarEstudiante)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnMatricularEstudianteCarrera, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnBuscarEstudiante, btnMatricularEstudianteCarrera});

        tblCursos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "CODIGOPLAN", "CODIGOCARRERA", "NOMBRECARRERA", "ESATDO"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblCursos.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tblCursos);
        if (tblCursos.getColumnModel().getColumnCount() > 0) {
            tblCursos.getColumnModel().getColumn(0).setPreferredWidth(20);
            tblCursos.getColumnModel().getColumn(1).setPreferredWidth(30);
            tblCursos.getColumnModel().getColumn(2).setPreferredWidth(450);
            tblCursos.getColumnModel().getColumn(3).setPreferredWidth(20);
        }

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 847, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 206, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnBuscarEstudianteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarEstudianteActionPerformed
        boolean encontrado = false;
        String identificacion = txtIdentificacion.getText().replaceAll("-", "").trim();

        if (identificacion.equals("")) {
            JOptionPane.showMessageDialog(rootPane, "Ingrese la identificación a buscar",
                    "Matricular estudiante", JOptionPane.ERROR_MESSAGE);
        } else {
            clsGestorEstudiante gestorEst = new clsGestorEstudiante();
            gestorEst.conectarBD();
            ResultSet rs = gestorEst.buscarEstudiante(identificacion);
            if (rs != null) {
                try {
                    String nombre = "";
                    if (rs.next()) {
                        encontrado = true;
                        nombre += rs.getString(3) + " ";
                        nombre += rs.getString(4) + " ";
                        nombre += rs.getString(5) + " ";
                        nombre += rs.getString(6);
                        txtPrimerNombre.setText(nombre);

                    } else {
                        JOptionPane.showMessageDialog(rootPane, "Identificación no encontrada, verifique formato",
                                "Matricular estudiante", JOptionPane.ERROR_MESSAGE);
                    }
                } catch (SQLException ex) {
                    logger.writeLog(this.getName(),ex.toString());
                    gestorEst.desconectarBD();
                }
            }
            gestorEst.desconectarBD();
        }
        if (encontrado) {
            cargarCarrerasEmpadronadas(identificacion);
        }
    }//GEN-LAST:event_btnBuscarEstudianteActionPerformed

    private void chkExtranjeroStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_chkExtranjeroStateChanged
        txtIdentificacion.setText("");
        txtPrimerNombre.setText("");
    }//GEN-LAST:event_chkExtranjeroStateChanged

    private void chkExtranjeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkExtranjeroActionPerformed
        if (!chkExtranjero.isSelected()) {
            try {
                txtIdentificacion.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("#-####-####")));
            } catch (java.text.ParseException ex) {
                logger.writeLog(this.getName(),ex.toString());
            }
        } else {

            txtIdentificacion.setFormatterFactory(new DefaultFormatterFactory());
            txtIdentificacion.setText("");
        }
    }//GEN-LAST:event_chkExtranjeroActionPerformed

    private void btnMatricularEstudianteCarreraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMatricularEstudianteCarreraActionPerformed
        eliminarCarreraEmpadronada();
    }//GEN-LAST:event_btnMatricularEstudianteCarreraActionPerformed

    private void popPegarCedulaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_popPegarCedulaActionPerformed
        TextTransfer textTransfer = new TextTransfer();
        txtIdentificacion.setText(textTransfer.getClipboardContents());
    }//GEN-LAST:event_popPegarCedulaActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscarEstudiante;
    private javax.swing.JButton btnMatricularEstudianteCarrera;
    private javax.swing.JCheckBox chkExtranjero;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JMenuItem popPegarCedula;
    private javax.swing.JPopupMenu popPortapapeles;
    private javax.swing.JTable tblCursos;
    private javax.swing.JFormattedTextField txtIdentificacion;
    private javax.swing.JTextField txtPrimerNombre;
    // End of variables declaration//GEN-END:variables

    private void cargarCarrerasEmpadronadas(String identificacion) {
        DefaultTableModel modelo = new javax.swing.table.DefaultTableModel(
                new Object[][]{},
                new String[]{
                    "CODIGOPLAN", "CODIGOCARRERA", "NOMBRECARRERA", "ESTADO"
                }) {
            boolean[] canEdit = new boolean[]{
                false, false, false, true
            };

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        };


        clsGestorEstudiante gestorEstudiante = new clsGestorEstudiante();
        gestorEstudiante.conectarBD();
        ResultSet rs = gestorEstudiante.cargarCarrerasEmpadronadas(identificacion);
        int activo;
        if (rs != null) {
            try {
                while (rs.next()) {
                    // Hay tres columnas en la tabla
                    Object[] fila = new Object[4]; // Hay tres columnas en la tabla

                    // Se rellena cada posición del array con una de las columnas de la tabla en base de datos.
                    fila[0] = rs.getObject(1);//CODIGOPLAN
                    fila[1] = rs.getObject(2);//CODIGOCARRERA
                    fila[2] = rs.getObject(3);//NOMBRECARRERA
                    //CONDICION
                    activo = rs.getInt(4);
                    switch (activo) {
                        case 1:
                            fila[3] = "ACTIVO";//ESTADO   
                            break;
                        case 2:
                            fila[3] = "EGRESADO";//ESTADO   
                            break;
                        case 3:
                            fila[3] = "DESERTOR";//ESTADO   
                            break;
                    }

                    modelo.addRow(fila);
                }

            } catch (SQLException ex) {
                logger.writeLog(this.getName(),ex.toString());
                gestorEstudiante.desconectarBD();
            }

        }
        tblCursos.setModel(modelo);
        tblCursos.getColumnModel().getColumn(0).setMinWidth(20);
        tblCursos.getColumnModel().getColumn(0).setPreferredWidth(20);
        tblCursos.getColumnModel().getColumn(1).setMinWidth(30);
        tblCursos.getColumnModel().getColumn(1).setPreferredWidth(30);
        tblCursos.getColumnModel().getColumn(2).setMinWidth(450);
        tblCursos.getColumnModel().getColumn(2).setPreferredWidth(450);
        tblCursos.getColumnModel().getColumn(3).setMinWidth(20);
        tblCursos.getColumnModel().getColumn(3).setPreferredWidth(20);
        gestorEstudiante.desconectarBD();

    }

    private void eliminarCarreraEmpadronada() {

        int fila = tblCursos.getSelectedRow();
        if (fila != -1) {
            String identificacion = txtIdentificacion.getText().replaceAll("-", "").trim();
            String plan = tblCursos.getValueAt(fila, 0).toString();
            String carrera = tblCursos.getValueAt(fila, 1).toString();
            int resp = JOptionPane.showConfirmDialog(rootPane, "Esta segur@ de eliminar la carrera " + carrera
                    + " \nal estudiante " + identificacion);
            if (resp == JOptionPane.YES_OPTION) {
                clsGestorEstudiante gestorEstudiante = new clsGestorEstudiante();
                if (gestorEstudiante.eliminarCarreraEmpadronada(identificacion, carrera, plan)) {
                    JOptionPane.showMessageDialog(rootPane, "Padron de carrera eliminada exitosamente",
                            this.getTitle(), JOptionPane.INFORMATION_MESSAGE);
                    this.dispose();
                } else {
                    JOptionPane.showMessageDialog(rootPane, "No se pudo eliminar carrera empadronada",
                            this.getTitle(), JOptionPane.ERROR_MESSAGE);
                }
            }

        } else {
            JOptionPane.showMessageDialog(rootPane, "Seleccione la carrera a matricular",
                    this.getTitle(),
                    JOptionPane.ERROR_MESSAGE);
        }

    }
}
