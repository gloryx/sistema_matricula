/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.carreras;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import logica.carreras.clsCarrera;
import logica.carreras.clsFacultad;
import logica.carreras.clsGestorCarreras;
import logica.clsValidar;
import logica.logger.UseLogger;
import org.jvnet.substance.SubstanceLookAndFeel;
import org.jvnet.substance.utils.SubstanceConstants;

/**
 *
 * @author Gloriana
 */
public class frmNuevaCarrera extends javax.swing.JInternalFrame {
UseLogger logger;
    /**
     * Creates new form frmNuevaCarrera
     */
    clsGestorCarreras gestorCarrera;
    clsCarrera carrera;
    private TableRowSorter trsfiltro; //creamos el filtro
    DefaultTableModel temp;
    String filtro;
    
    public frmNuevaCarrera() {
        initComponents();
        logger = new UseLogger();
        gestorCarrera = new clsGestorCarreras();
        this.btnAceptar.putClientProperty(SubstanceLookAndFeel.BUTTON_SIDE_PROPERTY, SubstanceConstants.Side.RIGHT);
        this.btnCancelar.putClientProperty(SubstanceLookAndFeel.BUTTON_SIDE_PROPERTY, SubstanceConstants.Side.LEFT);
        cargarCarreras();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        winCarrera = new javax.swing.JDialog();
        cboCodigoFacultad = new javax.swing.JComboBox();
        lblFacultad = new javax.swing.JLabel();
        txtCodigoCarrera = new javax.swing.JTextField();
        txtNombreCarrera = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtDescripcionCarrera = new javax.swing.JTextArea();
        btnAceptar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        popCarreras = new javax.swing.JPopupMenu();
        mnuEditar = new javax.swing.JMenuItem();
        mnuEliminar = new javax.swing.JMenuItem();
        toolBar = new javax.swing.JToolBar();
        btnGuardar = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        btnImprimir = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        btnCerrar = new javax.swing.JButton();
        txtFacultad = new javax.swing.JTextField();
        txtCodigo = new javax.swing.JTextField();
        txtNombre = new javax.swing.JTextField();
        txtDescripcion = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblCarrera = new javax.swing.JTable();

        winCarrera.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        winCarrera.setTitle("Guardar aula");
        winCarrera.setModal(true);
        winCarrera.addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                winCarreraWindowOpened(evt);
            }
        });

        cboCodigoFacultad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboCodigoFacultadActionPerformed(evt);
            }
        });

        lblFacultad.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N

        txtDescripcionCarrera.setColumns(20);
        txtDescripcionCarrera.setLineWrap(true);
        txtDescripcionCarrera.setRows(5);
        jScrollPane2.setViewportView(txtDescripcionCarrera);

        btnAceptar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/tool/save.png"))); // NOI18N
        btnAceptar.setText("Aceptar");
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarActionPerformed(evt);
            }
        });

        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/tool/cancel.png"))); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        jLabel1.setText("Código carrera:");

        jLabel2.setText("Nombre:");

        jLabel3.setText("Descripción");

        jLabel4.setText("Código facultad:");

        javax.swing.GroupLayout winCarreraLayout = new javax.swing.GroupLayout(winCarrera.getContentPane());
        winCarrera.getContentPane().setLayout(winCarreraLayout);
        winCarreraLayout.setHorizontalGroup(
            winCarreraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(winCarreraLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(winCarreraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel1)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(winCarreraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(winCarreraLayout.createSequentialGroup()
                        .addGap(84, 84, 84)
                        .addComponent(btnAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(winCarreraLayout.createSequentialGroup()
                        .addGroup(winCarreraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(txtCodigoCarrera, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cboCodigoFacultad, javax.swing.GroupLayout.Alignment.LEADING, 0, 156, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblFacultad, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(txtNombreCarrera)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 512, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        winCarreraLayout.setVerticalGroup(
            winCarreraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(winCarreraLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(winCarreraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(cboCodigoFacultad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblFacultad, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(winCarreraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtCodigoCarrera, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(winCarreraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtNombreCarrera, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(winCarreraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(winCarreraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAceptar)
                    .addComponent(btnCancelar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        mnuEditar.setText("Editar");
        mnuEditar.setToolTipText("");
        mnuEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuEditarActionPerformed(evt);
            }
        });
        popCarreras.add(mnuEditar);

        mnuEliminar.setText("Eliminar");
        mnuEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuEliminarActionPerformed(evt);
            }
        });
        popCarreras.add(mnuEliminar);

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setTitle("Administrar carreras");
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/img/win/carreras.png"))); // NOI18N

        toolBar.setFloatable(false);
        toolBar.setRollover(true);

        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/tool/save.png"))); // NOI18N
        btnGuardar.setText("Guardar");
        btnGuardar.setFocusable(false);
        btnGuardar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnGuardar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        toolBar.add(btnGuardar);

        btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/tool/edit.png"))); // NOI18N
        btnEditar.setText("Editar");
        btnEditar.setFocusable(false);
        btnEditar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnEditar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });
        toolBar.add(btnEditar);

        btnImprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/tool/printmgr.png"))); // NOI18N
        btnImprimir.setText("Imprimir");
        btnImprimir.setFocusable(false);
        btnImprimir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnImprimir.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnImprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprimirActionPerformed(evt);
            }
        });
        toolBar.add(btnImprimir);

        btnEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/tool/delete.png"))); // NOI18N
        btnEliminar.setText("Eliminar");
        btnEliminar.setFocusable(false);
        btnEliminar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnEliminar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });
        toolBar.add(btnEliminar);

        btnCerrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/tool/cancel.png"))); // NOI18N
        btnCerrar.setText("Cerrar");
        btnCerrar.setFocusable(false);
        btnCerrar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCerrar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });
        toolBar.add(btnCerrar);

        txtFacultad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtFacultadKeyTyped(evt);
            }
        });

        txtCodigo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCodigoKeyTyped(evt);
            }
        });

        txtNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNombreKeyTyped(evt);
            }
        });

        txtDescripcion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDescripcionKeyTyped(evt);
            }
        });

        tblCarrera.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "FACULTAD", "CODIGO", "NOMBRECARRERA", "DESCRIPCION"
            }
        ));
        tblCarrera.setComponentPopupMenu(popCarreras);
        tblCarrera.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tblCarrera.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblCarreraMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblCarrera);
        if (tblCarrera.getColumnModel().getColumnCount() > 0) {
            tblCarrera.getColumnModel().getColumn(0).setMinWidth(75);
            tblCarrera.getColumnModel().getColumn(0).setPreferredWidth(75);
            tblCarrera.getColumnModel().getColumn(0).setMaxWidth(75);
            tblCarrera.getColumnModel().getColumn(1).setMinWidth(75);
            tblCarrera.getColumnModel().getColumn(1).setPreferredWidth(75);
            tblCarrera.getColumnModel().getColumn(1).setMaxWidth(75);
        }

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(toolBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(txtFacultad, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(txtNombre, javax.swing.GroupLayout.DEFAULT_SIZE, 350, Short.MAX_VALUE)
                        .addGap(0, 0, 0)
                        .addComponent(txtDescripcion, javax.swing.GroupLayout.DEFAULT_SIZE, 351, Short.MAX_VALUE))
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(toolBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtFacultad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDescripcion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 383, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        winCarrera.setSize(640, 230);
        winCarrera.setTitle("Guardar carrera");
        cargarFacultades();
        cboCodigoFacultad.setSelectedIndex(0);
        txtCodigoCarrera.setText("");
        txtCodigoCarrera.setEnabled(true);
        txtNombreCarrera.setText("");
        txtDescripcionCarrera.setText("");
        lblFacultad.setText("");
        winCarrera.setVisible(true);
    }//GEN-LAST:event_btnGuardarActionPerformed
    
    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        editarCarrera();
    }//GEN-LAST:event_btnEditarActionPerformed
    
    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        eliminarCarrera();
    }//GEN-LAST:event_btnEliminarActionPerformed
    
    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnCerrarActionPerformed
    
    private void txtCodigoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCodigoKeyTyped
        txtCodigo.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(final KeyEvent e) {
                if (!txtCodigo.getText().equals("")) {
                    filtroCodigoCarrera();
                }
            }
        });
        trsfiltro = new TableRowSorter(temp);
        tblCarrera.setRowSorter(trsfiltro);
    }//GEN-LAST:event_txtCodigoKeyTyped
        
    private void txtNombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreKeyTyped
        txtNombre.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(final KeyEvent e) {
                if (!txtNombre.getText().equals("")) {
                    filtroNombre();
                }
            }
        });
        trsfiltro = new TableRowSorter(temp);
        tblCarrera.setRowSorter(trsfiltro);
    }//GEN-LAST:event_txtNombreKeyTyped
    
    private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarActionPerformed
        String mensaje = validarFacultades();
        if (mensaje.equals("")) {
            gestorCarrera = new clsGestorCarreras();
            carrera = new clsCarrera();
            carrera.setFacultad(cboCodigoFacultad.getSelectedItem().toString());
            carrera.setCodigo(txtCodigoCarrera.getText().toUpperCase());
            carrera.setNombre(clsValidar.quitarCarEsp(txtNombreCarrera.getText()));
            carrera.setDescripcion(clsValidar.quitarCarEsp(txtDescripcionCarrera.getText()));
            
            if (winCarrera.getTitle().equals("Editar carrera")) {
                if (gestorCarrera.editarCarrera(carrera)) {
                    cargarCarreras();
                    JOptionPane.showMessageDialog(rootPane, "Información editada exitosamente", this.getTitle(),
                            JOptionPane.INFORMATION_MESSAGE);
                    winCarrera.dispose();
                } else {
                    JOptionPane.showMessageDialog(rootPane, "Problemas al editar información", this.getTitle(),
                            JOptionPane.ERROR_MESSAGE);
                }
            } else {
                
                if (gestorCarrera.buscarFacultad(carrera.getCodigo()) == null) {
                    if (gestorCarrera.guardarCarrera(carrera)) {
                        cargarCarreras();
                        JOptionPane.showMessageDialog(rootPane, "Información almacenada exitosamente", this.getTitle(),
                                JOptionPane.INFORMATION_MESSAGE);
                        winCarrera.dispose();
                    } else {
                        JOptionPane.showMessageDialog(rootPane, "Problemas al guardar información", this.getTitle(),
                                JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(rootPane, "Código de carrera ya se encuentra registrado", this.getTitle(),
                            JOptionPane.ERROR_MESSAGE);
                }
            }
            
        } else {
            JOptionPane.showMessageDialog(rootPane, mensaje, this.getTitle(),
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnAceptarActionPerformed
    
    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        winCarrera.dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed
    
    private void winCarreraWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_winCarreraWindowOpened
        winCarrera.setLocationRelativeTo(tblCarrera);
        ImageIcon img;
        img = new ImageIcon(getClass().getResource("/img/tool/save.png"));
        winCarrera.setIconImage(img.getImage());
    }//GEN-LAST:event_winCarreraWindowOpened
    
    private void txtFacultadKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFacultadKeyTyped
        txtFacultad.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(final KeyEvent e) {
                if (!txtFacultad.getText().equals("")) {
                    filtroFacultad();
                }
            }
        });
        trsfiltro = new TableRowSorter(temp);
        tblCarrera.setRowSorter(trsfiltro);
    }//GEN-LAST:event_txtFacultadKeyTyped
    
    private void txtDescripcionKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDescripcionKeyTyped
        txtDescripcion.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(final KeyEvent e) {
                if (!txtDescripcion.getText().equals("")) {
                    filtroDescripcion();
                }
            }
        });
        trsfiltro = new TableRowSorter(temp);
        tblCarrera.setRowSorter(trsfiltro);
    }//GEN-LAST:event_txtDescripcionKeyTyped
    
    private void cboCodigoFacultadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboCodigoFacultadActionPerformed
        if (cboCodigoFacultad.getSelectedIndex() > 0) {
            gestorCarrera = new clsGestorCarreras();            
            clsFacultad facultad = gestorCarrera.buscarFacultad(cboCodigoFacultad.getSelectedItem().toString());
            if (facultad != null) {
                lblFacultad.setText(facultad.getNombre());
            }
        } else {
            lblFacultad.setText("");
        }
    }//GEN-LAST:event_cboCodigoFacultadActionPerformed

    private void btnImprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprimirActionPerformed
        gestorCarrera.cargarReporteCarreras();
    }//GEN-LAST:event_btnImprimirActionPerformed

    private void mnuEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuEditarActionPerformed
        editarCarrera();
    }//GEN-LAST:event_mnuEditarActionPerformed

    private void mnuEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuEliminarActionPerformed
        eliminarCarrera();
    }//GEN-LAST:event_mnuEliminarActionPerformed

    private void tblCarreraMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblCarreraMouseClicked
        int fila=tblCarrera.getSelectedRow();
        if(fila!=-1){
            if(evt.getClickCount()==2){
                editarCarrera();
            }
        }
    }//GEN-LAST:event_tblCarreraMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnCerrar;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnImprimir;
    private javax.swing.JComboBox cboCodigoFacultad;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblFacultad;
    private javax.swing.JMenuItem mnuEditar;
    private javax.swing.JMenuItem mnuEliminar;
    private javax.swing.JPopupMenu popCarreras;
    private javax.swing.JTable tblCarrera;
    private javax.swing.JToolBar toolBar;
    private javax.swing.JTextField txtCodigo;
    private javax.swing.JTextField txtCodigoCarrera;
    private javax.swing.JTextField txtDescripcion;
    private javax.swing.JTextArea txtDescripcionCarrera;
    private javax.swing.JTextField txtFacultad;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtNombreCarrera;
    private javax.swing.JDialog winCarrera;
    // End of variables declaration//GEN-END:variables

    private void cargarCarreras() {
        gestorCarrera.conectarBD();
        ResultSet rs = gestorCarrera.cargarTodoCarreras();
        if (rs != null) {
            tblCarrera.setModel(gestorCarrera.cargarEnTabla(rs));
        }
        tblCarrera.getColumnModel().getColumn(0).setMinWidth(75);
        tblCarrera.getColumnModel().getColumn(0).setPreferredWidth(75);
        tblCarrera.getColumnModel().getColumn(0).setMaxWidth(75);
        tblCarrera.getColumnModel().getColumn(1).setMinWidth(75);
        tblCarrera.getColumnModel().getColumn(1).setPreferredWidth(75);
        tblCarrera.getColumnModel().getColumn(1).setMaxWidth(75);
        gestorCarrera.desconectarBD();
        temp = (DefaultTableModel) tblCarrera.getModel();
        trsfiltro = new TableRowSorter(temp);
        tblCarrera.setRowSorter(trsfiltro);
    }
    
    private String validarFacultades() {
        String mensaje = "";
        if (cboCodigoFacultad.getSelectedIndex() == 0) {
            mensaje = "Elija la facultad a la que pertenece la carrera";
            cboCodigoFacultad.requestFocus();
            cboCodigoFacultad.showPopup();
            return mensaje;
        }
        if (txtCodigoCarrera.getText().equals("")) {
            mensaje = "Por favor ingrese el código";
            txtCodigoCarrera.requestFocus();
            return mensaje;
        } else {
            if (txtCodigoCarrera.getText().length() > 8) {
                mensaje = "Codigo demasiado largo TAMAÑO MAXIMO 8 caracteres";
                txtCodigoCarrera.requestFocus();
                return mensaje;
            }
        }
        if (txtNombreCarrera.getText().equals("")) {
            mensaje = "Por favor ingrese el nombre";
            txtNombreCarrera.requestFocus();
            return mensaje;
        } else {
            if (txtNombreCarrera.getText().length() > 100) {
                mensaje = "Nombre demasiado largo TAMAÑO MAXIMO 100 caracteres";
                txtNombreCarrera.requestFocus();
                return mensaje;
            }
        }
        
        if (!txtDescripcionCarrera.getText().equals("")) {
            if (txtDescripcionCarrera.getText().length() > 100) {
                mensaje = "Características demasiado largo TAMAÑO MAXIMO 100 caracteres";
                txtDescripcionCarrera.requestFocus();
                return mensaje;
            }
        }
        return mensaje;
    }
    
    public void filtroFacultad() {
        try {
            //Obtiene el valor del JTextField para el filtro
            filtro = txtFacultad.getText().toUpperCase();
            int columna = 0;
            trsfiltro.setRowFilter(RowFilter.regexFilter(txtFacultad.getText().toUpperCase(), columna));
        } catch (Exception ex) {
            logger.writeLog(this.getName(),ex.toString());
        }
    }
    
    public void filtroCodigoCarrera() {
        try {
            //Obtiene el valor del JTextField para el filtro
            filtro = txtCodigo.getText().toUpperCase();
            int columna = 1;
            trsfiltro.setRowFilter(RowFilter.regexFilter(txtCodigo.getText().toUpperCase(), columna));
        } catch (Exception ex) {
            logger.writeLog(this.getName(),ex.toString());
        }
    }
    
    public void filtroNombre() {
        try {
            //Obtiene el valor del JTextField para el filtro
            filtro = txtNombre.getText().toUpperCase();
            int columna = 2;
            trsfiltro.setRowFilter(RowFilter.regexFilter(txtNombre.getText().toUpperCase(), columna));
        } catch (Exception ex) {
            logger.writeLog(this.getName(),ex.toString());
        }
    }
    
    public void filtroDescripcion() {
        try {
            //Obtiene el valor del JTextField para el filtro
            filtro = txtDescripcion.getText().toUpperCase();
            int columna = 3;
            trsfiltro.setRowFilter(RowFilter.regexFilter(txtDescripcion.getText().toUpperCase(), columna));
        } catch (Exception ex) {
            logger.writeLog(this.getName(),ex.toString());
        }
    }
    
    private void cargarFacultades() {
        gestorCarrera = new clsGestorCarreras();
        gestorCarrera.conectarBD();
        ResultSet rs = gestorCarrera.carcarCodigoFacultades();
        cboCodigoFacultad.removeAllItems();
        cboCodigoFacultad.addItem("ELEGIR...");
        try {
            while (rs.next()) {
                cboCodigoFacultad.addItem(rs.getString(1));
            }
        } catch (SQLException ex) {
            logger.writeLog(this.getName(),ex.toString());
            gestorCarrera.desconectarBD();
        }
        gestorCarrera.desconectarBD();
    }

    private void editarCarrera() {
        int fila = tblCarrera.getSelectedRow();
        if (fila == -1) {
            JOptionPane.showMessageDialog(rootPane, "Seleccione de la tabla la información a editar",
                    this.getTitle(), JOptionPane.ERROR_MESSAGE);
        } else {
            gestorCarrera = new clsGestorCarreras();
            carrera = gestorCarrera.buscarCarrera(tblCarrera.getValueAt(fila, 1).toString());
            if (carrera != null) {
                winCarrera.setSize(640, 230);
                winCarrera.setTitle("Editar carrera");
                cargarFacultades();
                cboCodigoFacultad.setSelectedItem(carrera.getFacultad());
                txtCodigoCarrera.setText(carrera.getCodigo());
                txtCodigoCarrera.setEnabled(false);
                txtNombreCarrera.setText(carrera.getNombre());
                txtDescripcionCarrera.setText(carrera.getDescripcion());
                winCarrera.setVisible(true);
            }
        }
    }

    private void eliminarCarrera() {
        int fila = tblCarrera.getSelectedRow();
        if (fila == -1) {
            JOptionPane.showMessageDialog(rootPane, "Seleccione de la tabla la información a eliminar",
                    this.getTitle(), JOptionPane.ERROR_MESSAGE);
        } else {
            int resp = JOptionPane.showConfirmDialog(rootPane, "Esta segur@ de eliminar la información de "
                    + tblCarrera.getValueAt(fila, 1), this.getTitle(), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (resp == JOptionPane.YES_OPTION) {
                gestorCarrera = new clsGestorCarreras();
                if (gestorCarrera.eliminarCarrera(tblCarrera.getValueAt(fila, 1).toString())) {
                    cargarCarreras();
                    JOptionPane.showMessageDialog(rootPane, "Se elimino la información", this.getTitle(),
                            JOptionPane.INFORMATION_MESSAGE);
                    winCarrera.dispose();
                    
                } else {
                    JOptionPane.showMessageDialog(rootPane, "Problemas al eliminar información", this.getTitle(),
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }
}
