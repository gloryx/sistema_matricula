/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import logica.clsImagenFondo;
import org.edisoncor.gui.util.Avatar;
import org.jvnet.substance.SubstanceLookAndFeel;
import org.jvnet.substance.watermark.SubstanceImageWatermark;

/**
 *
 * @author Gloriana
 */
public class frmFondos extends javax.swing.JInternalFrame {

    /**
     * Creates new form frmFondos
     */
    public frmFondos() {
        initComponents();
        List<Avatar> avatares = new ArrayList();
        avatares.add(new Avatar("fondo.jpg", "Imagen 1", (BufferedImage) loadImage("/img/button/fondo_.jpg")));
        avatares.add(new Avatar("fondo1.jpg", "Imagen 2", (BufferedImage) loadImage("/img/button/fondo1_.jpg")));
        avatares.add(new Avatar("fondo2.jpg", "Imagen 3", (BufferedImage) loadImage("/img/button/fondo2_.jpg")));
        avatares.add(new Avatar("fondo3.jpg", "Imagen 4", (BufferedImage) loadImage("/img/button/fondo3_.jpg")));
        avatares.add(new Avatar("fondo4.jpg", "Imagen 5", (BufferedImage) loadImage("/img/button/fondo4_.jpg")));
        avatares.add(new Avatar("fondo5.jpg", "Imagen 6", (BufferedImage) loadImage("/img/button/fondo5_.jpg")));
        avatares.add(new Avatar("fondo6.jpg", "Imagen 7", (BufferedImage) loadImage("/img/button/fondo6_.jpg")));
        avatares.add(new Avatar("fondo7.jpg", "Imagen 8", (BufferedImage) loadImage("/img/button/fondo7_.jpg")));
        avatares.add(new Avatar("fondo8.jpg", "Imagen 9", (BufferedImage) loadImage("/img/button/fondo8_.jpg")));
        panelAvatarChooser1.setAvatars(avatares);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        popAplicarFondo = new javax.swing.JPopupMenu();
        popAplicar = new javax.swing.JMenuItem();
        panelAvatarChooser1 = new org.edisoncor.gui.panel.PanelAvatarChooser();

        popAplicar.setText("Aplicar fondo...");
        popAplicar.setToolTipText("");
        popAplicar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                popAplicarActionPerformed(evt);
            }
        });
        popAplicarFondo.add(popAplicar);

        setClosable(true);
        setTitle("Cambiar fondo...");
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/img/win/picture.png"))); // NOI18N

        panelAvatarChooser1.setComponentPopupMenu(popAplicarFondo);

        javax.swing.GroupLayout panelAvatarChooser1Layout = new javax.swing.GroupLayout(panelAvatarChooser1);
        panelAvatarChooser1.setLayout(panelAvatarChooser1Layout);
        panelAvatarChooser1Layout.setHorizontalGroup(
            panelAvatarChooser1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 384, Short.MAX_VALUE)
        );
        panelAvatarChooser1Layout.setVerticalGroup(
            panelAvatarChooser1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 260, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelAvatarChooser1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelAvatarChooser1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void popAplicarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_popAplicarActionPerformed
        Avatar avatar = panelAvatarChooser1.getSelectedAvatar();
        this.dispose();
        SubstanceLookAndFeel.setCurrentWatermark(new SubstanceImageWatermark(this.getClass().getResourceAsStream("/img/button/" + avatar.getId())));
        frmPrincipal.escritorio.setBorder(new clsImagenFondo("/img/button/" + avatar.getId()));
        pack();
        FileWriter fichero = null;
        PrintWriter pw = null;
        try {
            fichero = new FileWriter("C:\\SISMATUNIVERSIDADACTUALIZADO\\fondo.txt");
            pw = new PrintWriter(fichero);

            pw.println(avatar.getId());

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Nuevamente aprovechamos el finally para 
                // asegurarnos que se cierra el fichero.
                if (null != fichero) {
                    fichero.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }//GEN-LAST:event_popAplicarActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.edisoncor.gui.panel.PanelAvatarChooser panelAvatarChooser1;
    private javax.swing.JMenuItem popAplicar;
    private javax.swing.JPopupMenu popAplicarFondo;
    // End of variables declaration//GEN-END:variables

    private static Image loadImage(String fileName) {
        try {
            return ImageIO.read(JFrame.class.getResource(fileName));
        } catch (IOException ex) {
            System.err.println(ex);
            return null;
        }
    }
}
