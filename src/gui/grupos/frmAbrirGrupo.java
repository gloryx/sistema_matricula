/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.grupos;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import logica.clsValidar;
import logica.grupo.*;
import logica.logger.UseLogger;
import org.jvnet.substance.SubstanceLookAndFeel;
import org.jvnet.substance.utils.SubstanceConstants;

/**
 *
 * @author Gloriana
 */
public class frmAbrirGrupo extends javax.swing.JDialog {
//VARIABLES GLOBALES...

    public static DefaultListModel modelo;
    public static clsCursoAbierto cursoAbierto;
    public static clsGrupo grupo;
    private  UseLogger logger;

    /**
     * Creates new form frmAbrirGrupo
     */
    public frmAbrirGrupo(java.awt.Frame parent, boolean modal,
            String codigo, String nombre, String plan, clsCursoAbierto cursoAbierto) {
        super(parent, modal);
        initComponents();
        logger = new UseLogger();
        modelo = new DefaultListModel();
        lstListraGrupos.setModel(modelo);
        btnAceptar.putClientProperty(SubstanceLookAndFeel.BUTTON_SIDE_PROPERTY, SubstanceConstants.Side.RIGHT);
        btnCancelar.putClientProperty(SubstanceLookAndFeel.BUTTON_SIDE_PROPERTY, SubstanceConstants.Side.LEFT);
        setLocationRelativeTo(null);
        ImageIcon img;
        img = new ImageIcon(getClass().getResource("/img/win/group.png"));
        this.setIconImage(img.getImage());
        txtCodigoCurso.setText(codigo);
        if (nombre.length() > 57) {
            txtNombreCurso.setText(nombre.substring(0, 57) + "...");
        } else {
            txtNombreCurso.setText(nombre);
        }
        txtCodigoPlan.setText(plan);
        txtCantidadMin.setText("" + cursoAbierto.getCantidadMin());
        txtCantidadMax.setText("" + cursoAbierto.getCantidadMax());
        frmAbrirGrupo.cursoAbierto = cursoAbierto;
        modelo.removeAllElements();

        for (clsGrupo g : frmAbrirGrupo.cursoAbierto.getListaGrupos()) {
            modelo.addElement("Grupo #" + g.getNumero());
        }
        treeHorario.setModel(new DefaultTreeModel(null));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        txtCodigoCurso = new javax.swing.JTextField();
        txtNombreCurso = new javax.swing.JTextField();
        txtCodigoPlan = new javax.swing.JTextField();
        txtCantidadMin = new javax.swing.JTextField();
        txtCantidadMax = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        btnCancelar = new javax.swing.JButton();
        btnAceptar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        toolBarGroup = new javax.swing.JToolBar();
        btnNuevoGrupo = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        btnCantidadMaxima = new javax.swing.JButton();
        btnCantidadMinima = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        lstListraGrupos = new javax.swing.JList();
        tabbedPaneDatos = new javax.swing.JTabbedPane();
        pnlHorario = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        treeHorario = new javax.swing.JTree();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Apertura de grupos");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Apertura de cursos"));

        txtCodigoCurso.setEditable(false);
        txtCodigoCurso.setHorizontalAlignment(javax.swing.JTextField.LEFT);

        txtNombreCurso.setEditable(false);
        txtNombreCurso.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        txtNombreCurso.setHorizontalAlignment(javax.swing.JTextField.LEFT);

        txtCodigoPlan.setEditable(false);

        txtCantidadMin.setEditable(false);

        txtCantidadMax.setEditable(false);

        jLabel5.setText("Código plan");

        jLabel7.setText("Cantidad minima:");

        jLabel3.setText("Código curso:");

        jLabel8.setText("Cantidad maxima:");

        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/tool/cancel.png"))); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnAceptar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/tool/save.png"))); // NOI18N
        btnAceptar.setText("Cerrar");
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarActionPerformed(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Creación de grupos"));

        toolBarGroup.setFloatable(false);
        toolBarGroup.setRollover(true);

        btnNuevoGrupo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/tool/new_group.png"))); // NOI18N
        btnNuevoGrupo.setToolTipText("Crear nuevo grupo");
        btnNuevoGrupo.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnNuevoGrupo.setFocusable(false);
        btnNuevoGrupo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnNuevoGrupo.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnNuevoGrupo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoGrupoActionPerformed(evt);
            }
        });
        toolBarGroup.add(btnNuevoGrupo);

        btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/tool/edit_group.png"))); // NOI18N
        btnEditar.setToolTipText("Modificar grupo");
        btnEditar.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnEditar.setFocusable(false);
        btnEditar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnEditar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });
        toolBarGroup.add(btnEditar);

        btnEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/tool/delete_group.png"))); // NOI18N
        btnEliminar.setToolTipText("Eliminar grupo");
        btnEliminar.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnEliminar.setFocusable(false);
        btnEliminar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnEliminar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });
        toolBarGroup.add(btnEliminar);

        btnCantidadMaxima.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/tool/decrease.png"))); // NOI18N
        btnCantidadMaxima.setToolTipText("Cambiar cantidad maxima");
        btnCantidadMaxima.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnCantidadMaxima.setFocusable(false);
        btnCantidadMaxima.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCantidadMaxima.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnCantidadMaxima.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCantidadMaximaActionPerformed(evt);
            }
        });
        toolBarGroup.add(btnCantidadMaxima);

        btnCantidadMinima.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/tool/increase.png"))); // NOI18N
        btnCantidadMinima.setToolTipText("Cambiar cantidad minima");
        btnCantidadMinima.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnCantidadMinima.setFocusable(false);
        btnCantidadMinima.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCantidadMinima.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnCantidadMinima.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCantidadMinimaActionPerformed(evt);
            }
        });
        toolBarGroup.add(btnCantidadMinima);

        lstListraGrupos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstListraGruposMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(lstListraGrupos);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(toolBarGroup, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addComponent(toolBarGroup, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        javax.swing.tree.DefaultMutableTreeNode treeNode1 = new javax.swing.tree.DefaultMutableTreeNode("root");
        treeHorario.setModel(new javax.swing.tree.DefaultTreeModel(treeNode1));
        jScrollPane1.setViewportView(treeHorario);

        javax.swing.GroupLayout pnlHorarioLayout = new javax.swing.GroupLayout(pnlHorario);
        pnlHorario.setLayout(pnlHorarioLayout);
        pnlHorarioLayout.setHorizontalGroup(
            pnlHorarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlHorarioLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 374, Short.MAX_VALUE)
                .addContainerGap())
        );
        pnlHorarioLayout.setVerticalGroup(
            pnlHorarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlHorarioLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 152, Short.MAX_VALUE)
                .addContainerGap())
        );

        tabbedPaneDatos.addTab("Horario", pnlHorario);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(tabbedPaneDatos))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(jLabel7)
                            .addComponent(jLabel5)
                            .addComponent(jLabel3))
                        .addGap(0, 0, 0)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtCantidadMax)
                            .addComponent(txtCantidadMin)
                            .addComponent(txtCodigoPlan)
                            .addComponent(txtCodigoCurso, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNombreCurso)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(txtCodigoCurso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNombreCurso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtCodigoPlan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtCantidadMin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtCantidadMax, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addGap(0, 0, 0)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tabbedPaneDatos, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnCancelar)
                    .addComponent(btnAceptar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {txtCodigoCurso, txtNombreCurso});

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getAccessibleContext().setAccessibleParent(txtCantidadMin);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnNuevoGrupoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoGrupoActionPerformed
        //ESTABLECE EL NUMERO DE GRUPO SEGUN LA CANTIDAD QUE HAY EN LA LISTA
        clsGestorCursoAbierto gestorCursoAbierto = new clsGestorCursoAbierto();
        int numeroGrupo = gestorCursoAbierto.obtenerNumeroDeGrupo(cursoAbierto.getCodigo());
        if (numeroGrupo == 0) {
            frmAbrirGrupo.modelo.addElement("Grupo #1");
        } else {
            frmAbrirGrupo.modelo.addElement("Grupo #" + numeroGrupo);
        }
        grupo = new clsGrupo();

        //======================================================================
        final frmAbrirHorario dialog = new frmAbrirHorario(new javax.swing.JFrame(), true);
        dialog.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent e) {
                dialog.dispose();
            }
        });
        dialog.setVisible(true);
    }//GEN-LAST:event_btnNuevoGrupoActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        if (lstListraGrupos.getSelectedIndex() != -1) {
            int resp = JOptionPane.showConfirmDialog(rootPane, "¿Segur@ de eliminar " + lstListraGrupos.getSelectedValue().toString() + "?",
                    "Eliminar grupos", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (resp == JOptionPane.YES_OPTION) {
                String numGrupo = modelo.getElementAt(lstListraGrupos.getSelectedIndex()).toString();
                numGrupo = numGrupo.substring(7, numGrupo.length());
                clsGestorCursoAbierto gestorCursoAbierto = new clsGestorCursoAbierto();
                if (gestorCursoAbierto.eliminarGrupoAbierto(cursoAbierto.getCodigo(), Integer.parseInt(numGrupo))) {
                    cargarCursosAbiertos();
                    frmAbrirGrupo.modelo = new DefaultListModel();
                    frmAbrirGrupo.lstListraGrupos.setModel(frmAbrirGrupo.modelo);
                    for (clsGrupo g : frmAbrirGrupo.cursoAbierto.getListaGrupos()) {
                        frmAbrirGrupo.modelo.addElement("Grupo #" + g.getNumero());
                    }
                    JOptionPane.showMessageDialog(rootPane, "Grupo eliminado", "Eliminar grupo",
                            JOptionPane.INFORMATION_MESSAGE);
                    if (frmAbrirGrupo.modelo.isEmpty()) {
                        this.dispose();
                    } else {
                        cargarCursosAbiertos();
                        treeHorario.setModel(new DefaultTreeModel(null));
                    }
                } else {
                    JOptionPane.showMessageDialog(rootPane, "Error al eliminar grupo", "Eliminar grupo",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        } else {
            JOptionPane.showMessageDialog(rootPane, "Seleccione el grupo a eliminar", "Editar grupo",
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnAceptarActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
    }//GEN-LAST:event_formWindowOpened

    private void lstListraGruposMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstListraGruposMouseClicked
        cargarDatosGrupo();
    }//GEN-LAST:event_lstListraGruposMouseClicked

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        if (lstListraGrupos.getSelectedIndex() != -1) {
            final frmAbrirHorario dialog = new frmAbrirHorario(new javax.swing.JFrame(), true,
                    lstListraGrupos.getSelectedIndex());
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    dialog.dispose();
                }
            });
            dialog.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(rootPane, "Seleccione el grupo a editar", "Editar grupo",
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnEditarActionPerformed

    private void btnCantidadMaximaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCantidadMaximaActionPerformed
        String nuevaCantidad = (String) JOptionPane.showInputDialog("Digite la nueva cantidad maxima de estudiantes");
        if (!"".equals(nuevaCantidad) && clsValidar.esNumero(nuevaCantidad)) {
            if (Integer.parseInt(nuevaCantidad) < Integer.parseInt(txtCantidadMin.getText())) {
                JOptionPane.showMessageDialog(rootPane, "La nueva cantidad es inferior a la cantidad minima", this.getTitle(), JOptionPane.ERROR_MESSAGE);
            } else {
                clsGestorCursoAbierto gestorCursoAbierto = new clsGestorCursoAbierto();
                if (gestorCursoAbierto.cambiarCantidadMaxima(cursoAbierto.getCodigo(), nuevaCantidad)) {
                    txtCantidadMax.setText(nuevaCantidad);
                    JOptionPane.showMessageDialog(rootPane, "Cantidad modificada exitosamente", this.getTitle(), JOptionPane.INFORMATION_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(rootPane, "Error al modificar cantidad", this.getTitle(), JOptionPane.ERROR_MESSAGE);
                }
            }
        } else {
            JOptionPane.showMessageDialog(rootPane, "Cantidad invalida", this.getTitle(), JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnCantidadMaximaActionPerformed

    private void btnCantidadMinimaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCantidadMinimaActionPerformed
        String nuevaCantidad = (String) JOptionPane.showInputDialog("Digite la nueva cantidad minima de estudiantes");
        if (!"".equals(nuevaCantidad) && clsValidar.esNumero(nuevaCantidad)) {
            if (Integer.parseInt(nuevaCantidad) > Integer.parseInt(txtCantidadMax.getText()) || Integer.parseInt(nuevaCantidad) < 0) {
                JOptionPane.showMessageDialog(rootPane, "La nueva cantidad es superior o igual a la cantidad maxima", this.getTitle(), JOptionPane.ERROR_MESSAGE);
            } else {
                clsGestorCursoAbierto gestorCursoAbierto = new clsGestorCursoAbierto();
                if (gestorCursoAbierto.cambiarCantidadMinima(cursoAbierto.getCodigo(), nuevaCantidad)) {
                    txtCantidadMin.setText(nuevaCantidad);
                    JOptionPane.showMessageDialog(rootPane, "Cantidad modificada exitosamente", this.getTitle(), JOptionPane.INFORMATION_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(rootPane, "Error al modificar cantidad", this.getTitle(), JOptionPane.ERROR_MESSAGE);
                }
            }
        } else {
            JOptionPane.showMessageDialog(rootPane, "Cantidad invalida", this.getTitle(), JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnCantidadMinimaActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                frmAbrirGrupo dialog = new frmAbrirGrupo(new javax.swing.JFrame(), true, "", "", "", new clsCursoAbierto());
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnCantidadMaxima;
    private javax.swing.JButton btnCantidadMinima;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnNuevoGrupo;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    public static javax.swing.JList lstListraGrupos;
    private javax.swing.JPanel pnlHorario;
    private javax.swing.JTabbedPane tabbedPaneDatos;
    private javax.swing.JToolBar toolBarGroup;
    private javax.swing.JTree treeHorario;
    private javax.swing.JTextField txtCantidadMax;
    private javax.swing.JTextField txtCantidadMin;
    private javax.swing.JTextField txtCodigoCurso;
    private javax.swing.JTextField txtCodigoPlan;
    private javax.swing.JTextField txtNombreCurso;
    // End of variables declaration//GEN-END:variables

    public void cargarDatosGrupo() {
        DefaultMutableTreeNode prof = new DefaultMutableTreeNode("");
        DefaultMutableTreeNode dia = null;
        DefaultMutableTreeNode horario;
        if (lstListraGrupos.getSelectedIndex() != -1) {
            int grupoIndex = lstListraGrupos.getSelectedIndex();
            clsGestorCursoAbierto gestorCursoAbierto = new clsGestorCursoAbierto();
            gestorCursoAbierto.conectarBD();
            String profesor = gestorCursoAbierto.buscarProfesor(cursoAbierto.getListaGrupos().get(grupoIndex).getIdentificacion());
            gestorCursoAbierto.desconectarBD();
            prof = new DefaultMutableTreeNode("PROFESOR: " + cursoAbierto.getListaGrupos().get(grupoIndex).getIdentificacion() + "-" + profesor, true);
            horario = new DefaultMutableTreeNode("HORARIO");

            for (clsHorario h : cursoAbierto.getListaGrupos().get(grupoIndex).getListaHorarios()) {
                dia = new DefaultMutableTreeNode(h.getDia(), true);
                dia.add(new DefaultMutableTreeNode(h.getHoraInicio() + "-" + h.getHoraFin() + " EA: " + h.getAula()));
                horario.add(dia);
            }
            prof.add(horario);
        }
        DefaultTreeModel modeloT = new DefaultTreeModel(prof);
        treeHorario.setModel(modeloT);
    }

    public static void cargarCursosAbiertos() {
        boolean courseOpen = false;
        clsGestorCursoAbierto gestorCursoAbierto = new clsGestorCursoAbierto();
        gestorCursoAbierto.conectarBD();
        ResultSet rs = gestorCursoAbierto.seleccionar("SELECT * FROM tblcursosabiertos WHERE CODIGO LIKE '" + cursoAbierto.getCodigo() + "'");
        cursoAbierto = new clsCursoAbierto();
        try {
            if (rs.next()) {
                courseOpen = true;
                cursoAbierto.setCodigoCurso(rs.getString(1));
                cursoAbierto.setCodigoPlan(rs.getString(2));
                cursoAbierto.setAnnio(rs.getInt(3));
                cursoAbierto.setPeriodo(rs.getString(4));
                cursoAbierto.setCodigo(rs.getString(5));
                cursoAbierto.setCantidadMax(rs.getInt(6));
                cursoAbierto.setCantidadMin(rs.getInt(7));
            }
        } catch (SQLException ex) {
            new UseLogger().writeLog("frmAbrirGrupos",ex.toString());
            gestorCursoAbierto.desconectarBD();
        }
        //======================================================================
        if (courseOpen) {
            rs = gestorCursoAbierto.seleccionar("SELECT * FROM tblcursoabiertosgrupos WHERE CODIGO LIKE '" + cursoAbierto.getCodigo() + "'");
            clsGrupo grupo_;
            try {
                while (rs.next()) {
                    grupo_ = new clsGrupo();
                    grupo_.setCodigo(rs.getString(1));
                    grupo_.setNumero(rs.getInt(2));
                    grupo_.setFechaApertura(rs.getString(3));
                    grupo_.setIdentificacion(rs.getString(4));
                    String profesor = gestorCursoAbierto.buscarProfesor(grupo_.getIdentificacion());
                    grupo_.setProfesor(profesor);
                    grupo_.setEstado(rs.getInt(5));
                    cursoAbierto.getListaGrupos().add(grupo_);
                }
            } catch (SQLException ex) {
                new UseLogger().writeLog("frmAbriGrupo",ex.toString());
                gestorCursoAbierto.desconectarBD();
            }
            clsHorario horario;
            int i = 0;
            for (clsGrupo g : cursoAbierto.getListaGrupos()) {
                rs = gestorCursoAbierto.seleccionar("SELECT * FROM tblcursosabiertoshorarios WHERE CODIGO LIKE '" + cursoAbierto.getCodigo() + "' AND NUMERO =" + g.getNumero());
                try {
                    while (rs.next()) {
                        horario = new clsHorario();
                        horario.setCodigo(rs.getString(2));
                        horario.setAula(rs.getString(4));
                        horario.setDia(rs.getString(5));
                        horario.setHoraInicio(rs.getString(6));
                        horario.setHoraFin(rs.getString(7));
                        cursoAbierto.getListaGrupos().get(i).getListaHorarios().add(horario);
                    }
                } catch (SQLException ex) {
                    new UseLogger().writeLog("frmAbrirGrupo",ex.toString());
                }
                i++;
            }
        }
        gestorCursoAbierto.desconectarBD();
    }
}
