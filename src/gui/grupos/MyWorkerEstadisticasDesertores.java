/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.grupos;

import gui.estudiante.*;
import java.awt.Cursor;
import javax.swing.SwingWorker;
import logica.clsConexion;

/**
 *
 * @author Gloriana
 */
public class MyWorkerEstadisticasDesertores extends SwingWorker<Void, Void> {

    private frmEstadisticasDesercion frm;
    clsConexion conexion;

    public MyWorkerEstadisticasDesertores(frmEstadisticasDesercion frm) {
        this.frm = frm;
    }

    /**
     * Metodo que realiza la tarea pesada
     *
     * @throws java.lang.Exception
     */
    @Override
    protected Void doInBackground() throws Exception {
        frm.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        frm.cargarNotasCursos();
        return null;
    }

    /**
     * Resultado en pantalla
     */
    @Override
    protected void done() {
        frm.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }
}