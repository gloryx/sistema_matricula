/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.grupos;

import gui.frmPrincipal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import javax.swing.DefaultListModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import logica.carreras.clsGestorCarreras;
import logica.grupo.clsCursoAbierto;
import logica.grupo.clsGestorCursoAbierto;
import logica.grupo.clsGrupo;
import logica.grupo.clsHorario;
import logica.logger.UseLogger;
import logica.usuario.clsGestorUsuario;
import logica.usuario.clsUsuario;

/**
 *
 * @author Gloriana
 */
public class frmEstadisticasNotas extends javax.swing.JInternalFrame {

    clsCursoAbierto cursoAbierto;
    public DefaultListModel modelo;
    public clsGrupo grupo;
    private TableRowSorter trsfiltro; //creamos el filtro
    DefaultTableModel temp;
    String filtro;
    String carrera;
    /**
     * Creates new form frmOfertaAcademica
     */
    // <editor-fold defaultstate="collapsed" desc="frmOfertaAcademica">  
    public frmEstadisticasNotas() {
        initComponents();
        Calendar fechaAct = Calendar.getInstance();
        int mes = fechaAct.get(Calendar.MONTH) + 1;
        int dia = fechaAct.get(Calendar.DAY_OF_MONTH);
        int annio = fechaAct.get(Calendar.YEAR);

        clsGestorUsuario gestorUsuario = new clsGestorUsuario();
        clsUsuario usuario = gestorUsuario.verificarNivelUsuario(frmPrincipal.lblUsuario.getText().substring(9, frmPrincipal.lblUsuario.getText().length()));
        if (usuario != null) {

            if (usuario.getNivelSeguridad() == 5) {//SUPERUSUARIO CON PRIVILEGIO...
                for (int i = annio - 10; i <= annio; i++) { // 10 annio atras.
                    cboAnnio.addItem(i);
                }

                for (int i = annio + 1; i <= annio + 10; i++) {//10 annios adelante.
                    cboAnnio.addItem(i);
                }
            } else {//SIN PRIVILEGIOS...
                if (mes == 1) {
                    cboAnnio.addItem(annio - 1);
                }
                cboAnnio.addItem(annio);
                if (mes == 12) {
                    cboAnnio.addItem(annio + 1);
                }
            }
        }
        cboAnnio.setSelectedItem(annio);
        if (mes >= 1 && mes <= 4) {
            cboPeriodo.setSelectedIndex(0);
            if (mes == 4) {
                cboPeriodo.setSelectedIndex(1);
            }
        } else if (mes >= 5 && mes <= 8) {
            cboPeriodo.setSelectedIndex(1);
            if (mes == 8) {
                cboPeriodo.setSelectedIndex(2);
            }
        } else {
            cboPeriodo.setSelectedIndex(2);
            cboAnnio.addItem(annio + 1);
            if (mes == 12) {
                cboPeriodo.setSelectedIndex(0);
            }
        }

        cursoAbierto = new clsCursoAbierto();
        cargarCarreras();
    }
// </editor-fold>

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        popMnuAbrirCursos = new javax.swing.JPopupMenu();
        popAbrirCurso = new javax.swing.JMenuItem();
        popGraficoCurso = new javax.swing.JMenuItem();
        jLabel1 = new javax.swing.JLabel();
        cboCarreras = new javax.swing.JComboBox();
        cboPlanEstudios = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblCursos = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        cboAnnio = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        cboPeriodo = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        btnBuscar = new javax.swing.JButton();
        btnImprimir = new javax.swing.JButton();
        btnExportar = new javax.swing.JButton();

        popAbrirCurso.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/tool/opencourse.png"))); // NOI18N
        popAbrirCurso.setLabel("Imprimir este curso...");
        popAbrirCurso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                popAbrirCursoActionPerformed(evt);
            }
        });
        popMnuAbrirCursos.add(popAbrirCurso);

        popGraficoCurso.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/tool/report.png"))); // NOI18N
        popGraficoCurso.setText("Gráfico curso");
        popGraficoCurso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                popGraficoCursoActionPerformed(evt);
            }
        });
        popMnuAbrirCursos.add(popGraficoCurso);
        popGraficoCurso.getAccessibleContext().setAccessibleName("Ver gráfico");

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setTitle("Estadísticas calificaciones por curso");
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/img/win/group.png"))); // NOI18N

        jLabel1.setText("Carrera:");

        cboCarreras.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cboCarrerasItemStateChanged(evt);
            }
        });
        cboCarreras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboCarrerasActionPerformed(evt);
            }
        });

        cboPlanEstudios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboPlanEstudiosActionPerformed(evt);
            }
        });

        jLabel2.setText("Plan de estudios:");

        tblCursos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "CODIGO", "NOMBRE", "PROMEDIO"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblCursos.setColumnSelectionAllowed(true);
        tblCursos.setComponentPopupMenu(popMnuAbrirCursos);
        tblCursos.getTableHeader().setReorderingAllowed(false);
        tblCursos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblCursosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblCursos);
        tblCursos.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        if (tblCursos.getColumnModel().getColumnCount() > 0) {
            tblCursos.getColumnModel().getColumn(0).setPreferredWidth(50);
            tblCursos.getColumnModel().getColumn(1).setPreferredWidth(350);
            tblCursos.getColumnModel().getColumn(2).setPreferredWidth(30);
        }

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Periodo"));

        jLabel3.setText("Periodo:");

        cboPeriodo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "I", "II", "III", "IV" }));

        jLabel4.setText("Año:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(jLabel3))
                .addGap(0, 0, 0)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cboAnnio, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cboPeriodo, 0, 155, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(cboAnnio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(cboPeriodo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/tool/find.png"))); // NOI18N
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        btnImprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/tool/printmgr.png"))); // NOI18N
        btnImprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprimirActionPerformed(evt);
            }
        });

        btnExportar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/tool/exportoexcel.png"))); // NOI18N
        btnExportar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExportarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cboPlanEstudios, 0, 546, Short.MAX_VALUE)
                            .addComponent(cboCarreras, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnImprimir, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnExportar, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnBuscar, btnExportar, btnImprimir});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(cboCarreras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(cboPlanEstudios, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(btnBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnImprimir, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnExportar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 287, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cboCarrerasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboCarrerasActionPerformed
        if (cboCarreras.getSelectedIndex() > 0) {
            clsGestorCarreras gestorCarreras = new clsGestorCarreras();
            gestorCarreras.conectarBD();
            ResultSet rs = gestorCarreras.cargarPlanesCarreras(cboCarreras.getSelectedItem().toString());
            if (rs != null) {
                try {
                    cboPlanEstudios.removeAllItems();
                    cboPlanEstudios.addItem("ELEGIR...");
                    while (rs.next()) {
                        cboPlanEstudios.addItem(rs.getString(1));
                    }
                } catch (SQLException ex) {
                    new UseLogger().writeLog(this.getName(), ex.toString());
                    gestorCarreras.desconectarBD();
                }
            }
            temp = null;
            temp = (DefaultTableModel) tblCursos.getModel();
            trsfiltro = new TableRowSorter(temp);
            tblCursos.setRowSorter(trsfiltro);
            gestorCarreras.desconectarBD();
        }
    }//GEN-LAST:event_cboCarrerasActionPerformed

    private void cboPlanEstudiosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboPlanEstudiosActionPerformed
        if (cboCarreras.getSelectedIndex() > 0 && cboPlanEstudios.getSelectedIndex() > 0) {
            cargarNotasCursos(cboPlanEstudios.getSelectedItem().toString());
        }
    }//GEN-LAST:event_cboPlanEstudiosActionPerformed

    private void popAbrirCursoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_popAbrirCursoActionPerformed
        int fila = tblCursos.getSelectedRow();
        if (fila != -1) {
            String codigo = cboAnnio.getSelectedItem().toString()
                    + cboPeriodo.getSelectedItem().toString()
                    + tblCursos.getValueAt(fila, 0).toString()
                    + cboPlanEstudios.getSelectedItem().toString();
            cargarCursosAbiertos(codigo, fila);
            abrirVentanaGrupo(true);

        }
    }//GEN-LAST:event_popAbrirCursoActionPerformed

    private void tblCursosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblCursosMouseClicked
        int fila = tblCursos.getSelectedRow();
        if (fila != -1) {
            if (evt.getClickCount() == 2) {
                String codigo = cboAnnio.getSelectedItem().toString()
                        + cboPeriodo.getSelectedItem().toString()
                        + tblCursos.getValueAt(fila, 0).toString()
                        + cboPlanEstudios.getSelectedItem().toString();
                cargarCursosAbiertos(codigo, fila);
                abrirVentanaGrupo(true);
            }
        }
    }//GEN-LAST:event_tblCursosMouseClicked

    private void cboCarrerasItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cboCarrerasItemStateChanged
        DefaultTableModel modeloT = new javax.swing.table.DefaultTableModel(
                new Object[][]{},
                new String[]{
                    "CODIGO", "NOMBRE", "PROMEDIO", "GRUPO"
                }) {
            boolean[] canEdit = new boolean[]{
                false, false, false
            };

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        };
        tblCursos.setModel(modeloT);
        tblCursos.getColumnModel().getColumn(0).setPreferredWidth(40);
        tblCursos.getColumnModel().getColumn(1).setPreferredWidth(360);
        tblCursos.getColumnModel().getColumn(2).setPreferredWidth(40);
        tblCursos.getColumnModel().getColumn(3).setPreferredWidth(40);
    }//GEN-LAST:event_cboCarrerasItemStateChanged

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        if (cboCarreras.getSelectedIndex() > 0 && cboPlanEstudios.getSelectedIndex() > 0) {
            cargarNotasCursos(
                    cboPlanEstudios.getSelectedItem().toString());

        }
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void btnImprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprimirActionPerformed
        if (cboCarreras.getSelectedIndex() > 0 && cboPlanEstudios.getSelectedIndex() > 0) {
            clsGestorCarreras gestor = new clsGestorCarreras();
            gestor.estadisticasReporteNotas(
                    cboAnnio.getSelectedItem().toString(),
                    cboPeriodo.getSelectedItem().toString(),
                    cboPlanEstudios.getSelectedItem().toString(),
                    cboCarreras.getSelectedItem().toString());
        }
    }//GEN-LAST:event_btnImprimirActionPerformed

    private void btnExportarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExportarActionPerformed
        if (cboCarreras.getSelectedIndex() > 0 && cboPlanEstudios.getSelectedIndex() > 0) {
            clsGestorCarreras gestor = new clsGestorCarreras();

            String sql = elaborarSentencia();

            gestor.generarArchivoExcel(sql);
        }
    }//GEN-LAST:event_btnExportarActionPerformed

    private void popGraficoCursoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_popGraficoCursoActionPerformed
        int fila = tblCursos.getSelectedRow();
        if (fila != -1) {
            String codigo = cboAnnio.getSelectedItem().toString()
                    + cboPeriodo.getSelectedItem().toString()
                    + tblCursos.getValueAt(fila, 0).toString()
                    + cboPlanEstudios.getSelectedItem().toString();
            cargarCursosAbiertos(codigo, fila);
            abrirVentanaGrupo(false);

        }
    }//GEN-LAST:event_popGraficoCursoActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnExportar;
    private javax.swing.JButton btnImprimir;
    private javax.swing.JComboBox cboAnnio;
    private javax.swing.JComboBox cboCarreras;
    private javax.swing.JComboBox cboPeriodo;
    private javax.swing.JComboBox cboPlanEstudios;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JMenuItem popAbrirCurso;
    private javax.swing.JMenuItem popGraficoCurso;
    private javax.swing.JPopupMenu popMnuAbrirCursos;
    private javax.swing.JTable tblCursos;
    // End of variables declaration//GEN-END:variables

    private void cargarCarreras() {
        clsGestorCarreras gestorCarreras = new clsGestorCarreras();
        gestorCarreras.conectarBD();
        ResultSet rs = gestorCarreras.cargarCarreras();
        if (rs != null) {
            cboCarreras.addItem("ELEGIR...");
            try {
                while (rs.next()) {
                    cboCarreras.addItem(rs.getString(1));
                }
            } catch (SQLException ex) {
                new UseLogger().writeLog(this.getName(), ex.toString());
            }
        }
        gestorCarreras.desconectarBD();
    }

    private void cargarNotasCursos(String plan) {
        DefaultTableModel modeloT = new javax.swing.table.DefaultTableModel(
                new Object[][]{},
                new String[]{
                    "CODIGO", "NOMBRE", "PROMEDIO", "GRUPO"
                }) {
            boolean[] canEdit = new boolean[]{
                false, false, false, false
            };

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        };

        clsGestorCarreras gestorCarreras = new clsGestorCarreras();
        gestorCarreras.conectarBD();
        ResultSet rs = gestorCarreras.estadisticasNotasCursos(plan, cboAnnio.getSelectedItem().toString(),
                cboPeriodo.getSelectedItem().toString());
        if (rs != null) {
            try {
                double suma = 0;
                int cont = 0;
                Object[] fila = null;
                while (rs.next()) {
                    // Hay tres columnas en la tabla
                    fila = new Object[4]; // Hay tres columnas en la tabla

                    // Se rellena cada posición del array con una de las columnas de la tabla en base de datos.
                    for (int i = 0; i < 4; i++) {
                        fila[i] = rs.getObject(i + 1); // El primer indice en rs es el 1, no el cero, por eso se suma 1.
                        if (i == 2) {
                            suma += Double.parseDouble(fila[i].toString());
                        }

                    }
                    cont++;
                    // Se añade al modelo la fila completa.
                    modeloT.addRow(fila);

                }
                if (cont > 0) {
                    fila = new Object[2];
                    fila[0] = "TOTAL CURSOS";
                    fila[1] = cont;
                    modeloT.addRow(fila);
                    fila = new Object[2];
                    fila[0] = "PROMEDIO GENERAL";
                    fila[1] = round(suma / cont, 2);
                    modeloT.addRow(fila);
                }
                tblCursos.setModel(modeloT);

                tblCursos.setColumnSelectionAllowed(true);
                tblCursos.getTableHeader().setReorderingAllowed(false);
//                tblCursos.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
//                tblCursos.getColumnModel().getColumn(0).setPreferredWidth(40);
//                tblCursos.getColumnModel().getColumn(1).setPreferredWidth(360);
//                tblCursos.getColumnModel().getColumn(2).setPreferredWidth(40);
//                tblCursos.getColumnModel().getColumn(3).setPreferredWidth(40);
//                tblCursos.getColumnModel().getColumn(4).setPreferredWidth(0);
            } catch (SQLException ex) {
                new UseLogger().writeLog(this.getName(), ex.toString());
                gestorCarreras.desconectarBD();
            }

        }
        gestorCarreras.desconectarBD();
        temp = null;
        temp = (DefaultTableModel) tblCursos.getModel();
        trsfiltro = new TableRowSorter(temp);
        tblCursos.setRowSorter(trsfiltro);
    }

    private void abrirVentanaGrupo(boolean pop) {
        int fila = tblCursos.getSelectedRow();
        if (fila != -1) {
            String annio = "";
            String periodo = "";
            String plan = "";
            String curso = "";
            try {
                annio = cboAnnio.getSelectedItem().toString();
                periodo = cboPeriodo.getSelectedItem().toString();
                plan = cboPlanEstudios.getSelectedItem().toString();
                curso = tblCursos.getValueAt(fila, 0).toString();

            } catch (NullPointerException ex) {
                annio = "";
                periodo = "";
                plan = "";
                curso = "";
            }
            final frmGrupoAbiertoElegirImp dialog = new frmGrupoAbiertoElegirImp(new javax.swing.JFrame(),
                    true, annio, periodo,cboCarreras.getSelectedItem().toString(), plan, curso);
            if (pop == true) {
                dialog.setTitle("Imprimir calificaciones de estudiante por grupo");
            } else {
                dialog.setTitle("Imprimir gráfico de notas por grupo");
            }
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    dialog.dispose();
                }
            });
            dialog.setVisible(true);
        }
    }

    private void cargarCursosAbiertos(String codigo, int fila) {
        boolean courseOpen = false;
        clsGestorCursoAbierto gestorCursoAbierto = new clsGestorCursoAbierto();
        gestorCursoAbierto.conectarBD();
        ResultSet rs = gestorCursoAbierto.seleccionar("SELECT * FROM tblcursosabiertos WHERE CODIGO LIKE '" + codigo + "'");
        cursoAbierto = new clsCursoAbierto();
        try {
            if (rs.next()) {
                courseOpen = true;
                cursoAbierto.setCodigoCurso(rs.getString(1));
                cursoAbierto.setCodigoPlan(rs.getString(2));
                cursoAbierto.setAnnio(rs.getInt(3));
                cursoAbierto.setPeriodo(rs.getString(4));
                cursoAbierto.setCodigo(rs.getString(5));
                cursoAbierto.setCantidadMax(rs.getInt(6));
                cursoAbierto.setCantidadMin(rs.getInt(7));
            }
        } catch (SQLException ex) {
            new UseLogger().writeLog(this.getName(), ex.toString());
            gestorCursoAbierto.desconectarBD();
        }
        //======================================================================
        if (courseOpen) {
            rs = gestorCursoAbierto.seleccionar("SELECT * FROM tblcursoabiertosgrupos WHERE CODIGO LIKE '" + codigo + "'");
            clsGrupo grupo_;
            try {
                while (rs.next()) {
                    grupo_ = new clsGrupo();
                    grupo_.setCodigo(rs.getString(1));
                    grupo_.setNumero(rs.getInt(2));
                    grupo_.setFechaApertura(rs.getString(3));
                    grupo_.setIdentificacion(rs.getString(4));
                    String profesor = gestorCursoAbierto.buscarProfesor(grupo_.getIdentificacion());
                    grupo_.setProfesor(profesor);
                    grupo_.setEstado(rs.getInt(5));
                    cursoAbierto.getListaGrupos().add(grupo_);
                }
            } catch (SQLException ex) {
                new UseLogger().writeLog(this.getName(), ex.toString());
                gestorCursoAbierto.desconectarBD();
            }
            clsHorario horario;
            int i = 0;
            for (clsGrupo g : cursoAbierto.getListaGrupos()) {
                rs = gestorCursoAbierto.seleccionar("SELECT * FROM tblcursosabiertoshorarios WHERE CODIGO LIKE '" + codigo + "' AND NUMERO =" + g.getNumero());
                try {
                    while (rs.next()) {
                        horario = new clsHorario();
                        horario.setCodigo(rs.getString(2));
                        horario.setAula(rs.getString(4));
                        horario.setDia(rs.getString(5));
                        horario.setHoraInicio(rs.getString(6));
                        horario.setHoraFin(rs.getString(7));
                        cursoAbierto.getListaGrupos().get(i).getListaHorarios().add(horario);
                    }
                } catch (SQLException ex) {
                    new UseLogger().writeLog(this.getName(), ex.toString());
                }
                i++;
            }
        } else {//CUANDO ES EL PRIMER GRUPOS QUE SE VA ABRIR...
            cursoAbierto.setCodigoCurso(tblCursos.getValueAt(fila, 0).toString());
            cursoAbierto.setCodigoPlan(cboPlanEstudios.getSelectedItem().toString());
            //fecha del sistema          
            String annio = cboAnnio.getSelectedItem().toString();
            cursoAbierto.setAnnio(Integer.parseInt(annio));
            cursoAbierto.setPeriodo(cboPeriodo.getSelectedItem().toString());
            cursoAbierto.setCodigo(codigo);
            cursoAbierto.setCantidadMax(25);
            cursoAbierto.setCantidadMin(12);
            //estado del sistema
        }
        gestorCursoAbierto.desconectarBD();
    }

    public static double round(double value, int places) {
        if (places < 0) {
            throw new IllegalArgumentException();
        }

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    private String elaborarSentencia() {
        String sql = "";
        if (cboCarreras.getSelectedIndex() > 0 && cboPlanEstudios.getSelectedIndex() > 0) {
            sql = "SELECT\n"
                    + "  tblcursos.CODIGOCURSO,\n"
                    + "  tblcursos.NOMBRECURSO AS CURSO,\n"
                    + "	TRUNCATE(AVG(tblcalificaciones.`calificacion`),2) AS PROMEDIO,\n"
                    + "  tblcalificaciones.`grupo` AS GRUPO\n"
                    + "     \n"
                    + "FROM\n"
                    + "     `tblcursosabiertos` tblcursosabiertos INNER JOIN `tblcursoabiertosgrupos` tblcursoabiertosgrupos ON tblcursosabiertos.`CODIGO` = tblcursoabiertosgrupos.`CODIGO`\n"
                    + "     INNER JOIN `tblcursos` tblcursos ON tblcursosabiertos.`CODIGOCURSO` = tblcursos.`CODIGOCURSO`\n"
                    + "     AND tblcursos.`CODIGOPLAN` = tblcursosabiertos.`CODIGOPLAN`\n"
                    + "     INNER JOIN `tblcalificaciones` tblcalificaciones ON tblcursoabiertosgrupos.`CODIGO` = tblcalificaciones.`codigo`\n"
                    + "     AND tblcursoabiertosgrupos.`NUMERO` = tblcalificaciones.`grupo`\n"
                    + "WHERE\n"
                    + "     tblcursosabiertos.annio = "+cboAnnio.getSelectedItem().toString()+"\n"
                    + " AND tblcursosabiertos.periodo LIKE '"+ cboPeriodo.getSelectedItem().toString()+"'\n"
                    + " AND tblcursosabiertos.codigoplan LIKE '"+cboPlanEstudios.getSelectedItem().toString()+"'\n"
                    + "  AND tblcalificaciones.`calificacion` <> 0\n"
                    + "GROUP BY tblcursos.`NOMBRECURSO`,tblcalificaciones.`grupo`\n"
                    + "ORDER BY PROMEDIO DESC, tblcalificaciones.`grupo`";
        }
        return sql;
    }
}
